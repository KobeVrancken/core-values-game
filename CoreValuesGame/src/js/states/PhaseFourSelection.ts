/// <reference path= "../../../lib/phaser.d.ts" />
/// <reference path= "../permanent/GameState.ts" />
/// <reference path= "../phases/all/Pillar.ts" />
/// <reference path= "../phases/all/BigInstructions.ts" />
/// <reference path= "../phases/one/PillarButton.ts" />
/// <reference path= "../external/VideoReference.ts" />
/// <reference path= "../phases/all/GamePhaseTracker.ts" />


class PhaseFourSelection{

    private game:Phaser.Game;
    private gameState:GameState;
    private backgroundSprite:Phaser.Sprite;

    private pillarXLocations:Array<number> = [222, 376, 520, 665];
    private pillarYLocations:Array<number> = [180, 203, 272, 219];

    private fontStyleWhite:any = {
        font: 'bold 22px Arial',
        fill: '#FFFFFF'
    };

    private fontStyleYellow:any = {
        font: 'bold 26px Arial',
        fill: '#FFFF00'
    };


    constructor(game:Phaser.Game) {
        this.game = game;
    }

    public createGamePhaseTracker() {
        var gamePhaseTracker:GamePhaseTracker = new GamePhaseTracker(this.game, 4, "PHASE 4", "The Survival Phase", "phaseFourDogtag", "#FFFF00");
        gamePhaseTracker.setCopyRightColor("#1497d6");
        this.gameState.subscribe(gamePhaseTracker);
        gamePhaseTracker.draw();
        gamePhaseTracker.getScoreButton().hide();
        gamePhaseTracker.drawDelayed();
    }

    init(gameState:GameState){
        this.gameState = gameState;
        var pillars:Array<Pillar> = this.gameState.getPillars();
        var cont:boolean = true;
        for(var i:number = 0; i<pillars.length; i++){
            if(!this.gameState.hasValueScoresForPillar(pillars[i], 4)) {
                cont = false;
            }
        }
        if(cont){
            this.game.state.start("PhaseFive", true, false, this.gameState);
        }
    }

    create(){
        var sea:VideoReference = new VideoReference("video_dark_waves");
        sea.showVideo();
        sea.playVideo();
        this.backgroundSprite = this.game.add.sprite(90, 237, "phaseFourBackgroundTop");
        this.drawLifeRaft();
        this.drawClickInfo();
        this.createGamePhaseTracker();
        //this.drawPillarButtons();
        //this.drawPillarText();
        var instructions:Instructions = new BigInstructions(this.game, "PHASE 4", ["You and your most important values have been lost for days. All of you are cold, sick and desperate." +
        " Without food and water, you will wither away; many of your most important values will not survive until dawn.\n\nIn the dark of the night with the moon shining above you sit in " +
        "your life raft. You grapple with a difficult decision. You must decide which values will survive to see the morning. You are nearing the end of the game - but first you must survive" +
        " to see the morning!\n\n" +
        "The Survival Phase will take approximately 3-5 minutes."]);
        instructions.draw();
    }

    private drawLifeRaft():void{
        var lifeRaft:Phaser.Sprite = this.game.add.sprite(425, 440, "phaseFourLifeRaft");
        var health:Phaser.Sprite = this.game.add.sprite(425, 440, "phaseFourHealth");
        var god:Phaser.Sprite = this.game.add.sprite(425, 440, "phaseFourGod");
        var work:Phaser.Sprite = this.game.add.sprite(425, 440, "phaseFourWork");
        var love:Phaser.Sprite = this.game.add.sprite(425, 440, "phaseFourLove");
        var healthButton:Phaser.Sprite = this.drawHexagon(575, 485, 90, 85, 20, 260);
        var loveButton:Phaser.Sprite = this.drawHexagon(645, 520, 90, 85, 20, 285);
        var workButton:Phaser.Sprite = this.drawHexagon(725, 530, 90, 85, 20, 90);
        var godButton:Phaser.Sprite = this.drawHexagon(760, 490, 90, 85, 20, 45);
        var lovePillar:Pillar = this.gameState.getPillars()[0];
        var godPillar:Pillar = this.gameState.getPillars()[1];
        var workPillar:Pillar = this.gameState.getPillars()[2];
        var healthPillar:Pillar = this.gameState.getPillars()[3];

        this.addListener(loveButton, lovePillar);
        this.addListener(healthButton, healthPillar);
        this.addListener(workButton, workPillar);
        this.addListener(godButton, godPillar);

    }

    private addListener(button, pillar) {
        button.events.onInputDown.add(function () {
            this.game.state.start("PhaseFour", true, false, this.gameState, pillar);
        }, this);
    }


    private drawHexagon(x:number, y:number, trWidth:number, trHeight:number, topOffset:number, rotation:number):Phaser.Sprite{
            var group:Phaser.Group = new Phaser.Group(this.game);
            var hexagon:Phaser.Graphics = new Phaser.Graphics(this.game, 0, 0);
            var hexagonSprite:Phaser.Sprite = new Phaser.Sprite(this.game, x, y);
            hexagonSprite.addChild(hexagon);
            hexagonSprite.inputEnabled = true;
            hexagonSprite.input.useHandCursor = true;
            var points: Phaser.Point[] = [];
            points.push(new Phaser.Point(0,trHeight-topOffset));
            points.push(new Phaser.Point(0,topOffset));
            points.push(new Phaser.Point(trWidth/2, 0));
            points.push(new Phaser.Point(trWidth,topOffset));
            points.push(new Phaser.Point(trWidth,trHeight-topOffset));
            points.push(new Phaser.Point(trWidth/2, trHeight));
            hexagon.beginFill(0x000000, 0);
            hexagon.drawPolygon(points);
            hexagon.rotation = rotation;
            group.add(hexagonSprite);
            return hexagonSprite;
    }

    private drawClickInfo():void{
        var clickPillar:Phaser.Sprite = this.game.add.sprite(150, 425, "phaseFourClickPillar");
    }


    drawPillarButtons():void{
        var pillarButtonGroup = this.game.add.group();
        pillarButtonGroup.x = 150;
        pillarButtonGroup.y = 180;
        var pillars:Array<Pillar> = this.gameState.getPillars();
        var pillarButton:PillarButton;
        for(var i:number = 0; i<pillars.length; i++){
            pillarButton = new PillarButton(this.game, this.gameState, pillarButtonGroup, pillars[i]);
            if(this.gameState.hasValueScoresForPillar(pillars[i], 1)) {
                pillarButton.enableRescore();
            }
            pillarButton.draw(this.pillarXLocations[i], this.pillarYLocations[i]);
        }
    }

    drawPillarText():void{
        /*var text:Phaser.Text = this.game.add.text(600, 250, "You will find your Core Values in groups in different places on your cruise ship.\nYour Core Values are Organized in 4 Essential Core Value Pillars.", this.fontStyleWhite);
         text.anchor.set(0.5, 0.5);
         text.align = 'center';
         text.wordWrap = true;
         text.wordWrapWidth = 880;

         var text:Phaser.Text = this.game.add.text(600, 310, "Click on one of your Essential Core Values Pillars to Continue.", this.fontStyleYellow);
         text.anchor.set(0.5, 0.5);
         text.align = 'center';
         text.wordWrap = true;
         text.wordWrapWidth = 880;*/
    }
}