/**
 * Created by kobe on 3/23/2015.
 */
/// <reference path= "../../../lib/phaser.d.ts" />
/// <reference path= "../external/VideoReference.ts" />
/// <reference path= "../permanent/GameParameters.ts" />
var Boot = (function () {
    function Boot(game) {
        this.game = game;
    }
    Boot.prototype.preload = function () {
        this.game.load.image('preload_empty', 'img/preload_empty.png');
        this.game.load.image('preload_full', 'img/preload_full.png');
    };
    Boot.prototype.create = function () {
        //this.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
        var gameParameters = GameParameters.getInstance();
        this.game.stage.backgroundColor = gameParameters.stageColor;
        this.game.state.start("Preload");
    };
    return Boot;
})();
//# sourceMappingURL=boot.js.map