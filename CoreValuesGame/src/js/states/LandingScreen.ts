/// <reference path= "../../../lib/phaser.d.ts" />
/// <reference path= "../permanent/PermanentContext.ts"/>
/// <reference path= "../external/VideoReference.ts"/>
/// <reference path= "../permanent/GameState.ts" />
/// <reference path= "../permanent/Pause.ts"/>

class LandingScreen {

    private game: Phaser.Game;
    private gameState:GameState;
    private video: VideoReference;

    constructor(game:Phaser.Game) {
        this.game = game;
        this.video = new VideoReference("intro_screen");
    }

    init(gameState:GameState){
        this.gameState = gameState;
    }

    create() {
        this.game.onPause.add(function(){
            var pause:Pause = new Pause(this.game);
        }, this);
        this.video.showVideo();
        var permanentContext:PermanentContext = new PermanentContext(this.game, this.gameState);
        this.gameState.subscribe(permanentContext);
        permanentContext.draw();
        var background = this.game.add.sprite(145, 180, 'background');
        var button = this.game.add.button(485, 625, 'roundContinueButton', this.continue, this, 1, 0);

        //phase skip
        var phaseTwoKey:Phaser.Key = this.game.input.keyboard.addKey(Phaser.Keyboard.TWO);
        phaseTwoKey.onDown.add(this.phaseTwo, this);
        var phaseTwoBKey:Phaser.Key = this.game.input.keyboard.addKey(Phaser.Keyboard.THREE);
        phaseTwoBKey.onDown.add(this.phaseTwoB, this);
        var phaseThreeKey:Phaser.Key = this.game.input.keyboard.addKey(Phaser.Keyboard.FOUR);
        phaseThreeKey.onDown.add(this.phaseThree, this);
        var phaseThreeBKey:Phaser.Key = this.game.input.keyboard.addKey(Phaser.Keyboard.FIVE);
        phaseThreeBKey.onDown.add(this.phaseThreeB, this);
        var phaseFourKey:Phaser.Key = this.game.input.keyboard.addKey(Phaser.Keyboard.SIX);
        phaseFourKey.onDown.add(this.phaseFour, this);
    }

    continue(){
        this.video.hideVideo();
        this.game.state.start("PillarSelection", true, false, this.gameState);
    }

    phaseTwo(){
        console.log("phase two");
        this.video.hideVideo();
        this.game.state.start("PhaseTwo", true, false, this.gameState);
    }

    phaseTwoB(){
        console.log("phase two b");
        this.video.hideVideo();
        this.game.state.start("PhaseTwoPartB", true, false, this.gameState);
    }

    phaseThree(){
        console.log("phase three");
        this.video.hideVideo();
        this.game.state.start("PhaseThree", true, false, this.gameState);
    }

    phaseThreeB(){
        console.log("phase three b");
        this.video.hideVideo();
        this.game.state.start("PhaseThreePartB", true, false, this.gameState);
    }

    phaseFour(){
        console.log("phase four");
        this.video.hideVideo();
        this.game.state.start("PhaseFourSelection", true, false, this.gameState);
    }


}

