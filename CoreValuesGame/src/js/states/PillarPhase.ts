/**
 * Created by kobe on 3/24/2015.
 */
/// <reference path= "../../../lib/phaser.d.ts" />
/// <reference path= "../permanent/GameState.ts" />
/// <reference path= "../phases/all/Pillar.ts" />
/// <reference path= "../phases/all/GamePhaseTracker.ts" />
/// <reference path= "../phases/all/MediumInstructions.ts" />
/// <reference path= "../event/GameEventPublisher.ts" />
/// <reference path= "../event/GameEvent.ts" />
/// <reference path= "../event/EndInstructionsEvent.ts" />
/// <reference path= "../phases/one/PhaseOneGameBoard.ts" />
/// <reference path= "PlayPhase.ts" />

class PillarPhase extends PlayPhase{

    private pillar:Pillar;
    private backgroundSprite:Phaser.Sprite;

    constructor(game:Phaser.Game) {
        super(game);
    }

    public init(gameState:GameState, pillar:Pillar) {
        this.gameState = gameState;
        this.pillar = pillar;
        if(this.gameState.hasValueScoresForPillar(pillar, 1)){
            this.gameState.removeValueScoresForPillar(pillar, 1);
        }
    }

    public createGamePhaseTracker() {
        this.gamePhaseTracker = new GamePhaseTracker(this.game, 1, "PHASE 1", "The Cruise Ship Phase", "smallCruiseShip", this.pillar.getColor());
    }

    public drawBackgroundImage():void{
        this.backgroundSprite = this.game.add.sprite(45, 212, "phaseBackgroundBoat");
    }

    public createMediumInstructions():MediumInstructions{
        return new MediumInstructions(this.game, "PHASE 1", ['You have invited 90 of your God and spirituality values on a Cruise ship and they are ' +
        'all hanging out on the pool deck. You will have varying degrees of resonance with each value.',
            'As you hang out with your God and spirituality values you rate how important they are to you. If you rate a value high it would mean that you hold that value' +
            'in high esteem - either in yourself, or others.',
            'For each value, there is a button which reveals a definition and example. Your definition of each value is personal to you. The ones provided are generic, so' +
            'only read them when you must.']);
    }

    public startGameplayCleanup():void{
        this.backgroundSprite.destroy();
    }

    public drawGameBoard():void{
        var gameBoard:PhaseOneGameBoard = new PhaseOneGameBoard(this.game, this.pillar, this.gameState);
        gameBoard.subscribe(this.gameState);
        gameBoard.subscribe(this.gamePhaseTracker);
        gameBoard.draw();
    }
}