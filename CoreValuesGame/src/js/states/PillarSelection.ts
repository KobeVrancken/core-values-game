/**
 * Created by kobe on 3/23/2015.
 */

/// <reference path= "../../../lib/phaser.d.ts" />
/// <reference path= "../permanent/GameState.ts" />
/// <reference path= "../phases/all/Pillar.ts" />
/// <reference path= "../phases/all/BigInstructions.ts" />
/// <reference path= "../phases/one/PillarButton.ts" />


class PillarSelection {

    private game:Phaser.Game;
    private gameState:GameState;

    private pillarXLocations:Array<number> = [222, 376, 520, 665];
    private pillarYLocations:Array<number> = [180, 203, 272, 219];

    private fontStyleWhite:any = {
        font: 'bold 22px Arial',
        fill: '#FFFFFF'
    };

    private fontStyleYellow:any = {
        font: 'bold 26px Arial',
        fill: '#FFFF00'
    };


    constructor(game:Phaser.Game) {
        this.game = game;
    }

    init(gameState:GameState){
        this.gameState = gameState;
        var pillars:Array<Pillar> = this.gameState.getPillars();
        var cont:boolean = true;
        for(var i:number = 0; i<pillars.length; i++){
            if(!this.gameState.hasValueScoresForPillar(pillars[i], 1)) {
                cont = false;
            }
        }
        if(cont){
            this.game.state.start("PhaseTwo", true, false, this.gameState);
        }
    }

    create(){
        var background = this.game.add.sprite(145, 180, 'phaseSelectionBackground');
        this.drawPillarButtons();
        this.drawPillarText();
        var instructions:Instructions = new BigInstructions(this.game, "PHASE 1", ["Discovering your core values is one of the most valuable things you'll ever do! This game will help you do just that - while following an adventure story." +
        "\n\n One of our Core Values is authenticity, so we didn't cut any corners. You can expect a playing time of 45-90 minutes. Turn off the TV, and grab a cup of coffee, as you'll want to focus. Then, find a comfortable place to begin." +
        "\n\n In Phase 1, you'll score 360 values. This phase can take between 10 and 20 minutes. We've chosen music specifically for each phase of this game, but you can turn it on and off as you wish."]);
        instructions.draw();
    }

    drawPillarButtons():void{
        var pillarButtonGroup = this.game.add.group();
        pillarButtonGroup.x = 150;
        pillarButtonGroup.y = 180;
        var pillars:Array<Pillar> = this.gameState.getPillars();
        var pillarButton:PillarButton;
        for(var i:number = 0; i<pillars.length; i++){
            pillarButton = new PillarButton(this.game, this.gameState, pillarButtonGroup, pillars[i]);
            if(this.gameState.hasValueScoresForPillar(pillars[i], 1)) {
                pillarButton.enableRescore();
            }
            pillarButton.draw(this.pillarXLocations[i], this.pillarYLocations[i]);
        }
    }

    drawPillarText():void{
        var text:Phaser.Text = this.game.add.text(600, 250, "You will find your Core Values in groups in different places on your cruise ship.\nYour Core Values are Organized in 4 Essential Core Value Pillars.", this.fontStyleWhite);
        text.anchor.set(0.5, 0.5);
        text.align = 'center';
        text.wordWrap = true;
        text.wordWrapWidth = 880;

        var text:Phaser.Text = this.game.add.text(600, 310, "Click on one of your Essential Core Values Pillars to Continue.", this.fontStyleYellow);
        text.anchor.set(0.5, 0.5);
        text.align = 'center';
        text.wordWrap = true;
        text.wordWrapWidth = 880;
    }
}