/// <reference path= "../../../lib/phaser.d.ts" />
/// <reference path= "../permanent/GameState.ts" />
/// <reference path= "../phases/all/Pillar.ts" />
/// <reference path= "../phases/all/GamePhaseTracker.ts" />
/// <reference path= "../phases/all/MediumInstructions.ts" />
/// <reference path= "../event/GameEventPublisher.ts" />
/// <reference path= "../event/GameEvent.ts" />
/// <reference path= "../event/EndInstructionsEvent.ts" />
/// <reference path= "../phases/one/PhaseOneGameBoard.ts" />
/// <reference path= "PlayPhase.ts" />

class PhaseFour extends PlayPhase{

    private pillar:Pillar;
    private backgroundSprite:Phaser.Sprite;

    constructor(game:Phaser.Game) {
        super(game);
    }

    public init(gameState:GameState, pillar:Pillar) {
        this.gameState = gameState;
        this.pillar = pillar;
        if(this.gameState.hasValueScoresForPillar(pillar, 1)){
            this.gameState.removeValueScoresForPillar(pillar, 1);
        }
    }

    public createGamePhaseTracker() {
        this.gamePhaseTracker = new GamePhaseTracker(this.game, 4, "PHASE 4", "The Survival Phase", "phaseFourDogtag", "#FFFF00");
        this.gamePhaseTracker.setCopyRightColor("#1497d6");
    }

    public drawBackgroundImage():void{
        this.backgroundSprite = this.game.add.sprite(90, 237, "phaseFourBackgroundTop");
    }

    public createMediumInstructions():MediumInstructions{
        return null;
    }

    public startGameplayCleanup():void{
        //this.backgroundSprite.destroy();
    }

    public drawGameBoard():void{
        var gameBoard:PhaseFourGameBoard = new PhaseFourGameBoard(this.game, this.pillar, this.gameState);
        gameBoard.subscribe(this.gameState);
        gameBoard.subscribe(this.gamePhaseTracker);
        gameBoard.draw();
    }
}