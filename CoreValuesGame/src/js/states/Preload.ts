/// <reference path= "../../../lib/phaser.d.ts" />
/// <reference path= "../phases/all/Pillar.ts" />
/// <reference path= "../phases/all/Value.ts" />

/// <reference path= "../permanent/GameState.ts" />
/// <reference path= "../permanent/GameParameters.ts" />
/// <reference path= "../util/XMLParser.ts" />


class Preload{

    private game: Phaser.Game;
    private gameState: GameState;

    constructor(game: Phaser.Game) {
        this.game = game;
    }

    preload(){
        var gameParameters:GameParameters = GameParameters.getInstance();
        var emptyBar = this.game.add.sprite(gameParameters.stageWidth/2-100, gameParameters.stageHeight/2, "preload_empty");
        emptyBar.anchor.setTo(0, 0.5);
        var loadingBar = this.game.add.sprite(gameParameters.stageWidth/2-100,gameParameters.stageHeight/2,"preload_full");
        loadingBar.anchor.setTo(0, 0.5);
        this.game.load.setPreloadSprite(loadingBar);

        this.game.load.image('logo', 'img/core_values_game_logo.png');
        this.game.load.image('background', 'img/start_screen_background.png');
        this.game.load.image('phaseSelectionBackground', 'img/phase_selection_background.png');
        this.game.load.image('pillarShadow', 'img/pillar_shadow.png');
        this.game.load.image('instructionsPolygon', 'img/instructions_polygon.png');
        this.game.load.image('smallPolygon', 'img/polygon_small.png');
        this.game.load.image('mediumPolygon', 'img/polygon_medium.png');

        this.game.load.image('smallCruiseShip', 'img/cruise_ship_phase.png');
        this.game.load.image('bigShipPhaseTwo', 'img/big_ship_phase_two.png');

        this.game.load.image('speedboatPhase', 'img/speedboat_phase.png');
        this.game.load.image('phaseBackgroundBoat', 'img/phase_background_boat.png');
        this.game.load.image('pillar1', 'img/pillar1.png');
        this.game.load.image('pillar2', 'img/pillar2.png');
        this.game.load.image('pillar3', 'img/pillar3.png');
        this.game.load.image('pillar4', 'img/pillar4.png');
        this.game.load.image('sliderScale', 'img/slider_scale.png');
        this.game.load.image('sliderControl', 'img/slider_control.png');
        this.game.load.image('valueBonus', 'img/value_bonus.png');
        this.game.load.image('phaseTwoDragInstructions', 'img/phase_two_drag_instructions.png');
        this.game.load.image('phaseTwoDragTarget', 'img/phase_two_drag_target.png');
        this.game.load.image('phaseTwoYacht', 'img/phase_two_yacht.png');
        this.game.load.image('smallArrow', 'img/small_arrow.png');
        this.game.load.image('bigArrow', 'img/big_arrow.png');
        this.game.load.image('dashedHexagon', 'img/dashed_hexagon.png');
        this.game.load.image('phaseTwoContinueButton', 'img/phase_two_continue_button.png');
        this.game.load.image('phaseTwoForcedContinueButton', 'img/phase_two_forced_continue_button.png');
        this.game.load.image('phaseTwoBonusValue', 'img/phase_two_bonus_value.png');
        this.game.load.image('phaseTwoTargetAnimation', 'img/phase_two_target_white.png');
        this.game.load.image('phaseTwoYachtBackground', 'img/phase_two_yacht_background.png');
        this.game.load.image('phaseTwoSelectionBackground', 'img/phase_two_b_selection_background.png');
        this.game.load.image('checkSign', 'img/check_sign.png');
        this.game.load.image('phaseThreeStormBackground', 'img/phase_three_storm_background.png');
        this.game.load.image('lifeRaftPhase', 'img/life_raft_phase.png');
        this.game.load.image('lifeRaftMoving', 'img/life_raft_moving.png');
        this.game.load.image('sunkenYacht', 'img/sunken_yacht.png');
        this.game.load.image('lifeBand', 'img/life_band.png');
        this.game.load.image('phaseThreeTapToClick', 'img/phase_three_tap_to_start.png');
        this.game.load.image('phaseThreeBLifeRaft', 'img/phase_three_life_raft.png');
        this.game.load.image('keyboardInstructions', 'img/keyboard_instructions.png');
        this.game.load.image('moveArrows', 'img/move_arrows.png');
        this.game.load.image('moveArrowsTwo', 'img/move_arrows_two.png');
        this.game.load.image('LOVE', 'img/LOVE.png');
        this.game.load.image('GOD', 'img/GOD.png');
        this.game.load.image('WORK', 'img/WORK.png');
        this.game.load.image('HEALTH', 'img/HEALTH.png');
        this.game.load.image('valueBonusContinue', 'img/value_bonus_continue.png');
        this.game.load.image('rescore', 'img/rescore.png');
        this.game.load.image('clickHexagonToStart', 'img/click_hexagon_to_start.png');
        this.game.load.image('phaseFourBackgroundTop', 'img/phase_four_background_top.png');
        this.game.load.image('phaseFourClickPillar', 'img/phase_four_click_pillar.png');
        this.game.load.image('phaseFourLifeRaft', 'img/phase_four_life_raft.png');
        this.game.load.image('phaseFourDogtag', 'img/phase_four_dogtag.png');
        this.game.load.image('phaseFourGod', 'img/phase_four_god.png');
        this.game.load.image('phaseFourLove', 'img/phase_four_love.png');
        this.game.load.image('phaseFourHealth', 'img/phase_four_health.png');
        this.game.load.image('phaseFourWork', 'img/phase_four_work.png');
        this.game.load.image('phaseFourGodInactive', 'img/phase_four_god_inactive.png');
        this.game.load.image('phaseFourLoveInactive', 'img/phase_four_love_inactive.png');
        this.game.load.image('phaseFourHealthInactive', 'img/phase_four_health_inactive.png');
        this.game.load.image('phaseFourWorkInactive', 'img/phase_four_work_inactive.png');
        this.game.load.image('phaseFourLifeRaftBig', 'img/phase_four_life_raft_big.png');


        this.game.load.spritesheet('squareContinueButton', 'img/square_continue_sprite.png', 282, 41);
        this.game.load.spritesheet('roundContinueButton', 'img/continue_sprite.png', 249, 32);
        this.game.load.spritesheet('nextButton', 'img/next_sprite.png', 159, 26);
        this.game.load.spritesheet('startButton', 'img/start_sprite.png', 159, 26);
        this.game.load.spritesheet('helpButton', 'img/help_button.png', 143, 24);
        this.game.load.spritesheet('exampleButton', 'img/example_sprite.png', 263, 45);
        this.game.load.spritesheet('phaseFourDropTarget', 'img/phase_four_drop_target_sprite_shadow.png', 124, 115);




        this.game.load.spritesheet('phaseIndicator', 'img/phase_indicator.png', 24, 24);

        this.game.load.audio("track_1", ["mus/01.mp3"]);

        this.game.load.text('xml', 'xml/gameData.xml');


    }

    create(){
        var xml:string = this.game.cache.getText('xml');
        var xmlParser:XMLParser = new XMLParser(xml);
        var pillars:Array<Pillar> = xmlParser.parsePillars();
        var values:Array<Value> = xmlParser.parseValues();
        for(var i in values){
            for(var j in pillars){
                if(values[i].getSmallGroup() == pillars[j].getGroup()){
                    pillars[j].addValue(values[i]);
                }
            }
        }
        this.gameState = new GameState(pillars, values);


        this.game.state.start("LandingScreen", true, false, this.gameState);

    }



}

