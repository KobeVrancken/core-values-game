/**
 * Created by kobe on 3/24/2015.
 */
/// <reference path= "../../../lib/phaser.d.ts" />
/// <reference path= "../permanent/GameState.ts" />
/// <reference path= "../phases/all/Pillar.ts" />
/// <reference path= "../phases/all/GamePhaseTracker.ts" />
/// <reference path= "../phases/all/MediumInstructions.ts" />
/// <reference path= "../event/GameEventPublisher.ts" />
/// <reference path= "../event/GameEvent.ts" />
/// <reference path= "../event/EndInstructionsEvent.ts" />
/// <reference path= "../phases/two/PhaseTwoPartBGameBoard.ts" />
/// <reference path= "PlayPhase.ts" />

class PhaseTwoPartB extends PlayPhase{



    private advancedValues:Array<Value>;

    constructor(game:Phaser.Game) {
        super(game);
    }

    init(gameState:GameState) {
        this.gameState = gameState;
        this.determineAdvancedValues();
    }

    public createGamePhaseTracker() {
        this.gamePhaseTracker = new GamePhaseTracker(this.game, 2, "PHASE 2", "The Yacht Phase", "speedboatPhase", "#FFFF00");
        this.gamePhaseTracker.setCopyRightColor("#1497d6");
    }

    public drawBackgroundImage():void{
        var sea:VideoReference = new VideoReference("video_sea");
        sea.hideVideo();
        var backgroundSprite:Phaser.Sprite = this.game.add.sprite(90, 237, "phaseTwoYachtBackground");
    }


    public createMediumInstructions():MediumInstructions{
        return new MediumInstructions(this.game, "PHASE 2", ['In Stage 2 of the Yacht Phase, you will need to choose the answer' +
        ' that MOST APPLIES to each Value in your life.\n\nBe HONEST with yourself !!!', 'You have completed well over half of the' +
        ' Abundance Coaching Values Game. \n\n Feel free to stretch or take a short break.']);
    }

    public startGameplayCleanup():void{
        //do nothing
    }

    public drawGameBoard():void{
        var phaseTwoPartBGameBoard:PhaseTwoPartBGameBoard = new PhaseTwoPartBGameBoard(this.game, new Phaser.Group(this.game), this.gamePhaseTracker);
        phaseTwoPartBGameBoard.subscribe(this.gameState);
        phaseTwoPartBGameBoard.subscribe(this.gamePhaseTracker);
        phaseTwoPartBGameBoard.setAdvancedValues(this.advancedValues);
        phaseTwoPartBGameBoard.draw();
    }

    private determineAdvancedValues():void{
        this.advancedValues = new Array<Value>();
        var valueScores:Array<ValueScore> = this.gameState.getValueScores();
        for(var i in valueScores){
            var valueScore:ValueScore = valueScores[i];
            if(valueScore.getPhaseOneRating()>8.5){
                this.advanceAutomatically(valueScore.getValue());
            }else if(valueScore.getRealTotal()>=0){ //TODO DIT MOET 1 ZIJN MAAR LASTIG VOOR TESTS!
                this.advancedValues.push(valueScore.getValue());
            }
        }

    }

    private advanceAutomatically(value:Value){
        console.log("Advancing value automatically: "+value);
        this.gameState.accept(new ValueRatedEvent(this.gameState, value, 11, 0));
    }
}