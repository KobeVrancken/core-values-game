/**
 * Created by kobe on 3/24/2015.
 */
/// <reference path= "../../../lib/phaser.d.ts" />
/// <reference path= "../permanent/GameState.ts" />
/// <reference path= "../phases/all/Pillar.ts" />
/// <reference path= "../phases/all/GamePhaseTracker.ts" />
/// <reference path= "../phases/all/MediumInstructions.ts" />
/// <reference path= "../event/GameEventPublisher.ts" />
/// <reference path= "../event/GameEvent.ts" />
/// <reference path= "../event/EndInstructionsEvent.ts" />
/// <reference path= "../phases/three/PhaseThreePartBGameBoard.ts" />
/// <reference path= "PlayPhase.ts" />
/// <reference path= "../phases/all/BigInstructions.ts" />


class PhaseThreePartB extends PlayPhase{


    private advancedValues:Array<Value>;
    private lifeRaft:Phaser.Sprite;
    private boatControlsEnabled:boolean = false;

    constructor(game:Phaser.Game) {
        super(game);
    }

    init(gameState:GameState) {
        this.gameState = gameState;
        this.determineAdvancedValues();
    }

    public createGamePhaseTracker() {
        this.gamePhaseTracker = new GamePhaseTracker(this.game, 3, "PHASE 3", "The Life Raft Phase", "lifeRaftPhase", "#FFFF00");
        this.gamePhaseTracker.setCopyRightColor("#1497d6");
    }

    public drawBackgroundImage():void{
        var sea:VideoReference = new VideoReference("video_waves_horizon");
        sea.showVideo();
        sea.playVideo();
        this.drawLifeRaft();
    }

    private drawLifeRaft():void{
        this.lifeRaft = this.game.add.sprite(300, 530, "phaseThreeBLifeRaft");
    }

    public createMediumInstructions():MediumInstructions{
        var instructions:MediumInstructions = new MediumInstructions(this.game, "PHASE 3", ['The storm has calmed and some of your values have made it with you in your life raft. Other values are drifting in the sea.\n\n' +
        'It\'s the last opportunity to save more of your important values before it\'s too late.',
        'Use the arrow keys on your keyboard or your mouse / touchpad to steer your life raft to the RIGHT and LEFT to save the values that matter most to you.' +
        '\n\nDo NOT click your mouse or touchpad.',
        'You will NOT be able to save them all so choose to save the values that are most important to you.']);
        instructions.enableKeyboardInstructions();
        return instructions;
    }

    public startGameplayCleanup():void{

    }

    public drawGameBoard():void{
        var phaseThreePartBGameBoard:PhaseThreePartBGameBoard = new PhaseThreePartBGameBoard(this.game, new Phaser.Group(this.game), this.gameState, this.lifeRaft);
        phaseThreePartBGameBoard.subscribe(this.gameState);
        phaseThreePartBGameBoard.subscribe(this.gamePhaseTracker);
        phaseThreePartBGameBoard.setAdvancedValues(this.advancedValues);
        phaseThreePartBGameBoard.draw();
        this.enableBoatControls();
    }

    private enableBoatControls():void{
        this.boatControlsEnabled = true;
        this.game.input.addMoveCallback(this.enableMouse, this);
    }

    private determineAdvancedValues():void{
        this.advancedValues = new Array<Value>();
        var valueScores:Array<ValueScore> = this.gameState.getValueScores();
        for(var i in valueScores){
            var valueScore:ValueScore = valueScores[i];
            /*if(valueScore.getPhaseOneRating()>8.5){
             this.advanceAutomatically(valueScore.getValue());
             }else*/
            if(valueScore.getRealTotal()>=0){ //TODO DIT MOET x ZIJN MAAR LASTIG VOOR TESTS!
                this.advancedValues.push(valueScore.getValue());
            }
        }
        //todo which values to advance??
    }

    private cursorKeys:Phaser.CursorKeys;
    private actualVelocity:number = 2;
    private velocity:number = 0;
    static MAX_X:number = 830;
    static MIN_X:number = 120;

    private mouseEnabled:boolean = false;

    private enableMouse() {
        this.mouseEnabled = true;
    }

    private disableMouse(){
        this.mouseEnabled = false;
    }

    update(){
        if(this.boatControlsEnabled) {
            this.velocity = 0;
            if(this.mouseEnabled){
                if(this.game.input.mousePointer.x<this.lifeRaft.x+this.lifeRaft.width/3){
                    this.velocity = -this.actualVelocity;
                }else if(this.game.input.mousePointer.x>this.lifeRaft.x+this.lifeRaft.width*2/3){
                    this.velocity = this.actualVelocity;
                }
            }
            this.cursorKeys = this.game.input.keyboard.createCursorKeys();
            if (this.cursorKeys.left.isDown) {
                this.disableMouse();
                this.velocity = -this.actualVelocity;
            } else if (this.cursorKeys.right.isDown) {
                this.disableMouse();
                this.velocity = this.actualVelocity;
            }
            this.lifeRaft.x += this.velocity;
            if (this.lifeRaft.x > PhaseThreePartB.MAX_X) this.lifeRaft.x = PhaseThreePartB.MAX_X;
            if (this.lifeRaft.x < PhaseThreePartB.MIN_X) this.lifeRaft.x = PhaseThreePartB.MIN_X;
        }
    }
}