/**
 * Created by kobe on 3/24/2015.
 */
/// <reference path= "../../../lib/phaser.d.ts" />
/// <reference path= "../permanent/GameState.ts" />
/// <reference path= "../phases/all/Pillar.ts" />
/// <reference path= "../phases/all/GamePhaseTracker.ts" />
/// <reference path= "../phases/all/MediumInstructions.ts" />
/// <reference path= "../event/GameEventPublisher.ts" />
/// <reference path= "../event/GameEvent.ts" />
/// <reference path= "../event/EndInstructionsEvent.ts" />
/// <reference path= "../phases/one/PhaseOneGameBoard.ts" />

class PlayPhase implements GameEventSubscriber{


    public game:Phaser.Game;
    public gameState:GameState;
    public gamePhaseTracker:GamePhaseTracker;

    constructor(game:Phaser.Game) {
        this.game = game;
    }



    create(){
        this.createGamePhaseTracker();
        this.gameState.subscribe(this.gamePhaseTracker);
        this.gamePhaseTracker.draw();
        this.gamePhaseTracker.getPauseButton().hide();
        this.gamePhaseTracker.getScoreButton().hide();
        this.drawBackgroundImage();
        this.drawMediumInstructions();
        this.drawBigInstructions();
    }

    public drawBigInstructions():void{
        //TO OVERRIDE
    }

    private drawMediumInstructions():void{
        var mediumInstructions:MediumInstructions = this.createMediumInstructions();
        if(mediumInstructions != null) {
            mediumInstructions.draw();
            mediumInstructions.subscribe(this);
        }else{
            this.startPhaseGameplay();
            this.gamePhaseTracker.drawDelayed();
        }
    }

    accept(event:GameEvent) {
        if(event instanceof EndInstructionsEvent) {
            this.startPhaseGameplay();
            this.gamePhaseTracker.drawDelayed();
        }
    }

    private startPhaseGameplay():void{
        this.startGameplayCleanup();
        this.gamePhaseTracker.getPauseButton().show();
        this.gamePhaseTracker.getScoreButton().show();
        this.drawGameBoard();
    }


    public createGamePhaseTracker() {
        throw new Error("Error: extended class must override this method.");
    }

    public drawBackgroundImage():void{
        throw new Error("Error: extended class must override this method.");
    }

    public createMediumInstructions():MediumInstructions{
        throw new Error("Error: extended class must override this method.");

    }

    public startGameplayCleanup():void{
        throw new Error("Error: extended class must override this method.");
    }

    public drawGameBoard():void{
        throw new Error("Error: extended class must override this method.");
    }
}