/**
 * Created by kobe on 3/24/2015.
 */
/// <reference path= "../../../lib/phaser.d.ts" />
/// <reference path= "../permanent/GameState.ts" />
/// <reference path= "../phases/all/Pillar.ts" />
/// <reference path= "../phases/all/GamePhaseTracker.ts" />
/// <reference path= "../phases/all/MediumInstructions.ts" />
/// <reference path= "../event/GameEventPublisher.ts" />
/// <reference path= "../event/GameEvent.ts" />
/// <reference path= "../event/EndInstructionsEvent.ts" />
/// <reference path= "../phases/two/PhaseTwoGameBoard.ts" />
/// <reference path= "PlayPhase.ts" />

class PhaseTwo extends PlayPhase{


    private rockForwardTween:Phaser.Tween;
    private rockBackwardTween:Phaser.Tween;
    private cruiseShipSprite:Phaser.Sprite;
    private advancedValues:Array<Value>;
    private static MAX_VALUES_PER_PILLAR = 50;
    private static MIN_VALUES_PER_PILLAR = 20;

    constructor(game:Phaser.Game) {
        super(game);
    }

    init(gameState:GameState) {
        this.gameState = gameState;
        this.determineAdvancedValues();
    }

    public createGamePhaseTracker() {
        this.gamePhaseTracker = new GamePhaseTracker(this.game, 2, "PHASE 2", "The Yacht Phase", "speedboatPhase", "#FFFF00");
        this.gamePhaseTracker.setCopyRightColor("#1497d6");
    }

    public drawBackgroundImage():void{
        var sea:VideoReference = new VideoReference("video_sea");
        sea.showVideo();
        sea.playVideo();
        this.drawRockingBoat();
    }

    private drawRockingBoat(){
        this.cruiseShipSprite = this.game.add.sprite(500, 400, "bigShipPhaseTwo");
        this.cruiseShipSprite.anchor.set(0.5, 0.5);
        this.startRockForward();
    }

    private startRockForward(){
        this.rockForwardTween = this.game.add.tween(this.cruiseShipSprite).to({angle:'+2'}, 3000, Phaser.Easing.Quadratic.InOut, true, 100);
        this.rockForwardTween.onComplete.add(this.startRockBackward, this);
    }

    private startRockBackward(){
        this.rockBackwardTween = this.game.add.tween(this.cruiseShipSprite).to({angle:'-2'}, 3000, Phaser.Easing.Quadratic.InOut, true, 100);
        this.rockBackwardTween.onComplete.add(this.startRockForward, this);
    }


    public createMediumInstructions():MediumInstructions{
        return new MediumInstructions(this.game, "PHASE 2", ['You are continuing your holiday by bringing your most important health and wellbeing values on your luxury yacht. Some' +
        ' of the values you bring will be important to you and some will be Essential Core Values.', 'In Stage 1 of the Yacht Phase you will need to choose values that resonate with you in strategically organized groups.',
            'You have an option to choose up to 3 Bonus Values in Phase 2 Stage 1. Just click on the Bonus Values button and you will have an extra value to choose in that group.']);
    }

    public startGameplayCleanup():void{
        //do nothing
    }

    public drawGameBoard():void{
        var phaseTwoGameBoard:PhaseTwoGameBoard = new PhaseTwoGameBoard(this.game, new Phaser.Group(this.game), this.gameState);
        phaseTwoGameBoard.subscribe(this.gameState);
        phaseTwoGameBoard.subscribe(this.gamePhaseTracker);
        phaseTwoGameBoard.setAdvancedValues(this.advancedValues);
        phaseTwoGameBoard.draw();
    }

    private determineAdvancedValues():void{
        this.advancedValues = new Array<Value>();
        var pillars:Array<Pillar> = this.gameState.getPillars();
        for(var i in pillars){
            var sortedValues:Array<ValueScore> = this.gameState.getValueScoresByPillar(pillars[i]);
            sortedValues.sort(ValueScore.comparePhaseOne);
            var advanced:number = 0;
            var j:number = sortedValues.length;
            while(--j >= 0 && advanced < PhaseTwo.MAX_VALUES_PER_PILLAR){
                var currentValueScore:ValueScore = sortedValues[j];
                var currentValue:Value = currentValueScore.getValue();
                if(currentValueScore.getPhaseOneRating()<= 7){
                    if(advanced<PhaseTwo.MIN_VALUES_PER_PILLAR){
                        this.gameState.accept(new ValueRatedEvent(this.gameState, currentValue, 0, 0.5));
                    }else{
                        break;
                    }
                }
                this.advancedValues.push(currentValue);
                advanced++;
            }

        }
    }
}