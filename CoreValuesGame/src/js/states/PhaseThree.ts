/**
 * Created by kobe on 3/24/2015.
 */
/// <reference path= "../../../lib/phaser.d.ts" />
/// <reference path= "../permanent/GameState.ts" />
/// <reference path= "../phases/all/Pillar.ts" />
/// <reference path= "../phases/all/GamePhaseTracker.ts" />
/// <reference path= "../phases/all/MediumInstructions.ts" />
/// <reference path= "../event/GameEventPublisher.ts" />
/// <reference path= "../event/GameEvent.ts" />
/// <reference path= "../event/EndInstructionsEvent.ts" />
/// <reference path= "../phases/three/PhaseThreeGameBoard.ts" />
/// <reference path= "PlayPhase.ts" />
/// <reference path= "../phases/all/BigInstructions.ts" />


class PhaseThree extends PlayPhase{


    private advancedValues:Array<Value>;
    private backgroundSprite:Phaser.Sprite;
    private sunkenYacht:Phaser.Sprite;

    constructor(game:Phaser.Game) {
        super(game);
    }

    init(gameState:GameState) {
        this.gameState = gameState;
        this.determineAdvancedValues();
    }

    public createGamePhaseTracker() {
        this.gamePhaseTracker = new GamePhaseTracker(this.game, 3, "PHASE 3", "The Life Raft Phase", "lifeRaftPhase", "#FFFF00");
        this.gamePhaseTracker.setCopyRightColor("#1497d6");
    }

    public drawBackgroundImage():void{
        this.backgroundSprite = this.game.add.sprite(90, 237, "phaseThreeStormBackground");
    }

    public drawBigInstructions():void{
        var instructions:Instructions = new BigInstructions(this.game, "PHASE 3", ["Get ready for the next phase in your core values adventure! In this stage," +
        " you'll be forced to make difficult decisions as you keep some Values, and leave some behind.\n\n" +
        "You'll need to think FAST and react to your feelings and emotions. You're now well on your journey to finding out what matters the most to you." +
        "\n\nPhase 3 will take several minutes, but it could be the most powerful minutes of your life!"]);
        instructions.draw();
    }


    public createMediumInstructions():MediumInstructions{
        return new MediumInstructions(this.game, "PHASE 3", ['While on your luxury yacht, a MONSTER STORM hits!\n\n' +
        'The waves are crashing hard, and your communication goes down before you can call for help!', 'Suddenly an enormous wave smashes' +
        'into your yacht and tips it over.\n\n' +
        'Before your yacht sinks, you get on your life raft. However, your important values are thrown into the ocean and are at risk of drowning!',
        'You CANNOT save all your values; so you must save the Values that you can\'t live without first!\n\n' +
        'Be quick, you don\'t have much time before your values drown!']);
    }

    public startGameplayCleanup():void{
        this.backgroundSprite.destroy();
        var sea:VideoReference = new VideoReference("video_waves");
        sea.showVideo();
        sea.playVideo();
        this.drawSunkenYacht();
    }

    private drawSunkenYacht(){
        this.sunkenYacht = this.game.add.sprite(150, 250, "sunkenYacht");
        var rockForwardTween:Phaser.Tween = this.game.add.tween(this.sunkenYacht).to({angle:'-5'}, 2500, Phaser.Easing.Quadratic.InOut , true, 0, -1);
        rockForwardTween.yoyo(true);
    }


    public drawGameBoard():void{
        var phaseThreeGameBoard:PhaseThreeGameBoard = new PhaseThreeGameBoard(this.game, new Phaser.Group(this.game), this.gameState);
        phaseThreeGameBoard.subscribe(this.gameState);
        phaseThreeGameBoard.subscribe(this.gamePhaseTracker);
        phaseThreeGameBoard.setAdvancedValues(this.advancedValues);
        phaseThreeGameBoard.draw();
    }

    private determineAdvancedValues():void{
        this.advancedValues = new Array<Value>();
        var valueScores:Array<ValueScore> = this.gameState.getValueScores();
        for(var i in valueScores){
            var valueScore:ValueScore = valueScores[i];
            /*if(valueScore.getPhaseOneRating()>8.5){
                this.advanceAutomatically(valueScore.getValue());
            }else*/
            if(valueScore.getRealTotal()>=0){ //TODO DIT MOET x ZIJN MAAR LASTIG VOOR TESTS!
                this.advancedValues.push(valueScore.getValue());
            }
        }
        //todo which values to advance??
    }
}