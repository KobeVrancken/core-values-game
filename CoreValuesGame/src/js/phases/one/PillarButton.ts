/**
 * Created by kobe on 3/23/2015.
 */

/// <reference path= "../../../../lib/phaser.d.ts" />
/// <reference path= "../all/Pillar.ts" />
/// <reference path= "../../permanent/GameState.ts" />
/// <reference path= "../../permanent/GameParameters.ts" />

class PillarButton{

    private pillar:Pillar;
    private game:Phaser.Game;
    private group:Phaser.Group;
    private hexagonSprite:Phaser.Sprite;
    private gameState:GameState;
    private gameParameters:GameParameters = GameParameters.getInstance();
    private rescore:boolean = false;

    private fontStyle:any = {
        font: 'bold 17px Arial',
        fill: '#FFFFFF'
    };

    constructor(game: Phaser.Game, gameState:GameState, group: Phaser.Group, pillar:Pillar) {
        this.game = game;
        this.gameState = gameState;
        this.pillar = pillar;
        this.group = group;
    }

    public enableRescore():void{
        this.rescore = true;
    }

    draw(x:number, y:number):void{
        this.drawHexagon(x, y, 130, 150, 35);
        this.drawName(x, y, 130, 150);
        if(this.rescore) this.drawRescoreButton(x, y);
    }

    drawHexagon(x:number, y:number, trWidth:number, trHeight:number, topOffset:number):void{
        var shadowSprite:Phaser.Sprite = new Phaser.Sprite(this.game, x-6, y-4,"pillarShadow");
        this.group.add(shadowSprite);
        var hexagon:Phaser.Graphics = new Phaser.Graphics(this.game, 0, 0);
        this.hexagonSprite = new Phaser.Sprite(this.game, x, y);
        this.hexagonSprite.addChild(hexagon);
        this.group.add(this.hexagonSprite);
        if(!this.rescore) {
            this.hexagonSprite.inputEnabled = true;
            this.hexagonSprite.input.useHandCursor = true;
            this.hexagonSprite.events.onInputUp.add(this.openPhase, this);
        }
        var points: Phaser.Point[] = [];
        points.push(new Phaser.Point(0,trHeight-topOffset));
        points.push(new Phaser.Point(0,topOffset));
        points.push(new Phaser.Point(trWidth/2, 0));
        points.push(new Phaser.Point(trWidth,topOffset));
        points.push(new Phaser.Point(trWidth,trHeight-topOffset));
        points.push(new Phaser.Point(trWidth/2, trHeight));
        if(!this.rescore) {
            hexagon.beginFill(this.gameParameters.convertColorStringToNumber(this.pillar.getColor()), 1);
        }else{
            hexagon.beginFill(this.gameParameters.convertColorStringToNumber("#444c56"), 0.8);
        }
        hexagon.drawPolygon(points);
    }

    drawName(x:number, y:number, trWidth:number, trHeight:number):void{
        if(this.rescore) y = y - 10;
        var text:Phaser.Text = new Phaser.Text(this.game, x + trWidth/2, y + trHeight/2, this.pillar.getName(), this.fontStyle);
        text.anchor.set(0.5, 0.5);
        if(this.rescore) text.alpha = 0.6;
        text.align = 'center';
        text.wordWrap = true;
        text.wordWrapWidth = 110;
        this.group.add(text);
    }

    private drawRescoreButton(x:number, y:number):void{
        var rescoreButton:Phaser.Sprite = new Phaser.Sprite(this.game, x+5, y+95, "rescore");
        this.group.add(rescoreButton);
        rescoreButton.inputEnabled = true;
        rescoreButton.width = 120;
        rescoreButton.input.useHandCursor = true;
        rescoreButton.events.onInputUp.add(this.openPhase, this);
    }

    private openPhase():void{
        this.game.state.start("PillarPhase", true, false, this.gameState, this.pillar);
    }


}