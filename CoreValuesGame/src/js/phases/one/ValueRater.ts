/// <reference path= "../../../../lib/phaser.d.ts" />
/// <reference path= "../all/Value.ts" />
/// <reference path= "../../permanent/GameParameters.ts" />
/// <reference path= "../../event/GameEventPublisher.ts" />
/// <reference path= "../../event/GameEventSubscriber.ts" />
/// <reference path= "../../event/ValueRatedEvent.ts" />
/// <reference path= "../../event/ScorePenaltyEvent.ts" />
/// <reference path= "../all/ScoreTimer.ts" />

class ValueRater implements GameEventPublisher, GameEventSubscriber{


    private game:Phaser.Game;
    private group:Phaser.Group;
    private value:Value;
    private x:number;
    private y:number;
    private gameParameters:GameParameters = GameParameters.getInstance();
    private slider:Phaser.Sprite;

    private updateTimer:Phaser.Timer;
    private sliderStartX:number = 54;
    private sliderEndX:number = 356;
    private yellowBar:Phaser.Graphics;

    private scoreTimer:ScoreTimer;

    private subList:Array<GameEventSubscriber> = new Array<GameEventSubscriber>();
    private exampleButton:Phaser.Button;
    private valueName:Phaser.Text;

    constructor(game:Phaser.Game, group:Phaser.Group, value:Value){
        this.game = game;
        this.group = new Phaser.Group(this.game);
        group.add(this.group);
        this.group = group;
        this.value = value;
    }

    public draw(x:number, y:number):void{
        this.group.x  = x;
        this.group.y = y;
        this.x = x;
        this.y = y;
        this.drawBlockingSprite();
        this.drawBackground();
        this.drawInstruction();
        this.drawValueName();
        this.drawExampleButton();
        this.drawSlider();
        this.drawScoreTimer();
    }

    private drawScoreTimer():void{
        this.scoreTimer = new ScoreTimer(this.game, this.game.add.group());
        this.scoreTimer.draw(1095, 150);
        this.scoreTimer.start(20);
        this.scoreTimer.subscribe(this);
        this.scoreTimer.setTickCutoff(10);
    }

    private drawBlockingSprite():void{
        var blockingSprite:Phaser.Graphics = new Phaser.Graphics(this.game, 0, 0);
        blockingSprite.beginFill(0x000000, 0);
        blockingSprite.drawRect(0, 0, this.gameParameters.stageWidth, this.gameParameters.stageHeight);
        var actualSprite = new Phaser.Sprite(this.game, -this.x, -this.y);
        actualSprite.addChild(blockingSprite);
        actualSprite.inputEnabled = true;
        this.group.add(actualSprite);
    }


    private drawBackground(){
        var instructionsPolygon:Phaser.Sprite = new Phaser.Sprite(this.game, 0, 0, "mediumPolygon");
        this.group.add(instructionsPolygon);
    }


    private drawInstruction():void{
        var text:Phaser.Text = new Phaser.Text(this.game, 205, 110, "With 10 Being the Highest, Score How Important\n" +
        "This value is to you in your Life Right Now", this.gameParameters.font_valueRater);
        text.align = 'center';
        text.anchor.set(0.5, 0);
        this.group.add(text);
    }

    private drawValueName():void{
        this.valueName = new Phaser.Text(this.game, 205, 190, this.value.getValueName(), this.gameParameters.font_valueRaterTitle);
        this.valueName.align = 'center';
        this.valueName.anchor.set(0.5, 0);
        this.group.add(this.valueName);
        this.valueName.wordWrap = true;
        this.valueName.wordWrapWidth = 325;
    }

    private drawExampleButton(){
        this.exampleButton = new Phaser.Button(this.game, 205, 310, 'exampleButton', this.showExample, this, 1, 0);
        this.exampleButton.anchor.set(0.5,0);
        this.group.add(this.exampleButton);
    }

    private drawSlider():void{
        var scale:Phaser.Sprite = new Phaser.Sprite(this.game, 205, 260, "sliderScale");
        scale.anchor.set(0.5,0);
        this.group.add(scale);
        var emptyBar:Phaser.Graphics = new Phaser.Graphics(this.game, 0, 0);
        emptyBar.beginFill(0x000000, 0.2);
        emptyBar.drawRect(52, 241, 305, 10);
        this.group.add(emptyBar);
        this.yellowBar = new Phaser.Graphics(this.game, 52, 0);
        this.yellowBar.beginFill(0xFFFF00, 1);
        this.yellowBar.drawRect(0, 241, 305, 10);
        this.group.add(this.yellowBar);
        this.yellowBar.width = 0;

        this.slider = new Phaser.Sprite(this.game, 54, 232, "sliderControl");
        this.slider.anchor.set(0.5, 0);
        this.group.add(this.slider);

        var boundsRectangle:Phaser.Rectangle = new Phaser.Rectangle(54, 200, 330, 100);
        this.slider.inputEnabled = true;
        this.slider.input.useHandCursor = true;
        this.slider.input.enableDrag(false, false, true, 0, boundsRectangle);
        this.slider.input.allowVerticalDrag = false;
        this.slider.events.onDragStart.add(this.startDrag, this);
        this.slider.events.onDragStop.add(this.stopDrag, this);
        this.updateTimer = this.game.time.create();

    }

    private showExample():void{
        this.exampleButton.input.enabled = false;
        this.gameParameters.fadeObjectOut(this.game, this.exampleButton, 500, 0);
        var tween:Phaser.Tween = this.game.add.tween(this.valueName).to({x: this.valueName.x, y: this.valueName.y-35}, 1000, Phaser.Easing.Cubic.Out, true);
        var shortDescription:Phaser.Text = new Phaser.Text(this.game, 205, 188, this.value.getLongDefinition()+".", this.gameParameters.font_valueRaterItalic);
        shortDescription.anchor.set(0.5, 0);
        shortDescription.wordWrap = true;
        shortDescription.wordWrapWidth = 325;
        shortDescription.align = 'center';
        this.group.add(shortDescription);
        shortDescription.alpha = 0;
        this.gameParameters.fadeObjectIn(this.game, shortDescription, 500, 1000);

        var example:Phaser.Text = new Phaser.Text(this.game, 205, 297, this.value.getExample()+".", this.gameParameters.font_valueRaterItalic);
        example.anchor.set(0.5, 0);
        example.wordWrap = true;
        example.wordWrapWidth = 325;
        example.align = 'center';
        this.group.add(example);
        example.alpha = 0;
        this.gameParameters.fadeObjectIn(this.game, example, 500, 1000);
    }

    private startDrag():void{
        this.updateTimer.loop(50, this.updateRectangle, this);
        this.updateTimer.start();
    }

    private stopDrag():void{
        this.updateTimer.stop();
        var percentage:number = (this.slider.x-this.sliderStartX)*10/(this.sliderEndX-this.sliderStartX);
        var score: number = Math.round(percentage*10)/10;
        this.group.destroy();
        this.scoreTimer.stop();

        var realScore = 0;
        if(score>9.5){
            realScore = 3;
        }else if(score>9){
            realScore = 2;
        }else if(score>8.5){
            realScore = 1.5;
        }else if(score>8){
            realScore = 1;
        }else if(score>7){
            realScore = 0.5;
        }

        this.publish(new ValueRatedEvent(this, this.value, score, realScore));
    }

    private updateRectangle():void{
        this.yellowBar.width = this.slider.x-this.sliderStartX;
        this.yellowBar.x = 52;
    }

    subscribe(sub:GameEventSubscriber):void {
        this.subList.push(sub);
    }

    publish(gameEvent:GameEvent):void {
        for(var i in this.subList){
            this.subList[i].accept(gameEvent);
        }
    }

    accept(gameEvent:GameEvent) {
        if(gameEvent instanceof TimerEndEvent){
            this.stopDrag();
        }else {
            this.publish(gameEvent);
        }
    }

}