/// <reference path= "../../../../lib/phaser.d.ts" />
/// <reference path= "../all/Pillar.ts" />
/// <reference path= "../../permanent/GameState.ts" />
/// <reference path= "ValueButton.ts" />

/// <reference path= "../../event/GameEventPublisher.ts" />
/// <reference path= "../../event/GameEventSubscriber.ts" />
/// <reference path= "../../event/ValueRatedEvent.ts" />
/// <reference path= "../../event/TimerTickEvent.ts" />
/// <reference path= "../../event/ScorePenaltyEvent.ts" />

/// <reference path= "../../event/GameProgressEvent.ts" />
/// <reference path= "../../permanent/GameParameters.ts" />

class PhaseOneGameBoard implements GameEventSubscriber, GameEventPublisher{

    private pillar:Pillar;
    private game:Phaser.Game;
    private group:Phaser.Group;
    private values:Array<Value>;
    private buttonGroup:Phaser.Group;
    private backgroundSprite:Phaser.Sprite;
    private subs:Array<GameEventSubscriber> = new Array<GameEventSubscriber>();
    private totalButtons:number = 0;
    public ratedButtons:number = 0;
    private gameParameters:GameParameters = GameParameters.getInstance();
    private gameState:GameState;

    private hexagonToStartSprite:Phaser.Sprite;
    private hexagonToStartText:Phaser.Text;
    private hexagonToStartTextBlack:Phaser.Text;
    private destroyEvent:Phaser.SignalBinding;

    constructor(game:Phaser.Game, pillar:Pillar, gameState:GameState){
        this.pillar = pillar;
        this.game = game;
        this.group = game.add.group();
        this.values = pillar.getValues();
        this.buttonGroup = new Phaser.Group(this.game);
        this.gameState = gameState;
    }

    public draw():void{
        this.drawBackground();
        this.drawText();
        this.drawButtons();
        this.drawHexagonToStart();
    }

    private drawHexagonToStart(){
        this.hexagonToStartSprite = new Phaser.Sprite(this.game, 405, 180, "clickHexagonToStart");
        this.group.add(this.hexagonToStartSprite);

        this.hexagonToStartTextBlack = new Phaser.Text(this.game, 445, 190, "CLICK ON ANY OF\nTHE HEXAGONS TO START\n\n\n\n\n\n\n", this.gameParameters.font_hexagonToStartBlack);
        this.group.add(this.hexagonToStartTextBlack);
        this.hexagonToStartTextBlack.align = "center";
        this.hexagonToStartTextBlack.lineSpacing = -7;

        this.hexagonToStartText = new Phaser.Text(this.game, 445, 190, "CLICK ON ANY OF\nTHE HEXAGONS TO START\n\n\n\n\n\n\n\n\n", this.gameParameters.font_hexagonToStart);
        this.group.add(this.hexagonToStartText);
        this.hexagonToStartText.align = "center";
        this.hexagonToStartText.lineSpacing = -7;

        var fadeTween:Phaser.Tween = this.game.add.tween(this.hexagonToStartText).to( { alpha: 0.2 }, 600, Phaser.Easing.Linear.None, true, 0, -1);
        fadeTween.yoyo(true);
        this.destroyEvent = this.game.input.onDown.add(this.removeClickToStart, this);
    }

    private removeClickToStart():void{
        this.hexagonToStartSprite.destroy();
        this.hexagonToStartText.destroy();
        this.hexagonToStartTextBlack.destroy();
        this.destroyEvent.active = false;
    }

    private drawText():void{
        var x:number = 680;
        var y:number = 240;
        var pillarText:Phaser.Text = new Phaser.Text(this.game, x, y, this.pillar.getLongName(), this.gameParameters.getPillarPhaseFont(this.pillar.getColor()));
        this.group.add(pillarText);
        var coreValuesText:Phaser.Text = new Phaser.Text(this.game, x+pillarText.width+15, y, "Core Values", this.gameParameters.getPillarPhaseFont("#FFFFFF"));
        this.group.add(coreValuesText);
    }

    private drawBackground():void{
        this.backgroundSprite = new Phaser.Sprite(this.game, 45, 212, "pillar"+this.pillar.getGroup());
        this.group.add(this.backgroundSprite);
    }

    private drawButtons():void{
        this.group.add(this.buttonGroup);
        var map:Array<Array<string>> = this.pillar.getMap();
        var horizontalSpacing = 75;
        var verticalSpacing = 64;
        var startX = 50;
        var startY = 218;
        var y = startY;
        var val = 0;
        for (var i in map) {
            var x = startX;
            if (i % 2 == 1) {
                x += horizontalSpacing / 2;
            }
            for (var j in map[i]) {
                if (map[i][j] === "1" || map[i][j] === "2" ) {
                    var valueButton:ValueButton;
                    if(map[i][j] === "2") {
                        valueButton = this.drawValueButton(this.values[0]);
                        valueButton.setValueBonus();
                    }else {
                        valueButton = this.drawValueButton(this.values[val++]);
                    }
                    valueButton.draw(x, y);
                }
                x += horizontalSpacing;
            }
            y += verticalSpacing;
        }
    }


    private drawValueButton(value:Value):ValueButton{
        this.totalButtons++;
        var valueButton:ValueButton = new ValueButton(this.game, this.buttonGroup, value, this.pillar.getColor(), this);
        valueButton.subscribe(this);
        return valueButton;
    }

    private visibility:boolean = true;

    public toggleVisibility():void{
        this.checkFinished();
        this.visibility = !this.visibility;
        this.buttonGroup.visible = this.visibility;
        this.backgroundSprite.visible = this.visibility;
    }

    private checkFinished():void{
        if(this.ratedButtons == this.totalButtons){
            this.game.state.start("PillarSelection", true, false, this.gameState);
        }
    }


    accept(gameEvent:GameEvent) {
        if(gameEvent instanceof ValueRatedEvent){
            this.publish(gameEvent);
            this.ratedButtons++;
            var gameProgressEvent:GameProgressEvent = new GameProgressEvent(this, this.ratedButtons*100/this.totalButtons);
            this.publish(gameProgressEvent);
            this.checkFinished();
        }else if(gameEvent instanceof TimerTickEvent){
            var penalty = new ScorePenaltyEvent(this, this.pillar, 1);
            this.publish(penalty);
        }
    }

    subscribe(sub:GameEventSubscriber):void {
        this.subs.push(sub);
    }

    publish(gameEvent:GameEvent):void {
        for(var i in this.subs){
            this.subs[i].accept(gameEvent);
        }
    }


}