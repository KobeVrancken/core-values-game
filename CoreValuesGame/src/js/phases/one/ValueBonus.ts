/// <reference path= "../../../../lib/phaser.d.ts" />
/// <reference path= "../../external/VideoReference.ts" />
/// <reference path= "../../permanent/GameParameters.ts" />
/// <reference path= "../all/Pillar.ts" />
/// <reference path= "PhaseOneGameBoard.ts" />


class ValueBonus {

    private game:Phaser.Game;
    private group:Phaser.Group;
    private videoReference:VideoReference = new VideoReference("video_stars")
    private gameParameters:GameParameters = GameParameters.getInstance();
    private initialText:Phaser.Text;
    private valueBonusGraphic:Phaser.Sprite;
    private pillar:Pillar;
    private board:PhaseOneGameBoard;

    constructor(game:Phaser.Game, pillar:Pillar, board:PhaseOneGameBoard){
        this.game = game;
        this.group = this.game.add.group();
        this.pillar = pillar;
        this.board = board;
    }

    public draw():void{
        this.drawBlockingSprite();
        this.valueBonusGraphic = new Phaser.Sprite(this.game, 300, 250, "valueBonus");
        this.group.add(this.valueBonusGraphic);
        this.videoReference.showVideo();
        this.videoReference.playVideo();
        this.drawInitialText();
    }

    public drawBlockingSprite():void{
        var blockingSprite:Phaser.Graphics = new Phaser.Graphics(this.game, 0, 0);
        blockingSprite.beginFill(0x1E1C4B, 0.9);
        blockingSprite.drawRect(0, 0, 300, this.gameParameters.stageHeight);
        blockingSprite.drawRect(900, 0, 300, this.gameParameters.stageHeight);
        blockingSprite.drawRect(300, 0, 600, 275);
        blockingSprite.drawRect(300, 600, 600, 250);
        var actualSprite = new Phaser.Sprite(this.game, 0, 0);
        actualSprite.addChild(blockingSprite);
        actualSprite.inputEnabled = true;
        this.group.add(actualSprite);
    }

    private drawInitialText():void{
        this.initialText = new Phaser.Text(this.game, 615,425, "YOU FOUND YOUR\nVALUE BONUS", this.gameParameters.font_valueBonusInitial);
        this.initialText.anchor.set(0.5, 0.5);
        this.initialText.align = 'center';
        var fadeTween:Phaser.Tween = this.game.add.tween(this.initialText).to( { alpha: 0.2 }, 700, Phaser.Easing.Linear.None, true, 0, -1);
        fadeTween.yoyo(true);
        this.group.add(this.initialText);
        var timer:Phaser.Timer = this.game.time.create(true);
        timer.add(4000, this.drawCongratulations, this);
        timer.start();
    }

    private drawCongratulations():void{
        this.initialText.text = "CONGRATULATIONS !!!";
        var timer:Phaser.Timer = this.game.time.create(true);
        timer.add(3000, this.drawInstructions, this);
        timer.start();
    }

    private drawInstructions():void{
        var sprite:Phaser.Sprite = new Phaser.Sprite(this.game, 0, 0);
        this.group.add(sprite);
        var background:Phaser.Graphics = new Phaser.Graphics(this.game, 0, 0);
        background.beginFill(0x000000, 1);
        background.drawRect(300, 275, 600, 300);
        background.endFill();
        sprite.addChild(background);
        this.valueBonusGraphic.bringToTop();
        var titleText:Phaser.Text = new Phaser.Text(this.game, 525, 300, "Your "+this.pillar.getLongName()+"\nValues guide!", this.gameParameters.font_valueBonusTitle);
        titleText.align = "center";
        sprite.addChild(titleText);
        var longText:Phaser.Text = new Phaser.Text(this.game, 580, 360, "This Special Guide Helps you get the most from your "+this.pillar.getLongName()+" core values! We know that this process can" +
        " be intense and insightful and we want to help you navigate through understanding your values.", this.gameParameters.getValueBonusText(this.pillar.getColor()));
        longText.align = "center";
        longText.wordWrap = true;
        longText.wordWrapWidth = 250;
        sprite.addChild(longText);
        var footerText:Phaser.Text = new Phaser.Text(this.game, 485, 510, "We will be sending this guide to your\nemail after you finish your game!", this.gameParameters.font_valueBonusFooter);
        footerText.align = "center";
        sprite.addChild(footerText);
        var book:Phaser.Sprite = new Phaser.Sprite(this.game, 415, 340, this.pillar.getShortName());
        sprite.addChild(book);
        sprite.alpha = 0;
        this.game.add.tween(sprite).to( { alpha: 1 }, 500, Phaser.Easing.Linear.None, true).onComplete.add(
            this.showContinue, this
        );

    }

    private showContinue():void{
        this.videoReference.hideVideo();
        var continueSprite:Phaser.Sprite = new Phaser.Sprite(this.game, 465, 615, "valueBonusContinue");
        this.group.add(continueSprite);
        continueSprite.inputEnabled = true;
        continueSprite.input.useHandCursor = true;
        continueSprite.events.onInputDown.add(this.remove, this);
    }

    private remove():void{
        this.group.destroy();
        this.board.toggleVisibility();
    }

}