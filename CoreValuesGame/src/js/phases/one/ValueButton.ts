/// <reference path= "../../../../lib/phaser.d.ts" />
/// <reference path= "../all/Value.ts" />
/// <reference path= "../../permanent/GameParameters.ts" />
/// <reference path= "PhaseOneGameBoard.ts" />
/// <reference path= "ValueRater.ts" />
/// <reference path= "../../event/GameEventPublisher.ts" />
/// <reference path= "../../event/GameEventSubscriber.ts" />
/// <reference path= "../../event/ValueRatedEvent.ts" />
/// <reference path= "../../event/EndInstructionsEvent.ts" />
/// <reference path= "ValueBonus.ts" />
/// <reference path= "PhaseOneGameBoard.ts" />


class ValueButton implements GameEventPublisher, GameEventSubscriber{

    private value:Value;
    private game:Phaser.Game;
    private group:Phaser.Group;
    private hexagonSprite:Phaser.Sprite;
    private gameParameters:GameParameters = GameParameters.getInstance();
    private color:string;
    private x:number;
    private y:number;
    private valueBonus:boolean = false;

    private subList:Array<GameEventSubscriber> = new Array<GameEventSubscriber>();

    private board:PhaseOneGameBoard;

    constructor(game: Phaser.Game, group: Phaser.Group, value:Value, color:string, board:PhaseOneGameBoard) {
        this.game = game;
        this.value = value;
        this.group = group;
        this.color = color;
        this.board = board;
    }

    public setValueBonus(){
        this.valueBonus = true;
    }

    draw(x:number, y:number):void{
        this.drawHexagon(x, y, 72, 82, 21);
    }

    drawHexagon(x:number, y:number, trWidth:number, trHeight:number, topOffset:number):void{
        this.x = x;
        this.y = y;
        var hexagon:Phaser.Graphics = new Phaser.Graphics(this.game, 0, 0);
        this.hexagonSprite = new Phaser.Sprite(this.game, x, y);
        this.hexagonSprite.addChild(hexagon);
        this.group.add(this.hexagonSprite);
        this.hexagonSprite.inputEnabled = true;
        this.hexagonSprite.input.useHandCursor = true;
        if(!this.valueBonus) {
            this.hexagonSprite.events.onInputUp.add(this.rateValue, this);
        }else{
            this.hexagonSprite.events.onInputUp.add(this.drawValueBonus, this);
        }
        var points: Phaser.Point[] = [];
        points.push(new Phaser.Point(0,trHeight-topOffset));
        points.push(new Phaser.Point(0,topOffset));
        points.push(new Phaser.Point(trWidth/2, 0));
        points.push(new Phaser.Point(trWidth,topOffset));
        points.push(new Phaser.Point(trWidth,trHeight-topOffset));
        points.push(new Phaser.Point(trWidth/2, trHeight));
        hexagon.beginFill(this.gameParameters.convertColorStringToNumber(this.color), 1);
        hexagon.drawPolygon(points);
    }


    private drawValueBonus(){
        this.hexagonSprite.destroy();
        var valueBonus:ValueBonus = new ValueBonus(this.game, this.value.getPillar(), this.board);
        valueBonus.draw();
        this.board.toggleVisibility();
        this.board.ratedButtons++;
    }

    private rateValue():void{
        this.hexagonSprite.destroy();
        var group:Phaser.Group = this.game.add.group();
        var valueRater = new ValueRater(this.game, group, this.value);
        valueRater.subscribe(this);
        var xPos = this.game.input.x;
        var yPos = 210;
        if(xPos < 700){
            xPos -= 406;
            if(xPos<0){
                xPos = 0;
            }
        }else if(xPos + 406 > 1200){
            xPos = 794;
        }
        valueRater.draw(xPos, yPos);
    }

    public subscribe(sub:GameEventSubscriber):void {
        this.subList.push(sub);
    }


    public publish(gameEvent:GameEvent):void {
        for(var i in this.subList){
            this.subList[i].accept(gameEvent);
        }
    }

    public accept(gameEvent:GameEvent) {
        this.publish(gameEvent);
    }


}