/**
 * Created by kobe on 3/24/2015.
 */

/// <reference path= "../../../../lib/phaser.d.ts" />
/// <reference path= "MenuButton.ts" />
/// <reference path= "../../permanent/GameParameters.ts" />
/// <reference path= "../../permanent/Pause.ts" />


class PauseButton extends MenuButton {


    constructor(game:Phaser.Game, group:Phaser.Group) {
        super(game, group);
    }

    complete():void{
        this.addText();
        this.backgroundSprite.inputEnabled = true;
        this.backgroundSprite.input.useHandCursor = true;
        this.backgroundSprite.events.onInputDown.add(this.pause, this);
    }

    private addText():void{
        var text:Phaser.Text = new Phaser.Text(this.getGame(), 11, 27, "PAUSE\nGAME", this.getGameParameters().font_menuButtonBig);
        text.align = 'center';
        this.getGroup().add(text);
    }

    private pause(){
        var pause:Pause = new Pause(this.getGame());
    }


}