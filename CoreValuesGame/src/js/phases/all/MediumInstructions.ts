/// <reference path= "../../../../lib/phaser.d.ts" />
/// <reference path= "../../permanent/GameParameters.ts" />
/// <reference path= "Instructions.ts" />
class MediumInstructions extends Instructions{

    constructor(game: Phaser.Game, title:string, instructions:Array<string>) {
        super(game, title, instructions);
        this.enableFade();
    }

    public drawBlockingSprite():void{
        //DO NOTHING
    }

    public drawPolygon():void{
        var instructionsPolygon:Phaser.Sprite = new Phaser.Sprite(this.getGame(), 0, 0, "mediumPolygon");
        instructionsPolygon.anchor.set(0.5, 0.5);
        this.getGroup().add(instructionsPolygon);
    }

    public drawTitle():void{
        var text:Phaser.Text = new Phaser.Text(this.getGame(), 0, -155, this.getTitle(), this.getGameParameters().font_mediumInstructionsTitle);
        text.anchor.set(0.5, 0);
        text.align = 'center';
        this.getGroup().add(text);
        var instructionsText:Phaser.Text = new Phaser.Text(this.getGame(), 0, -100, "INSTRUCTIONS", this.getGameParameters().font_mediumInstructionsSubtitle);
        instructionsText.anchor.set(0.5, 0);
        instructionsText.align = 'center';
        this.getGroup().add(instructionsText);
    }

    public drawText(pageText:string):Phaser.Text{
        var text:Phaser.Text = new Phaser.Text(this.getGame(), 0, 20, pageText, this.getGameParameters().font_mediumInstructions);
        text.anchor.set(0.5, 0.5);
        text.align = 'center';
        this.getGroup().add(text);
        text.wordWrap = true;
        text.wordWrapWidth = 325;
        return text;
    }

    public drawCloseButton():Phaser.Button{
        var button = new Phaser.Button(this.getGame(), 0, 125, 'startButton', this.removeInstructions, this, 1, 0);
        button.anchor.set(0.5,0);
        this.getGroup().add(button);
        return button;
    }

    public drawNextButton():Phaser.Button{
        var button = new Phaser.Button(this.getGame(), 0, 125, 'nextButton', this.drawNextPage, this, 1, 0);
        button.anchor.set(0.5,0);
        this.getGroup().add(button);
        return button;
    }

    public finalize():void{
        this.getGroup().x = this.getGameParameters().stageWidth-220;
        this.getGroup().y = this.getGameParameters().stageHeight/2+70;
    }



}