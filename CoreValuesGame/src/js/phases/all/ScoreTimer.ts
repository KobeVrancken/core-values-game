/// <reference path= "../../../../lib/phaser.d.ts" />
/// <reference path= "../../event/GameEventPublisher.ts" />
/// <reference path= "../../event/GameEventSubscriber.ts" />
/// <reference path= "../../event/TimerTickEvent.ts" />
/// <reference path= "../../event/TimerEndEvent.ts" />
/// <reference path= "../../permanent/GameParameters.ts" />

class ScoreTimer implements GameEventPublisher{


    private game:Phaser.Game;
    private group:Phaser.Group;
    private currentTime:number;
    private updateTimer:Phaser.Timer;
    private subList:Array<GameEventSubscriber> = new Array<GameEventSubscriber>();
    private cutoff:number;
    private gameParameters:GameParameters = GameParameters.getInstance();
    private scoreText:Phaser.Text;
    private timerText:Phaser.Text;

    constructor(game:Phaser.Game, group:Phaser.Group){
        this.game = game;
        this.group = group;
    }

    public draw(x:number, y:number):void{
        this.timerText = new Phaser.Text(this.game, x, y, "TIME   ", this.gameParameters.font_Timer);
        this.group.addChild(this.timerText);
        this.scoreText = new Phaser.Text(this.game, x, y+25, "00:00  ", this.gameParameters.font_Timer);
        this.group.addChild(this.scoreText);
    }

    public stop():void{
        this.updateTimer.stop();
        this.group.destroy();
    }

    public setTickCutoff(cutoff:number){
        this.cutoff = cutoff;
    }

    public start(startTime:number){
        this.currentTime = startTime;
        this.updateText();
        this.updateTimer = this.game.time.create();
        this.updateTimer.loop(1000, this.subtractTime, this);
        this.updateTimer.start();
    }

    private subtractTime(){
        this.currentTime--;
        if(this.currentTime == 0){
            this.applyBounceAlphaTween(this.scoreText);
            this.applyBounceAlphaTween(this.timerText);
        }
        if(this.currentTime<0){
            this.currentTime = 0;
            this.publish(new TimerEndEvent(this));
            this.updateTimer.stop();

        }else if(this.currentTime<this.cutoff){
            this.publish(new TimerTickEvent(this));
        }

        this.updateText();
    }

    private applyBounceAlphaTween(sprite:Phaser.Text){
        var fadeTween:Phaser.Tween = this.game.add.tween(sprite).to( { alpha: 0.2 }, 300, Phaser.Easing.Linear.None, true, 0, -1);
        fadeTween.yoyo(true);
    }


    private updateText(){
        var minutes:number = Math.floor(this.currentTime/60);
        var seconds:number = this.currentTime-60*minutes;
        this.scoreText.text = this.gameParameters.pad(minutes, 2)+':'+this.gameParameters.pad(seconds,2)+"  ";
    }

    subscribe(sub:GameEventSubscriber):void {
        this.subList.push(sub);
    }

    publish(gameEvent:GameEvent):void {
        for(var i in this.subList){
            this.subList[i].accept(gameEvent);
        }
    }


}