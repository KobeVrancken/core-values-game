/// <reference path= "Pillar.ts" />
/// <reference path= "ValueScore.ts" />

class PillarScore{

    private pillar:Pillar;
    private valueScores:Array<ValueScore> = new Array<ValueScore>();
    private penalties:Array<number> = new Array<number>();

    constructor(pillar:Pillar){
        this.pillar = pillar;
    }

    public addValueScore(valueScore:ValueScore){
        this.valueScores.push(valueScore);
    }

    public getPillar():Pillar{
        return this.pillar;
    }

    public getScore():number{
        var total:number = 0;
        for(var i in this.valueScores){
            total += this.valueScores[i].getTotal();
        }
        for(var i in this.penalties){
            total -= this.penalties[i];
        }
        return total;
    }

    public addPenalty(penalty:number){
        this.penalties.push(penalty);
    }


}