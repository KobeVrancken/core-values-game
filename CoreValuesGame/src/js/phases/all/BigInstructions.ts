/// <reference path= "../../../../lib/phaser.d.ts" />
/// <reference path= "../../permanent/GameParameters.ts" />
/// <reference path= "Instructions.ts" />
class BigInstructions extends Instructions{

    private video:boolean = false;

    constructor(game: Phaser.Game, title:string, instructions:Array<string>) {
        super(game, title, instructions);
    }

    public drawBlockingSprite():void{
        var blockingSprite:Phaser.Graphics = new Phaser.Graphics(this.getGame(), 0, 0);
        blockingSprite.beginFill(0x1E1C4B, 0.8);
        blockingSprite.drawRect(-this.getGroup().x, -this.getGroup().y, this.getGameParameters().stageWidth, this.getGameParameters().stageHeight);
        var actualSprite = new Phaser.Sprite(this.getGame(), 0, 0);
        actualSprite.addChild(blockingSprite);
        actualSprite.inputEnabled = true;
        this.getGroup().add(actualSprite);
    }

    public drawPolygon():void{

        var instructionsPolygon:Phaser.Sprite = new Phaser.Sprite(this.getGame(), 0, 20, "instructionsPolygon");
        instructionsPolygon.anchor.set(0.5, 0.5);
        this.getGroup().add(instructionsPolygon);
    }

    public drawTitle():void{
        var text:Phaser.Text = new Phaser.Text(this.getGame(), 0, -180, this.getTitle(), this.getGameParameters().font_bigInstructionsTitle);
        text.anchor.set(0.5, 0);
        text.align = 'center';
        this.getGroup().add(text);
    }

    public drawText(pageText:string):Phaser.Text{
        var text:Phaser.Text = new Phaser.Text(this.getGame(), 0, -120, pageText, this.getGameParameters().font_bigInstructions);
        text.anchor.set(0.5, 0);
        text.align = 'center';
        this.getGroup().add(text);
        text.wordWrap = true;
        text.wordWrapWidth = 480;
        return text;
    }

    public drawCloseButton():Phaser.Button{
        var button = new Phaser.Button(this.getGame(), 0, 175, 'squareContinueButton', this.removeInstructions, this, 1, 0);
        button.anchor.set(0.5,0);
        this.getGroup().add(button);
        return button;
    }

    public enableVideo():void{
        this.video = true;
    }

    public finalize():void{
    }

}