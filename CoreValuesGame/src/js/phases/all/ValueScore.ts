/// <reference path= "Value.ts" />

class ValueScore{

    private value:Value;
    private ratings:Array<number> = new Array<number>();
    private phaseOneRating:number = 0;
    private realScores:Array<number> = new Array<number>();

    constructor(value:Value){
        this.value = value;
    }

    public addRating(rating:number){
        this.ratings.push(rating);
    }

    public addRealScore(rating:number){
        this.realScores.push(rating);
    }

    public setPhaseOneRating(phaseOneRating:number){
        this.phaseOneRating = phaseOneRating;
    }

    public removePhaseOneRating():void{
        this.phaseOneRating = 0;
        this.ratings = this.ratings.slice(0, this.ratings.length-1);
        this.realScores = this.realScores.slice(0, this.realScores.length-1);
    }

    public getTotal():number{
        var total:number = 0;
        for(var i in this.ratings){
            total += this.ratings[i];
        }
        return total;
    }

    public getRealTotal():number{
        var total:number = 0;
        for(var i in this.realScores){
            total += this.realScores[i];
        }
        return total;
    }

    public getValue():Value{
        return this.value;
    }

    public getPhaseOneRating(){
        return this.phaseOneRating;
    }

    static compare(a:ValueScore, b:ValueScore) {
        if (a.getTotal() < b.getTotal()) return -1;
        if (a.getTotal() > b.getTotal()) return 1;
        return 0;
    }

    static comparePhaseOne(a:ValueScore, b:ValueScore) {
        if (a.getPhaseOneRating() < b.getPhaseOneRating()) return -1;
        if (a.getPhaseOneRating() > b.getPhaseOneRating()) return 1;
        return 0;
    }
}