/// <reference path= "Pillar.ts" />


class Value {
    private valueName:string;
    private shortDefinition:string;
    private longDefinition:string;
    private example:string;
    private improvement:string;
    private smallGroup:string;
    private bigGroup:string = '0';
    private pillar:Pillar;

    constructor(valueName:string) {
        this.valueName = valueName;
    }
    
    public getValueName():string{
        return this.valueName;
    }
    
    public getShortDefinition():string{
        return this.shortDefinition;
    }
    
    public setShortDefinition(shortDefinition:string):void{
        this.shortDefinition = shortDefinition;
    }
    
    public getLongDefinition():string{
        return this.longDefinition;
    }
    
    public setLongDefinition(longDefinition:string):void{
        this.longDefinition = longDefinition;
    }
    
    public getExample():string{
        return this.example;
    }
    
    public setExample(example:string):void{
        this.example = example;
    }
    
    public getImprovement():string{
        return this.improvement;
    }
    
    public setImprovement(improvement:string):void{
        this.improvement = improvement;
    }
    
    public getSmallGroup():string{
        return this.smallGroup;
    }
    
    public setSmallGroup(smallGroup:string):void{
        this.smallGroup = smallGroup;
    }
    
    public getBigGroup():string{
        return this.bigGroup;
    }
    
    public setBigGroup(bigGroup:string):void{
        this.bigGroup = bigGroup;
    }

    public getPillar():Pillar{
        return this.pillar;
    }

    public setPillar(pillar:Pillar):void{
        this.pillar = pillar;
    }

    static compareBigGroup(a:Value, b:Value) {
        if (parseInt(a.getBigGroup()) < parseInt(b.getBigGroup())) return -1;
        if (parseInt(a.getBigGroup()) > parseInt(b.getBigGroup())) return 1;
        return 0;
    }

}