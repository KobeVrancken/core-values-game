/// <reference path="Value.ts" />

class Pillar{

    private name:string;
    private color:string;
    private map:Array<Array<string>> = new Array<Array<string>>();
    private values:Array<Value> = new Array<Value>();
    private group:string;
    private shortName:string;
    private longName:string;

    constructor(name:string, color:string, map:Array<Array<string>>, group:string, shortName:string, longName:string){
        this.name = name;
        this.color = color;
        this.map = map;
        this.group = group;
        this.shortName = shortName;
        this.longName = longName;
    }

    public getName():string{
        return this.name;
    }

    public getShortName():string{
        return this.shortName;
    }

    public getLongName():string{
        return this.longName;
    }

    public getColor():string{
        return this.color;
    }

    public addValue(value:Value){
        this.values.push(value);
    }

    public getValues():Array<Value>{
        return this.values;
    }

    public hasValue(value:Value):boolean{
        var i:number = this.values.length;
        while (i--) {
            if (this.values[i] === value) {
                return true;
            }
        }
        return false;
    }

    public getGroup():string{
        return this.group;
    }

    public getMap():Array<Array<string>>{
        return this.map;
    }




}