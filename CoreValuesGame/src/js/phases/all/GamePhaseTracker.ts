/**
 * Created by kobe on 3/24/2015.
 */

/// <reference path= "../../../../lib/phaser.d.ts" />
/// <reference path= "PauseButton.ts" />
/// <reference path= "AudioButton.ts" />
/// <reference path= "ScoreButton.ts" />
/// <reference path= "../../permanent/GameParameters.ts" />
/// <reference path= "PercentageBar.ts"/>
/// <reference path= "PhaseIndicator.ts"/>
/// <reference path= "../../event/GameEventPublisher.ts" />
/// <reference path= "../../event/GameEventSubscriber.ts" />
/// <reference path= "../../event/ValueRatedEvent.ts" />
/// <reference path= "../../event/PillarScoreEvent.ts" />
/// <reference path= "../../event/CurrentScoreEvent.ts" />
/// <reference path= "../../event/GameProgressEvent.ts" />


class GamePhaseTracker implements GameEventSubscriber, GameEventPublisher{


    private game:Phaser.Game;
    private gameParameters:GameParameters = GameParameters.getInstance();
    private phaseNumber:number;
    private phaseImage:string;
    private phaseTitle:string;
    private phaseSubText:string;
    private color:string;
    private copyrightColor:string;
    private menuGroup:Phaser.Group;
    private contextGroup:Phaser.Group;

    private pauseButton:PauseButton;
    private scoreButton:ScoreButton;
    private audioButton:AudioButton;
    private percentageBar:PercentageBar;
    private subs:Array<GameEventSubscriber> = new Array<GameEventSubscriber>();

    constructor(game:Phaser.Game, phaseNumber:number, phaseTitle:string, phaseSubText:string, phaseImage:string, color:string){
        this.game = game;
        this.phaseNumber = phaseNumber;
        this.phaseTitle = phaseTitle;
        this.phaseSubText = phaseSubText;
        this.color = color;
        this.copyrightColor = color;
        this.phaseImage = phaseImage;
    }

    draw(){
        this.drawMenu();
        this.drawPhaseInfo();
    }

    private drawMenu():void{
        this.menuGroup = new Phaser.Group(this.game);
        this.menuGroup.x = 940;
        this.menuGroup.y = 45;
        this.drawPauseButton(this.menuGroup);
        this.drawScoreButton(this.menuGroup);
        this.drawAudioButton(this.menuGroup);
        this.drawEmptyHexagon(this.menuGroup);
    }

    private drawPhaseInfo() {
        this.contextGroup = new Phaser.Group(this.game);
        this.drawPhaseTitle(this.contextGroup);
        this.drawPhaseImage(this.contextGroup);
        this.drawPhaseSubTitle(this.contextGroup);
        this.drawPercentageBar(this.contextGroup);
        this.drawCopyRight(this.contextGroup, this.copyrightColor);
    }

    public drawDelayed():void {
        this.drawPhaseIndicator(this.contextGroup);
        this.drawUser(this.contextGroup);
        this.drawHelp(this.contextGroup);
    }

    private drawPauseButton(group:Phaser.Group):void{
        var buttonGroup:Phaser.Group = new Phaser.Group(this.game);
        group.add(buttonGroup);
        this.pauseButton = new PauseButton(this.game, buttonGroup);
        this.pauseButton.draw();
    }

    private drawAudioButton(group:Phaser.Group):void{
        var buttonGroup:Phaser.Group = new Phaser.Group(this.game);
        buttonGroup.x = 93;
        group.add(buttonGroup);
        this.audioButton = new AudioButton(this.game, buttonGroup);
        this.audioButton.draw();
    }

    private drawScoreButton(group:Phaser.Group):void{
        var buttonGroup:Phaser.Group = new Phaser.Group(this.game);
        buttonGroup.x = 46;
        buttonGroup.y = 80;
        group.add(buttonGroup);
        this.scoreButton = new ScoreButton(this.game, buttonGroup);
        this.scoreButton.draw();
    }

    private drawEmptyHexagon(group:Phaser.Group):void{
        var backgroundSprite:Phaser.Sprite = new Phaser.Sprite(this.game, 0, 0, "smallPolygon");
        backgroundSprite.x = 139;
        backgroundSprite.y = 80;
        group.add(backgroundSprite);
    }

    private drawPhaseTitle(group:Phaser.Group):void{
        var text:Phaser.Text = new Phaser.Text(this.game, 510, 70, this.phaseTitle, this.gameParameters.font_bigPhaseTitle);
       group.add(text);
    }

    private drawPhaseImage(group:Phaser.Group):void{
        var sprite:Phaser.Sprite = new Phaser.Sprite(this.game, 345, 125, this.phaseImage);
        group.add(sprite);
    }

    private drawPhaseSubTitle(group:Phaser.Group):void{
        var text:Phaser.Text = new Phaser.Text(this.game, 640, 130, this.phaseSubText, this.gameParameters.font_phaseSubtitle);
        group.add(text);
    }

    private drawPercentageBar(group:Phaser.Group):void{
        this.percentageBar = new PercentageBar(this.game, group, this.color);
        this.percentageBar.draw(645, 165);
    }

    private copyrightBar:Phaser.Graphics = null;

    public setCopyRightColor(color:string){
        this.copyrightColor = color;
        if(this.copyrightBar != null){
            this.copyrightBar.destroy();
            this.drawCopyRight(this.contextGroup, color);
        }
    }

    private drawCopyRight(group:Phaser.Group, color:string):void{
        this.copyrightBar = new Phaser.Graphics(this.game, 0, 0);
        this.copyrightBar.beginFill(this.gameParameters.convertColorStringToNumber(color), 1);
        this.copyrightBar.drawRoundedRect(265,this.gameParameters.stageHeight-25, 685, 50, 20);
        group.add(this.copyrightBar);
        var text:Phaser.Text = new Phaser.Text(this.game, 292, this.gameParameters.stageHeight-20 , "Copyright© 2014 All Rights Reserved | Created by Scott Epp C.P.C. | www.abundancecoaching.com", this.gameParameters.font_copyrightFont);
        group.add(text);
    }

    private drawPhaseIndicator(group:Phaser.Group):void{
        var phaseIndicator:PhaseIndicator = new PhaseIndicator(this.game, group);
        phaseIndicator.draw();
        phaseIndicator.setPhase(this.phaseNumber);
    }

    private drawUser(group:Phaser.Group):void{
        var emptyBar:Phaser.Graphics = new Phaser.Graphics(this.game, 0, 0);
        emptyBar.beginFill(0x000000, 1);
        emptyBar.drawRoundedRect(75,160, 280, 20, 8);
        group.add(emptyBar);
        var text:Phaser.Text = new Phaser.Text(this.game, 87, 163, "USER: Scott Epp", this.gameParameters.font_usernameFont);
        group.add(text);
    }

    private drawHelp(group:Phaser.Group):void{
        var button = new Phaser.Button(this.game, 210, 185, 'helpButton', this.openHelp, this, 0, 1);
        group.add(button);
    }

    public openHelp(){
        console.log("Open help");
    }

    public getPauseButton(){
        return this.pauseButton;
    }

    public getAudioButton(){
        return this.audioButton;
    }

    public getScoreButton(){
        return this.scoreButton;
    }


    accept(gameEvent:GameEvent) {
        if(gameEvent instanceof CurrentScoreEvent){
            this.scoreButton.setScore(gameEvent.getScore());
        }else if(gameEvent instanceof GameProgressEvent){
            this.percentageBar.setPercentage(gameEvent.getProgress());
        }
    }

    subscribe(sub:GameEventSubscriber):void {
        this.subs.push(sub);
    }

    publish(gameEvent:GameEvent):void {
        for(var i in this.subs){
            this.subs[i].accept(gameEvent);
        }
    }

}