/**
 * Created by kobe on 3/24/2015.
 */
/// <reference path= "../../../../lib/phaser.d.ts" />
/// <reference path= "../../permanent/GameParameters.ts" />

class PercentageBar {

    private game:Phaser.Game;
    private group:Phaser.Group;
    private color:string;
    private gameParameters:GameParameters = GameParameters.getInstance();
    private fillBar:Phaser.Graphics;
    private progressText:Phaser.Text;

    constructor(game:Phaser.Game, group:Phaser.Group, color:string) {
        this.game = game;
        this.group = group;
        this.color = color;
    }

    draw(x, y){
        var percentageGroup:Phaser.Group = new Phaser.Group(this.game);
        percentageGroup.x = x;
        percentageGroup.y = y;
        this.group.add(percentageGroup);
        var emptyBar:Phaser.Graphics = new Phaser.Graphics(this.game, 0, 0);

        emptyBar.lineStyle(1, this.gameParameters.convertColorStringToNumber(this.color));
        emptyBar.drawRect(0, 0, 285, 7);
        percentageGroup.add(emptyBar);

        this.fillBar = new Phaser.Graphics(this.game, 0, 0);
        this.fillBar.beginFill(this.gameParameters.convertColorStringToNumber(this.color), 1);
        this.fillBar.drawRect(0, 0, 285, 7);
        this.fillBar.width = 0;
        percentageGroup.add(this.fillBar);

        this.progressText = new Phaser.Text(this.game, 0, 13, "PERCENTAGE COMPLETED: 0%", this.gameParameters.getPercentageBarFont(this.color));
        percentageGroup.add(this.progressText);

    }

    public setPercentage(percentage:number):void{
        this.fillBar.width = 285*(percentage/100);
        this.progressText.text = "PERCENTAGE COMPLETED: "+Math.round(percentage)+"%";
    }



}