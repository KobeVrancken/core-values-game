/**
 * Created by kobe on 3/24/2015.
 */
/// <reference path= "../../../../lib/phaser.d.ts" />
/// <reference path= "../../permanent/GameParameters.ts" />


class MenuButton {

    private game:Phaser.Game;
    private group:Phaser.Group;
    private gameParameters:GameParameters = GameParameters.getInstance();
    public backgroundSprite:Phaser.Sprite;

    constructor(game:Phaser.Game, group:Phaser.Group) {
        this.game = game;
        this.group = new Phaser.Group(this.game);
        group.add(this.group);
    }

    draw(){
        this.drawBackground();
        this.complete();
    }

    private drawBackground():void{
        this.backgroundSprite = new Phaser.Sprite(this.game, 0, 0, "smallPolygon");
        this.group.add(this.backgroundSprite);
    }

    public complete():void{
        throw new Error("Error: extended class must override this method.");
    }

    public getGame():Phaser.Game{
        return this.game;
    }

    public getGroup():Phaser.Group{
        return this.group;
    }

    public getGameParameters():GameParameters{
        return this.gameParameters;
    }

    public hide(){
        this.group.visible = false;
    }

    public show(){
        this.group.visible = true;
    }
}