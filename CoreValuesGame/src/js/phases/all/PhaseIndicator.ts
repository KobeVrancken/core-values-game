/// <reference path= "../../../../lib/phaser.d.ts" />
/// <reference path= "../../permanent/GameParameters.ts" />

class PhaseIndicator{

    private game:Phaser.Game;
    private group:Phaser.Group;
    private indicators:Array<Phaser.Sprite> = new Array<Phaser.Sprite>();
    private labels:Array<Phaser.Text> = new Array<Phaser.Text>();
    private gameParameters:GameParameters = GameParameters.getInstance();

    constructor(game:Phaser.Game, group:Phaser.Group){
        this.game = game;
        this.group = new Phaser.Group(this.game);
        this.group.x = 75;
        this.group.y = 185;
        group.add(this.group);
    }

    public draw():void{
        for(var i:number = 1;i<=5;i++) {
            var sprite:Phaser.Sprite = new Phaser.Sprite(this.game, (i-1)*26, 0, "phaseIndicator");
            this.indicators.push(sprite);
            this.group.add(sprite);
            var text:Phaser.Text = new Phaser.Text(this.game,(i-1)*26+9,5,""+i, JSON.parse(JSON.stringify(this.gameParameters.font_phaseIndicator))); //copy font
            this.labels.push(text);
            this.group.add(text);
        }

    }

    public setPhase(phaseNumber:number) {
        for(var i in this.indicators){
            if(i == phaseNumber-1){
                this.indicators[i].frame = 1;
                this.labels[i].fill = "#33336c";

            }else{
                this.indicators[i].frame = 0;
                this.labels[i].fill = this.gameParameters.font_phaseIndicator.fill;
            }
        }

    }
}