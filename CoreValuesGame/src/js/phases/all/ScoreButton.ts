/**
 * Created by kobe on 3/24/2015.
 */

/// <reference path= "../../../../lib/phaser.d.ts" />
/// <reference path= "MenuButton.ts" />
/// <reference path= "../../permanent/GameParameters.ts" />

class ScoreButton extends MenuButton {

    private numberText:Phaser.Text;


    constructor(game:Phaser.Game, group:Phaser.Group) {
        super(game, group);
    }

    complete():void{
        this.addText();
    }

    private addText():void{
        var text:Phaser.Text = new Phaser.Text(this.getGame(), 11, 27, "SCORE", this.getGameParameters().font_menuButtonBig);
        this.getGroup().add(text);
        this.numberText = new Phaser.Text(this.getGame(), 23, 55, "0000", this.getGameParameters().font_menuButtonBigYellow);
        this.getGroup().add(this.numberText);
    }

    public setScore(score:number):void{
        if(score < 0) score = 0;
        this.numberText.text = this.getGameParameters().pad(Math.round(score),4);
    }
}