/// <reference path= "../../../../lib/phaser.d.ts" />
/// <reference path= "MenuButton.ts" />
/// <reference path= "../../permanent/GameParameters.ts" />

class AudioButton extends MenuButton {

    private sound:Phaser.Sound;

    constructor(game:Phaser.Game, group:Phaser.Group) {
        super(game, group);
        this.startSound();
    }

    complete():void{
        this.addText();
    }

    private addText():void{
        var middle:number = 35;
        var text:Phaser.Text = new Phaser.Text(this.getGame(), middle-25, 25, "AUDIO ON", this.getGameParameters().font_menuAudioButtonWhite);
        this.getGroup().add(text);
        var trackText:Phaser.Text = new Phaser.Text(this.getGame(), middle-12, 50, "TRACK", this.getGameParameters().font_menuAudioButtonYellow);
        this.getGroup().add(trackText);
        var numberText:Phaser.Text = new Phaser.Text(this.getGame(), middle, 67, "1/6", this.getGameParameters().font_menuAudioButtonYellow);
        this.getGroup().add(numberText);
    }

    private startSound():void{
        this.sound = this.getGame().add.audio("track_1");
        //this.sound.play();
    }
}