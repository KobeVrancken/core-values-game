/**
 * Created by kobe on 3/23/2015.
 */

/// <reference path= "../../../../lib/phaser.d.ts" />
/// <reference path= "../../permanent/GameParameters.ts" />
/// <reference path= "../../event/GameEventPublisher.ts" />
/// <reference path= "../../event/EndInstructionsEvent.ts" />


class Instructions implements GameEventPublisher{

    private game:Phaser.Game;
    private group:Phaser.Group;
    private title:string;
    private instructions:Array<string>;
    private gameParameters:GameParameters = GameParameters.getInstance();
    private currentPage:number = 0;
    private currentText:Phaser.Text;
    private currentButton:Phaser.Button;
    private closeButton:Phaser.Button;
    private fade:boolean = false;
    private keyboardInstructionsBoolean:boolean = false;
    private keyboardInstructionsGroup:Phaser.Group;

    constructor(game: Phaser.Game, title:string, instructions:Array<string>) {
        this.game = game;
        this.title = title;
        this.instructions = instructions;
        this.group = new Phaser.Group(this.game);
        this.group.x = this.gameParameters.stageWidth / 2;
        this.group.y = this.gameParameters.stageHeight / 2;
    }

    draw():void{
        this.drawBlockingSprite();
        this.drawPolygon();
        this.drawTitle();
        this.currentText = this.drawText(this.getPageText(this.currentPage++));
        this.fadeObject(this.currentText,0);
        this.currentButton = this.drawCloseButton();
        this.closeButton = this.currentButton;
        if(this.instructions.length>1){
            this.currentButton = this.drawNextButton();
            this.closeButton.alpha = 0;
        }
        this.fadeObject(this.currentButton,500);
        this.finalize();
    }


    public getGame():Phaser.Game{
        return this.game;
    }

    public getGroup():Phaser.Group{
        return this.group;
    }

    public getTitle():string{
        return this.title;
    }

    public getInstructions():Array<string>{
        return this.instructions;
    }

    public getGameParameters():GameParameters{
        return this.gameParameters;
    }

    public removeInstructions():void{
        this.group.destroy();
        this.publish(new EndInstructionsEvent(this));
    }

    public getPageText(page:number):string{
        return this.instructions[page];
    }

    private hasNextPage():boolean{
        return this.currentPage < this.instructions.length;
    }


    public drawNextPage():void{
        if(this.keyboardInstructionsBoolean){
            if(this.currentPage == 1){
                this.showKeyboardInstructions();
            }else if(this.currentPage == 2){
                this.keyboardInstructionsGroup.destroy();
            }
        }
        this.replaceText(this.getPageText(this.currentPage++));
        this.fadeObject(this.currentText,0);
        if(!this.hasNextPage()){
            this.removeNextButton();
            this.currentButton = this.closeButton;
        }
        this.fadeObject(this.currentButton,500);

    }

    public replaceText(newText:string):void{
        this.currentText.setText(newText);

    }

    private fadeObject(phaserObject:any, delay:number):void{
        if(this.fade) {
            this.gameParameters.fadeObjectIn(this.game, phaserObject, 500, delay);
        }
    }

    public drawBlockingSprite():void{
        throw new Error("Error: extended class must override this method.");
    }

    public drawPolygon():void{
        throw new Error("Error: extended class must override this method.");

    }

    public drawTitle():void{
        throw new Error("Error: extended class must override this method.");
    }

    public drawText(text:string):Phaser.Text{
        throw new Error("Error: extended class must override this method.");
    }

    public drawCloseButton():Phaser.Button{
        throw new Error("Error: extended class must override this method.");
    }

    public removeNextButton():void{
        this.currentButton.destroy();
    }

    public drawNextButton():Phaser.Button{
        throw new Error("Error: extended class must override this method.");
    }

    public finalize():void{
        throw new Error("Error: extended class must override this method.");
    }

    public enableFade():void{
        this.fade = true;
    }



    public enableKeyboardInstructions():void{
        this.keyboardInstructionsBoolean = true;
    }

    public showKeyboardInstructions():void{
        this.keyboardInstructionsGroup = new Phaser.Group(this.game);
        var inputSprite:Phaser.Sprite = new Phaser.Sprite(this.game, 205, 350, "keyboardInstructions");
        this.keyboardInstructionsGroup.add(inputSprite);
        var arrowSprite:Phaser.Sprite = new Phaser.Sprite(this.game, 175, 585, "moveArrows");
        this.keyboardInstructionsGroup.add(arrowSprite);
        this.keyboardInstructionsGroup.alpha = 0.8;
        var fadeTween:Phaser.Tween = this.game.add.tween(this.keyboardInstructionsGroup).to( { alpha: 0.4 }, 700, Phaser.Easing.Linear.None, true, 0, -1);
        fadeTween.yoyo(true);
        arrowSprite.alpha = 0.8;
    }


    //INTERFACES
    private subList:Array<GameEventSubscriber> = new Array<GameEventSubscriber>();

    subscribe(sub:GameEventSubscriber):void {
        this.subList.push(sub);
    }

    publish(gameEvent:GameEvent):void {
        for(var i in this.subList){
            this.subList[i].accept(gameEvent);
        }
    }




}