/// <reference path= "Value.ts" />
/// <reference path= "../../../../lib/phaser.d.ts" />
/// <reference path= "../../permanent/GameParameters.ts" />


class ValueHexagon{
    private game:Phaser.Game;
    private group:Phaser.Group;
    private value:Value;
    private x:number;
    private y:number;
    private hexagonSprite:Phaser.Sprite;
    private backgroundSprite:Phaser.Sprite;
    private gameParameters:GameParameters = GameParameters.getInstance();
    private textSprite:Phaser.Sprite;
    private width:number = 87;
    private height:number = 100;
    private offset:number = 24;
    private fontSize:number = 12;
    private scale:number = 1;
    private dashed:boolean = true;

    constructor(game:Phaser.Game, group:Phaser.Group){
        this.game = game;
        var newGroup:Phaser.Group = new Phaser.Group(this.game);
        group.add(newGroup);
        this.group = newGroup;
    }

    public setValue(value:Value){
        this.value = value;
    }

    public hasValue():boolean{
        return this.value != null;
    }

    public getValue():Value{
        return this.value;
    }

    public draw(x:number, y:number):void{
        this.drawHexagon(x, y, this.width*this.scale, this.height*this.scale, this.offset*this.scale);
    }

    public getDragSprite():Phaser.Sprite{
        return this.hexagonSprite;
    }

    public getHitSprite():Phaser.Sprite{
        return this.textSprite;
    }

    public setWidth(width:number):void{
        this.width = width;
    }

    public setHeight(height:number):void{
        this.height = height;
    }

    public setOffset(offset:number):void{
        this.offset = offset;
    }

    public setFontSize(size:number):void{
        this.fontSize = size;
    }

    public setScale(scale:number):void{
        this.scale = scale;
    }

    public disableDashedHexagon():void{
        this.dashed = false;
    }

    public getHexagonSprite():Phaser.Sprite{
        return this.hexagonSprite;
    }

    private drawHexagon(x:number, y:number, trWidth:number, trHeight:number, topOffset:number):void{
        if(this.dashed) {
            this.backgroundSprite = new Phaser.Sprite(this.game, x, y, "dashedHexagon");
            this.group.add(this.backgroundSprite);
        }
        if(this.hasValue()) {
            this.x = x;
            this.y = y;
            var hexagon:Phaser.Graphics = new Phaser.Graphics(this.game, 0, 0);
            this.hexagonSprite = new Phaser.Sprite(this.game, x, y);
            this.hexagonSprite.addChild(hexagon);
            this.hexagonSprite.inputEnabled = true;
            this.hexagonSprite.input.useHandCursor = true;
            var points: Phaser.Point[] = [];
            points.push(new Phaser.Point(0,trHeight-topOffset));
            points.push(new Phaser.Point(0,topOffset));
            points.push(new Phaser.Point(trWidth/2, 0));
            points.push(new Phaser.Point(trWidth,topOffset));
            points.push(new Phaser.Point(trWidth,trHeight-topOffset));
            points.push(new Phaser.Point(trWidth/2, trHeight));
            hexagon.beginFill(this.gameParameters.convertColorStringToNumber(this.value.getPillar().getColor()), 1);
            hexagon.drawPolygon(points);
            this.group.add(this.hexagonSprite);
            var text:Phaser.Text = new Phaser.Text(this.game, 43*this.scale, 53*this.scale, this.value.getValueName(), this.gameParameters.getValueHexagonFont(Math.round(this.fontSize*this.scale)+""));
            text.anchor.set(0.5, 0.5);
            this.textSprite = new Phaser.Sprite(this.game, 0, 0);
            this.textSprite.addChild(text);
            this.hexagonSprite.addChild(this.textSprite);

        }
    }






}