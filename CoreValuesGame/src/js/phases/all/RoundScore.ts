class RoundScore{
    private score:number = 0;

    public addScore(score:number):void{
        this.score += score;
    }

    public getScore():number{
        return this.score;
    }
}