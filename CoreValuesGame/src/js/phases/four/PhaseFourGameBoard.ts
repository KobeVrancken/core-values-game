/// <reference path= "../../../../lib/phaser.d.ts" />
/// <reference path= "../all/Pillar.ts" />
/// <reference path= "../../permanent/GameState.ts" />

/// <reference path= "../../event/GameEventPublisher.ts" />
/// <reference path= "../../event/GameEventSubscriber.ts" />
/// <reference path= "../../event/ValueRatedEvent.ts" />
/// <reference path= "../../event/TimerTickEvent.ts" />
/// <reference path= "../../event/ScorePenaltyEvent.ts" />

/// <reference path= "../../event/GameProgressEvent.ts" />
/// <reference path= "../../permanent/GameParameters.ts" />

class PhaseFourGameBoard implements GameEventSubscriber, GameEventPublisher{

    private pillar:Pillar;
    private game:Phaser.Game;
    private group:Phaser.Group;
    private values:Array<Value>;
    private buttonGroup:Phaser.Group;
    private backgroundSprite:Phaser.Sprite;
    private subs:Array<GameEventSubscriber> = new Array<GameEventSubscriber>();
    private totalButtons:number = 0;
    public ratedButtons:number = 0;
    private gameParameters:GameParameters = GameParameters.getInstance();
    private gameState:GameState;

   /* private hexagonToStartSprite:Phaser.Sprite;
    private hexagonToStartText:Phaser.Text;
    private hexagonToStartTextBlack:Phaser.Text;*/
    private destroyEvent:Phaser.SignalBinding;

    private dropTargets:Array<Phaser.Sprite> = new Array<Phaser.Sprite>();
    private dropTexts:Array<Phaser.Text> = new Array<Phaser.Text>();
    private occupiedTargets:Array<number> = new Array<number>();

    private visibleValues:Array<Value>;
    private visibleValueSprites:Array<Phaser.Sprite>;

    private frameNumber:number;
    private currentPage:number;
    private valueGroup:Phaser.Group;

    constructor(game:Phaser.Game, pillar:Pillar, gameState:GameState){
        this.pillar = pillar;
        this.game = game;
        this.group = game.add.group();
        this.values = pillar.getValues();
        this.buttonGroup = new Phaser.Group(this.game);
        this.gameState = gameState;
        var gr:number = parseInt(this.pillar.getGroup());
        if(gr == 2) gr = 3;
        else if(gr == 3) gr = 2;
        this.frameNumber = gr;
    }

    public draw():void{
        this.drawLifeRaft();
        this.drawValueBackground();
        this.drawClickToStart();
        this.drawDropTargets();
        this.drawValueHexagons();
        this.showPage(0);
    }

    private drawLifeRaft():void{
        var lifeRaft:Phaser.Sprite = new Phaser.Sprite(this.game, 105, 470, "phaseFourLifeRaftBig");
        this.group.add(lifeRaft);
    }

    private drawValueBackground():void{
        var valueBackground:Phaser.Sprite = new Phaser.Sprite(this.game, 650, 230, "mediumPolygon");
        this.group.add(valueBackground);
    }

    private drawDropTargets():void{
        var x:Array<number> = [200, 330, 275, 365, 465];
        var y:Array<number> = [470, 470, 530, 550, 550];
        var rotation:Array<number> = [-0.2, 0, 0.5, 0, -0.1];
        for(var i in x){
            var dropTarget:Phaser.Sprite = new Phaser.Sprite(this.game, x[i], y[i], "phaseFourDropTarget", 0);
            dropTarget.anchor.set(0.5, 0.5);
            dropTarget.rotation = rotation[i];
            this.group.add(dropTarget);
            var text:Phaser.Text = new Phaser.Text(this.game, 0, 0, "", this.gameParameters.font_phaseFourValueName);
            text.anchor.set(0.5, 0.5);
            dropTarget.addChild(text);
            this.dropTargets.push(dropTarget);
            this.dropTexts.push(text);
            this.occupiedTargets.push(-1);
        }
    }

    private drawValueHexagons():void{
        this.visibleValues = new Array<Value>();
        this.visibleValueSprites = new Array<Phaser.Sprite>();
        var valueCounter = 0;
        this.valueGroup = new Phaser.Group(this.game);
        this.valueGroup.x = 667;
        this.valueGroup.y = 375;
        var hor:number = 93;
        var ver:number = 80;
        var scale:number = 0.82;
        for(var page:number = 0; page<2; page++) {
            for (var i:number = 0; i < 4; i++) {
                for (var j:number = 0; j < 3; j++) {
                    if (!(j % 2 == 0 && i == 0)) {
                        var xOffset:number = (j == 1 ? 47 : 0);

                        if(page == 0) this.drawValueSprite(hor, i, xOffset, ver, j, scale, this.valueGroup, 5);
                        var valueSprite:Phaser.Sprite = this.drawValueSprite(hor, i, xOffset, ver, j, scale, this.valueGroup, this.frameNumber);
                        valueSprite.inputEnabled = true;
                        valueSprite.input.useHandCursor = true;
                        valueSprite.input.enableDrag();
                        this.game.physics.enable(valueSprite, Phaser.Physics.ARCADE);
                        //****VALUE PART ***//
                        this.addDropListener(valueSprite, valueCounter, hor * i + xOffset, ver * j);
                        var value:Value = this.values[valueCounter++];
                        this.visibleValues.push(value);
                        this.visibleValueSprites.push(valueSprite);
                        ///////

                        var text:Phaser.Text = new Phaser.Text(this.game, 0, 0, value.getValueName(), this.gameParameters.font_phaseFourValueName);
                        text.anchor.set(0.5, 0.5);
                        valueSprite.addChild(text);
                        text.rotation = -3 * Math.PI / 6;

                    }
                }
            }
        }
    }

    public showPage(page:number){
        this.currentPage = page;
        for(var i:number=0;i<10;i++){
            var valueNumber:number = i+(page*10);
            if(this.occupiedTargets.indexOf(valueNumber) == -1) this.visibleValueSprites[valueNumber].visible = true;
            this.visibleValueSprites[i+((page+1)%2)*10].visible = false;
        }
    }


    private addDropListener(valueSprite, valueCounter, x, y) {
        var th = this;
        valueSprite.events.onDragStop.add(function () {
            th.dropValue(valueCounter, x, y);
        });
        return th;
    }

    private dropValue(valueNumber:number, x:number, y:number){
        //console.log("dropped value");
        var sprite:Phaser.Sprite = this.visibleValueSprites[valueNumber];
        var hit:boolean = false;
        for(var i in this.dropTargets) {
            var dropTarget:Phaser.Sprite = this.dropTargets[i];
            this.game.physics.enable(dropTarget, Phaser.Physics.ARCADE);

            this.game.physics.arcade.overlap(dropTarget, sprite, function () {
                hit = true;
            });
        }
        if(hit){
            sprite.visible = false;
            this.addValue(valueNumber);
        }
        sprite.x = x;
        sprite.y = y;

    }

    private addValue(valueNumber:number){
        if(this.occupiedTargets[this.occupiedTargets.length-1] != -1){
            this.visibleValueSprites[valueNumber].visible = true;
        }else {
            for (var i:number = 0; i < this.occupiedTargets.length; i++) {
                if (this.occupiedTargets[i] == -1) {
                    var dropTarget:Phaser.Sprite = this.dropTargets[i];
                    dropTarget.frame = this.frameNumber;
                    this.dropTexts[i].text = this.values[valueNumber].getValueName();
                    this.occupiedTargets[i] = valueNumber;

                    dropTarget.inputEnabled = true;
                    dropTarget.input.useHandCursor = true;
                    this.addReturnValueListener(dropTarget, valueNumber, i);
                    break;
                }
            }
        }
    }

    private addReturnValueListener(dropTarget, valueNumber, targetNumber) {
        var th = this;
        var event:Phaser.SignalBinding = dropTarget.events.onInputDown.add(function () {
            th.returnValue(valueNumber, targetNumber, event);
        }, this);
        return th;
    }

    private returnValue(valueNumber:number, targetNumber:number, event:Phaser.SignalBinding){
        this.visibleValueSprites[valueNumber].visible = true;
        this.visibleValueSprites[valueNumber].x = this.game.input.x-this.valueGroup.x;
        this.visibleValueSprites[valueNumber].y = this.game.input.y-this.valueGroup.y;
        this.visibleValueSprites[valueNumber].input.startDrag(this.game.input.activePointer);
        this.occupiedTargets[targetNumber] = -1;
        this.dropTargets[targetNumber].frame = 0;
        this.dropTexts[targetNumber].text = "";
        this.dropTargets[targetNumber].inputEnabled = false;
        event.active = false;
        console.log("returning value");
        console.log(this.visibleValues[valueNumber]);
    }

    private drawValueSprite(hor, i, xOffset, ver, j, scale, valueGroup, frame:number) {
        var valueSprite:Phaser.Sprite = new Phaser.Sprite(this.game, hor * i + xOffset, ver * j, "phaseFourDropTarget", frame);
        valueSprite.anchor.set(0.5, 0.5);
        valueSprite.rotation = 3 * Math.PI / 6;
        valueSprite.height = valueSprite.height * scale;
        valueSprite.width = valueSprite.width * scale;
        valueGroup.add(valueSprite);
        return valueSprite;
    }

    private drawClickToStart(){
        /*this.hexagonToStartSprite = new Phaser.Sprite(this.game, 405, 180, "clickHexagonToStart");
        this.group.add(this.hexagonToStartSprite);

        this.hexagonToStartTextBlack = new Phaser.Text(this.game, 445, 190, "CLICK ON ANY OF\nTHE HEXAGONS TO START\n\n\n\n\n\n\n", this.gameParameters.font_hexagonToStartBlack);
        this.group.add(this.hexagonToStartTextBlack);
        this.hexagonToStartTextBlack.align = "center";
        this.hexagonToStartTextBlack.lineSpacing = -7;

        this.hexagonToStartText = new Phaser.Text(this.game, 445, 190, "CLICK ON ANY OF\nTHE HEXAGONS TO START\n\n\n\n\n\n\n\n\n", this.gameParameters.font_hexagonToStart);
        this.group.add(this.hexagonToStartText);
        this.hexagonToStartText.align = "center";
        this.hexagonToStartText.lineSpacing = -7;

        var fadeTween:Phaser.Tween = this.game.add.tween(this.hexagonToStartText).to( { alpha: 0.2 }, 600, Phaser.Easing.Linear.None, true, 0, -1);
        fadeTween.yoyo(true);
        this.destroyEvent = this.game.input.onDown.add(this.removeClickToStart, this);
    */
    }

    private removeClickToStart():void{
    /*
        this.hexagonToStartSprite.destroy();
        this.hexagonToStartText.destroy();
        this.hexagonToStartTextBlack.destroy();
        this.destroyEvent.active = false;*/
    }


    accept(gameEvent:GameEvent) {
        if(gameEvent instanceof ValueRatedEvent){
            this.publish(gameEvent);
            /*this.ratedButtons++;
            var gameProgressEvent:GameProgressEvent = new GameProgressEvent(this, this.ratedButtons*100/this.totalButtons);
            this.publish(gameProgressEvent);
            this.checkFinished();*/
        }else if(gameEvent instanceof TimerTickEvent){
            var penalty = new ScorePenaltyEvent(this, this.pillar, 1);
            this.publish(penalty);
        }
    }

    subscribe(sub:GameEventSubscriber):void {
        this.subs.push(sub);
    }

    publish(gameEvent:GameEvent):void {
        for(var i in this.subs){
            this.subs[i].accept(gameEvent);
        }
    }


}