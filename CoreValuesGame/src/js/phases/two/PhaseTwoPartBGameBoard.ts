/**
 * Created by kobe on 4/4/2015.
 */
/// <reference path= "../../../../lib/phaser.d.ts" />
/// <reference path= "../../event/GameEventPublisher.ts" />
/// <reference path= "../../event/GameEventSubscriber.ts" />
/// <reference path= "../../event/PhaseTwoRoundEvent.ts" />
/// <reference path= "../../event/GameProgressEvent.ts" />
/// <reference path= "PhaseTwoPartBRound.ts" />
/// <reference path= "../all/Value.ts" />
/// <reference path= "../all/GamePhaseTracker.ts" />


class PhaseTwoPartBGameBoard implements GameEventSubscriber, GameEventPublisher{

    private subs:Array<GameEventSubscriber> = new Array<GameEventSubscriber>();
    private game:Phaser.Game;
    private group:Phaser.Group;
    private advancedValues:Array<Value>;
    private currentValue:number = 0;
    private gamePhaseTracker:GamePhaseTracker;

    constructor(game:Phaser.Game, group:Phaser.Group, gamePhaseTracker:GamePhaseTracker){
        this.game = game;
        var newGroup:Phaser.Group = new Phaser.Group(this.game);
        group.add(newGroup);
        this.group = newGroup;
        this.gamePhaseTracker = gamePhaseTracker;
    }

    public draw():void{
        var selectionBackground:Phaser.Sprite = this.game.add.sprite(410, 470,"phaseTwoSelectionBackground")
        this.startRounds();
    }

    public setAdvancedValues(advancedValues:Array<Value>){
        this.advancedValues = advancedValues;
    }

    private startRounds():void{
        this.startNextRound();
    }

    private startNextRound():void{
        if(this.currentValue<this.advancedValues.length) {
            this.gamePhaseTracker.setCopyRightColor(this.advancedValues[this.currentValue].getPillar().getColor());
            var round:PhaseTwoPartBRound = new PhaseTwoPartBRound(this.game, new Phaser.Group(this.game), this.advancedValues[this.currentValue++]);
            round.subscribe(this);
            round.draw();
            var gameProgressEvent:GameProgressEvent = new GameProgressEvent(this, (this.currentValue/this.advancedValues.length)*100);
            this.publish(gameProgressEvent);
        }else{
            console.log("finished");
        }
    }

    accept(gameEvent:GameEvent) {
        if(gameEvent instanceof PhaseTwoRoundEvent){
            this.startNextRound();
        }else {
            this.publish(gameEvent);
        }
    }

    subscribe(sub:GameEventSubscriber):void {
        this.subs.push(sub);
    }

    publish(gameEvent:GameEvent):void {
        for(var i in this.subs){
            this.subs[i].accept(gameEvent);
        }
    }



}