/// <reference path= "../../../../lib/phaser.d.ts" />
/// <reference path= "../../event/GameEventPublisher.ts" />
/// <reference path= "../../event/GameEventSubscriber.ts" />
/// <reference path= "../all/Value.ts" />
/// <reference path= "../all/ValueHexagon.ts" />
/// <reference path= "../../permanent/GameParameters.ts" />
/// <reference path= "../../event/ValueRatedEvent.ts" />
/// <reference path= "../../event/PhaseTwoRoundEvent.ts" />
/// <reference path= "../all/ScoreTimer.ts" />
/// <reference path= "../../event/TimerTickEvent.ts" />
/// <reference path= "../../event/ScorePenaltyEvent.ts" />

class PhaseTwoPartBRound implements GameEventPublisher, GameEventSubscriber{

    private game:Phaser.Game;
    private group:Phaser.Group;

    private subs:Array<GameEventSubscriber> = new Array<GameEventSubscriber>();
    private gameParameters:GameParameters = GameParameters.getInstance();
    private value:Value;

    private firstCircleSprite:Phaser.Sprite;
    private secondCircleSprite:Phaser.Sprite;
    private thirdCircleSprite:Phaser.Sprite;
    private scoreTimer:ScoreTimer;


    constructor(game:Phaser.Game, group:Phaser.Group, value:Value){
        this.game = game;
        var newGroup:Phaser.Group = new Phaser.Group(this.game);
        group.add(newGroup);
        this.group = newGroup;
        this.value = value;
    }

    public draw(){
        this.drawValue(255, 485);
        this.drawText();
        this.drawCircles();
        this.group.alpha = 0;
        var fadeTween:Phaser.Tween = this.game.add.tween(this.group).to( { alpha: 1 }, 500, Phaser.Easing.Linear.None, true, 0);
        this.drawScoreTimer();
    }

    private drawScoreTimer():void{
        this.scoreTimer = new ScoreTimer(this.game, this.game.add.group());
        this.scoreTimer.draw(1095, 150);
        this.scoreTimer.start(15);
        this.scoreTimer.subscribe(this);
        this.scoreTimer.setTickCutoff(10);
    }

    drawValue(x:number, y:number):void{
        this.drawHexagon(x, y, 130, 150, 35);
        this.drawName(x, y, 130, 150);
    }

    drawHexagon(x:number, y:number, trWidth:number, trHeight:number, topOffset:number):void{
        var shadowSprite:Phaser.Sprite = new Phaser.Sprite(this.game, x-6, y-4,"pillarShadow");
        this.group.add(shadowSprite);
        var hexagon:Phaser.Graphics = new Phaser.Graphics(this.game, 0, 0);
        var hexagonSprite:Phaser.Sprite = new Phaser.Sprite(this.game, x, y);
        hexagonSprite.addChild(hexagon);
        this.group.add(hexagonSprite);
        var points: Phaser.Point[] = [];
        points.push(new Phaser.Point(0,trHeight-topOffset));
        points.push(new Phaser.Point(0,topOffset));
        points.push(new Phaser.Point(trWidth/2, 0));
        points.push(new Phaser.Point(trWidth,topOffset));
        points.push(new Phaser.Point(trWidth,trHeight-topOffset));
        points.push(new Phaser.Point(trWidth/2, trHeight));
        hexagon.beginFill(this.gameParameters.convertColorStringToNumber(this.value.getPillar().getColor()), 1);
        hexagon.drawPolygon(points);

    }

    drawName(x:number, y:number, trWidth:number, trHeight:number):void{
        var text:Phaser.Text = new Phaser.Text(this.game, x + trWidth/2, y + trHeight/2, this.value.getValueName(), this.gameParameters.font_phaseTwoBValueName);
        text.anchor.set(0.5, 0.5);
        text.align = 'center';
        text.wordWrap = true;
        text.wordWrapWidth = 110;
        this.group.add(text);
    }

    private drawText():void{
        var titleText:Phaser.Text = new Phaser.Text(this.game, 460, 485, "Check the Option that Applies Most to this Value in Your Life", this.gameParameters.font_phaseTwoBTitleText);
        this.group.add(titleText);
        var choiceOne:Phaser.Text = new Phaser.Text(this.game, 470, 525, "I need to \nuse this\nvalue more \n ", this.gameParameters.getPhaseTwoBChoiceFont(this.value.getPillar().getColor()));
        this.group.add(choiceOne);
        var choiceTwo:Phaser.Text = new Phaser.Text(this.game, 650, 535, "I use this\nvalue often \n ", this.gameParameters.getPhaseTwoBChoiceFont(this.value.getPillar().getColor()));
        this.group.add(choiceTwo);
        var choiceThree:Phaser.Text = new Phaser.Text(this.game, 830, 535, "I use this\nvalue daily \n ", this.gameParameters.getPhaseTwoBChoiceFont(this.value.getPillar().getColor()));
        this.group.add(choiceThree);
        choiceOne.align = 'center';
        choiceTwo.align = 'center';
        choiceThree.align = 'center';
        choiceOne.lineSpacing = -5;
        choiceTwo.lineSpacing = -5;
        choiceThree.lineSpacing = -5;

    }

    private drawCircles():void{
        this.firstCircleSprite = this.drawCircleSprite(515, 625);
        this.secondCircleSprite = this.drawCircleSprite(695, 625);
        this.thirdCircleSprite = this.drawCircleSprite(875, 625);
        this.firstCircleSprite.events.onInputDown.add(this.chooseFirst, this);
        this.secondCircleSprite.events.onInputDown.add(this.chooseSecond, this);
        this.thirdCircleSprite.events.onInputDown.add(this.chooseThird, this);
    }

    private chooseFirst():void{
        this.firstCircleSprite.destroy();
        this.secondCircleSprite.inputEnabled = false;
        this.thirdCircleSprite.inputEnabled = false;
        var checkSprite:Phaser.Sprite = new Phaser.Sprite(this.game, 500, 610, "checkSign");
        this.group.add(checkSprite);
        this.publish(new ValueRatedEvent(this, this.value, 1, -2));
        this.endRound();
    }

    private chooseSecond():void{
        this.secondCircleSprite.destroy();
        this.firstCircleSprite.inputEnabled = false;
        this.thirdCircleSprite.inputEnabled = false;
        var checkSprite:Phaser.Sprite = new Phaser.Sprite(this.game, 680, 610, "checkSign");
        this.group.add(checkSprite);
        this.publish(new ValueRatedEvent(this, this.value, 6, -1));
        this.endRound();
    }

    private chooseThird():void{
        this.thirdCircleSprite.destroy();
        this.secondCircleSprite.inputEnabled = false;
        this.firstCircleSprite.inputEnabled = false;
        var checkSprite:Phaser.Sprite = new Phaser.Sprite(this.game, 860, 610, "checkSign");
        this.group.add(checkSprite);
        this.publish(new ValueRatedEvent(this, this.value, 11, 0));
        this.endRound();
    }

    private drawCircleSprite(x:number, y:number):Phaser.Sprite{
        var circle:Phaser.Graphics = new Phaser.Graphics(this.game, 0, 0);
        circle.lineStyle(3, 0xffffff);
        circle.drawCircle(10, 10, 20);
        var circleSprite:Phaser.Sprite = new Phaser.Sprite(this.game, x-10, y-10);
        circleSprite.addChild(circle);
        this.group.add(circleSprite);
        circleSprite.inputEnabled = true;
        circleSprite.input.useHandCursor = true;
        return circleSprite;
    }

    private endRound():void{
        this.scoreTimer.stop();
        var timer:Phaser.Timer = this.game.time.create(true);
        timer.add(200, this.removeRound, this);
        timer.start();
    }

    private removeRound():void{
        this.group.destroy();
        this.publish(new PhaseTwoRoundEvent(this, 0, 0));
    }

    accept(gameEvent:GameEvent) {
        if(gameEvent instanceof TimerTickEvent){
            this.publish(new ScorePenaltyEvent(this, this.value.getPillar(), 1));
        }
    }

    subscribe(sub:GameEventSubscriber):void {
        this.subs.push(sub);
    }

    publish(gameEvent:GameEvent):void {
        for(var i in this.subs){
            this.subs[i].accept(gameEvent);
        }
    }

}