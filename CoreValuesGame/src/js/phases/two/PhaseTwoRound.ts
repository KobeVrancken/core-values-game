/// <reference path= "../../../../lib/phaser.d.ts" />
/// <reference path= "../../event/GameEventPublisher.ts" />
/// <reference path= "../../event/GameEventSubscriber.ts" />
/// <reference path= "../all/Value.ts" />
/// <reference path= "../all/ValueHexagon.ts" />
/// <reference path= "../../permanent/GameParameters.ts" />
/// <reference path= "../../event/ValueRatedEvent.ts" />
/// <reference path= "../../event/PhaseTwoRoundEvent.ts" />


class PhaseTwoRound implements GameEventPublisher, GameEventSubscriber{

    private game:Phaser.Game;
    private group:Phaser.Group;
    private fadeGroup:Phaser.Group;
    private fadeGroupTwo:Phaser.Group;

    private subs:Array<GameEventSubscriber> = new Array<GameEventSubscriber>();
    private values:Array<Value>;
    public dropTargets:Array<Phaser.Sprite> = new Array<Phaser.Sprite>();
    public occupiedTargets:Array<Value> = new Array<Value>();
    private continueButton:Phaser.Sprite;
    private forcedContinueButton:Phaser.Sprite;
    private bonusValueButton:Phaser.Sprite;
    private gameParameters:GameParameters = GameParameters.getInstance();
    private bonusValueAnimation:Phaser.Sprite;
    private bonusValuesLeft:number;
    private valuesAdvanced:number;

    constructor(game:Phaser.Game, group:Phaser.Group, values:Array<Value>, valuesAdvanced:number,  bonusValuesLeft:number){
        this.game = game;
        var newGroup:Phaser.Group = new Phaser.Group(this.game);
        group.add(newGroup);
        this.group = newGroup;
        this.fadeGroup = new Phaser.Group(this.game);
        newGroup.add(this.fadeGroup);
        this.fadeGroupTwo = new Phaser.Group(this.game);
        newGroup.add(this.fadeGroupTwo);
        this.values = values;
        this.bonusValuesLeft = bonusValuesLeft;
        this.valuesAdvanced = valuesAdvanced;
    }

    public draw(){
        this.drawDropTargets();
        this.drawButtons();
        this.drawContinueButtons();
        this.fadeGroup.alpha = 0.8;
        var fadeTween:Phaser.Tween = this.game.add.tween(this.fadeGroup).to( { alpha: 0.4 }, 1000, Phaser.Easing.Linear.None, true, 0, -1);
        fadeTween.yoyo(true)
        this.fadeGroupTwo.alpha = 0.9;
        var fadeTweenTwo:Phaser.Tween = this.game.add.tween(this.fadeGroupTwo).to( { alpha: 0.8 }, 1000, Phaser.Easing.Linear.None, true, 0, -1);
        fadeTweenTwo.yoyo(true)
        this.drawBonusValueButton();
        this.drawAdvancedCounter();
    }

    private drawButtons():void{
        var map:Array<Array<string>> = [['1','1','1'],['1','0'],['1','0','0'],['1','0'],['1','1','1']];
        var horizontalSpacing = 90;
        var verticalSpacing = 80;
        var startX = 110;
        var startY = 250;
        var y = startY;
        var val = 0;
        for (var i in map) {
            var x = startX;
            if (i % 2 == 1) {
                x += horizontalSpacing / 2;
            }
            for (var j in map[i]) {
                if (map[i][j] === "1") {
                    var valueHexagon:ValueHexagon = new ValueHexagon(this.game, this.group);
                    if(val<this.values.length) valueHexagon.setValue(this.values[val++]);
                    valueHexagon.draw(x, y);
                    if(valueHexagon.hasValue()) {
                        valueHexagon.getDragSprite().input.enableDrag();
                        var obj:Object = {game: this.game, round: this, hexagon: valueHexagon, x: valueHexagon.getDragSprite().x, y: valueHexagon.getDragSprite().y};
                        valueHexagon.getDragSprite().events.onDragStart.add(this.startDrag, obj);
                        valueHexagon.getDragSprite().events.onDragStop.add(this.stopDrag, obj);
                    }
                }
                x += horizontalSpacing;
            }
            y += verticalSpacing;
        }
    }

    private drawContinueButtons():void{
        var x:number = 790;
        var y:number = 240;
        this.continueButton = new Phaser.Sprite(this.game, x, y, "phaseTwoContinueButton");
        this.forcedContinueButton = new Phaser.Sprite(this.game, x, y, "phaseTwoForcedContinueButton");
        this.fadeGroupTwo.add(this.continueButton);
        this.fadeGroupTwo.add(this.forcedContinueButton);
        this.continueButton.inputEnabled = true;
        this.continueButton.events.onInputDown.add(this.endRound, this);
        this.continueButton.input.useHandCursor = true;
        this.forcedContinueButton.inputEnabled = true;
        this.forcedContinueButton.input.useHandCursor = true;
        this.forcedContinueButton.events.onInputDown.add(this.endRound, this);
        this.hideButtons();
    }

    private startDrag():void{
        var th:any = this;
        var val:Value = th.hexagon.getValue();
        var occupied:Array<Value> = th.round.occupiedTargets;
        var index:number = occupied.indexOf(val);
        if(index >= 0) th.round.occupiedTargets[index] = null;
    }

    private stopDrag():void{
        var th:any = this;
        var hex:Phaser.Sprite = th.hexagon.getDragSprite();
        th.game.physics.enable(hex, Phaser.Physics.ARCADE);
        var goodTargets:Array<Phaser.Sprite> = new Array<Phaser.Sprite>();
        for(var i in th.round.dropTargets){
            var dropTarget:Phaser.Sprite = th.round.dropTargets[i];
            th.game.physics.enable(dropTarget, Phaser.Physics.ARCADE);
            th.game.physics.arcade.overlap(dropTarget, hex, function() {
                if(th.round.occupiedTargets[i] == null)
                    goodTargets.push(dropTarget);
            });
        }

        if(goodTargets.length == 0){
            hex.x = th.x;
            hex.y = th.y;
        } else {
            var shortestDistance:number = -1;
            var shortestIndex:number = -1;
            for (var i in goodTargets) {
                var dist:number = th.game.physics.arcade.distanceBetween(goodTargets[i], hex);
                if (dist < shortestDistance || shortestDistance == -1) {
                    shortestIndex = i;
                    shortestDistance = dist;
                }
            }
            hex.x = goodTargets[shortestIndex].x-1;
            hex.y = goodTargets[shortestIndex].y-1;
            for (var i in th.round.dropTargets) {
                if (th.round.dropTargets[i] === goodTargets[shortestIndex]) {
                    th.round.occupiedTargets[i] = th.hexagon.getValue();
                }
            }

        }

        th.round.checkButtonVisibility();


    }

    public checkButtonVisibility():void{
        var count:number = 0;
        for (var i in this.occupiedTargets) {
            if (this.occupiedTargets[i] != null) {
                count++;
            }
        }
        if(count == this.occupiedTargets.length){
            this.showForcedContinueButton();
        }else if(count > 0) {
            this.showContinueButton();
        } else {
            this.hideButtons();
        }
    }

    public hideButtons():void{
        this.continueButton.visible = false;
        this.forcedContinueButton.visible = false;
    }

    public showContinueButton():void{
        this.hideButtons();
        this.continueButton.visible = true;
    }

    public showForcedContinueButton():void{
        this.hideButtons();
        this.forcedContinueButton.visible = true;
    }



    private drawDropTargets():void{
        var amount:number = 1;
        if(this.values.length > 5) amount = 3;
        else if(this.values.length > 3) amount = 2;


        if(amount >= 1){
            var firstTarget:Phaser.Sprite = new Phaser.Sprite(this.game, 930, 350, "phaseTwoDragTarget");
            this.dropTargets.push(firstTarget);
            this.occupiedTargets.push(null);
        }
        if(amount >= 2){
            var secondTarget:Phaser.Sprite = new Phaser.Sprite(this.game, 930+45, 350+78, "phaseTwoDragTarget");
            this.dropTargets.push(secondTarget);
            this.occupiedTargets.push(null);
        }
        if(amount >= 3){
            var thirdTarget:Phaser.Sprite = new Phaser.Sprite(this.game, 930, 350+156, "phaseTwoDragTarget");
            this.dropTargets.push(thirdTarget);
            this.occupiedTargets.push(null);
        }


        for(var i in this.dropTargets){
            var target:Phaser.Sprite = this.dropTargets[i];;
            this.fadeGroup.add(target);
        }
    }

    private drawBonusValueButton():void{
        if(this.bonusValuesLeft>0) {
            this.bonusValueButton = new Phaser.Sprite(this.game, 445, 530, "phaseTwoBonusValue");
            this.group.add(this.bonusValueButton);
            var bonusText = new Phaser.Text(this.game, 28, 35, "BONUS ", this.gameParameters.font_bonusValueWhite);
            var valueText = new Phaser.Text(this.game, 17, 55, "VALUE ", this.gameParameters.font_bonusValueYellow);
            var countText = new Phaser.Text(this.game, 60, 90, (10 - this.bonusValuesLeft + 1) + " of 10 ", this.gameParameters.font_bonusValueWhite);
            countText.anchor.set(0.5, 0);
            this.bonusValueButton.addChild(bonusText);
            this.bonusValueButton.addChild(valueText);
            this.bonusValueButton.addChild(countText);
            this.bonusValueButton.inputEnabled = true;
            this.bonusValueButton.input.useHandCursor = true;
            this.bonusValueButton.events.onInputDown.add(this.animateBonusValue, this);
        }
    }

    private animateBonusValue():void{
        this.bonusValuesLeft--;
        this.bonusValueButton.destroy();
        this.bonusValueAnimation = new Phaser.Sprite(this.game, 930-42, 350+48, "phaseTwoTargetAnimation");
        this.bonusValueAnimation.anchor.set(0.5, 0.5);
        this.group.add(this.bonusValueAnimation);
        this.bonusValueAnimation.alpha = 0.8;
        var rotateTween:Phaser.Tween = this.game.add.tween(this.bonusValueAnimation).to( { angle: +1440, width: 85, height: 98, alpha: 0 }, 2000, Phaser.Easing.Linear.None, true, 0);
        rotateTween.onComplete.add(this.destroyAnimation, this);

        var bonusValueTarget:Phaser.Sprite = new Phaser.Sprite(this.game, 930-90, 350, "phaseTwoDragTarget");
        bonusValueTarget.alpha = 0;
        this.game.add.tween(bonusValueTarget).to( { alpha: 1 }, 1, Phaser.Easing.Linear.None, true, 1500, 1);
        this.dropTargets.push(bonusValueTarget);
        this.occupiedTargets.push(null);
        this.fadeGroup.add(bonusValueTarget);
        this.checkButtonVisibility();
    }

    private destroyAnimation():void{
        this.bonusValueAnimation.destroy();
    }

    private drawAdvancedCounter():void{
        var trHeight:number = 85;
        var trWidth:number = 75;
        var topOffset:number = 20;
        var counter:Phaser.Sprite = new Phaser.Sprite(this.game, 980, 575);
        this.group.add(counter);

        var hexagon:Phaser.Graphics = new Phaser.Graphics(this.game, 0, 0);
        var points: Phaser.Point[] = [];
        points.push(new Phaser.Point(0,trHeight-topOffset));
        points.push(new Phaser.Point(0,topOffset));
        points.push(new Phaser.Point(trWidth/2, 0));
        points.push(new Phaser.Point(trWidth,topOffset));
        points.push(new Phaser.Point(trWidth,trHeight-topOffset));
        points.push(new Phaser.Point(trWidth/2, trHeight));
        hexagon.beginFill(0x00000, 0.8);
        hexagon.drawPolygon(points);
        counter.addChild(hexagon);

        var countTitle:Phaser.Text = new Phaser.Text(this.game, 39, 15, "Values ", this.gameParameters.font_valuesAdvancedYellow);
        countTitle.anchor.set(0.5, 0);
        counter.addChild(countTitle);
        var countTitle2:Phaser.Text = new Phaser.Text(this.game, 39, 30, "Advanced ", this.gameParameters.font_valuesAdvancedYellow);
        countTitle2.anchor.set(0.5, 0);
        counter.addChild(countTitle2);
        var count:Phaser.Text = new Phaser.Text(this.game, 39, 45, this.valuesAdvanced+" ", this.gameParameters.font_valuesAdvancedWhite);
        count.anchor.set(0.5, 0);
        counter.addChild(count);

    }

    private endRound():void{
        for(var i in this.occupiedTargets){
            var value:Value = this.occupiedTargets[i];
            if(value != null){
                this.publish(new ValueRatedEvent(this, value, 11, 0.5));
                this.valuesAdvanced++;
            }
        }
        this.publish(new PhaseTwoRoundEvent(this, this.valuesAdvanced, this.bonusValuesLeft));
        this.group.destroy();
    }



    accept(gameEvent:GameEvent) {
    }

    subscribe(sub:GameEventSubscriber):void {
        this.subs.push(sub);
    }

    publish(gameEvent:GameEvent):void {
        for(var i in this.subs){
            this.subs[i].accept(gameEvent);
        }
    }

}