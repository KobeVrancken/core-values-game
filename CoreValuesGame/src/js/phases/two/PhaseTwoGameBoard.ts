/// <reference path= "../../../../lib/phaser.d.ts" />
/// <reference path= "../../event/GameEventPublisher.ts" />
/// <reference path= "../../event/GameEventSubscriber.ts" />
/// <reference path= "../../event/PhaseTwoRoundEvent.ts" />
/// <reference path= "../../event/GameProgressEvent.ts" />
/// <reference path= "PhaseTwoRound.ts" />
/// <reference path= "../all/Value.ts" />
/// <reference path= "../../permanent/GameState.ts" />

class PhaseTwoGameBoard implements GameEventSubscriber, GameEventPublisher{

    private subs:Array<GameEventSubscriber> = new Array<GameEventSubscriber>();
    private game:Phaser.Game;
    private group:Phaser.Group;

    private rockForwardTween:Phaser.Tween;
    private rockBackwardTween:Phaser.Tween;
    private yachtSprite:Phaser.Sprite;

    private smallArrowSprite:Phaser.Sprite;
    private bigArrowSprite:Phaser.Sprite;
    private advancedValues:Array<Value>;
    private destroyEvent:Phaser.SignalBinding;
    private round:number = 0;
    private uniqueGroups:number = 0;
    private biggestGroup:string;
    private choiceGroups:number = 0;
    private gameState:GameState;

    constructor(game:Phaser.Game, group:Phaser.Group, gameState:GameState){
        this.game = game;
        var newGroup:Phaser.Group = new Phaser.Group(this.game);
        group.add(newGroup);
        this.group = newGroup;
        this.gameState = gameState;
    }

    public draw():void{
        this.drawSmallBoat();
        this.drawDragInstructions();
        this.drawSmallArrow();
        this.dragBigArrow();
        this.destroyEvent = this.game.input.onDown.add(this.removeArrows, this);
        this.startRounds();
    }

    public setAdvancedValues(advancedValues:Array<Value>){
        this.advancedValues = advancedValues;
        this.advancedValues.sort(Value.compareBigGroup);
        var previousGroup:string = '0';
        var lastValueChanged:boolean = false;
        var wait:boolean = true;
        for(var i in this.advancedValues){
            if(!lastValueChanged && !wait){
                this.choiceGroups++;
                wait = true;
            }
            if(this.advancedValues[i].getBigGroup() != previousGroup){
                this.uniqueGroups++;
                previousGroup = this.advancedValues[i].getBigGroup();
                lastValueChanged = true;
                wait = false;
            }else{
                lastValueChanged = false;
            }

        }
        this.biggestGroup = this.advancedValues[this.advancedValues.length-1].getBigGroup();
    }

    private removeArrows():void{
        this.smallArrowSprite.destroy();
        this.bigArrowSprite.destroy();
        this.destroyEvent.active = false;
    }

    private drawSmallArrow():void{
        this.smallArrowSprite = new Phaser.Sprite(this.game, 565, 590, "smallArrow");
        this.smallArrowSprite.alpha = 0.8;
        this.group.add(this.smallArrowSprite);
        this.applyBounceAlphaTween(this.smallArrowSprite);
    }

    private dragBigArrow():void{
        this.bigArrowSprite = new Phaser.Sprite(this.game, 400, 250, "bigArrow");
        this.bigArrowSprite.alpha = 0.8;
        this.group.add(this.bigArrowSprite);
        this.applyBounceAlphaTween(this.bigArrowSprite);
    }

    private applyBounceAlphaTween(sprite:Phaser.Sprite){
        var fadeTween:Phaser.Tween = this.game.add.tween(sprite).to( { alpha: 0.2 }, 500, Phaser.Easing.Linear.None, true, 0, -1);
        fadeTween.yoyo(true);
    }


    private drawSmallBoat(){
        this.yachtSprite = new Phaser.Sprite(this.game, 900, 495, "phaseTwoYacht");
        this.yachtSprite.anchor.set(0.5, 0.5);
        this.group.add(this.yachtSprite);
        this.startRockBackward();
    }

    private startRockForward(){
        this.rockForwardTween = this.game.add.tween(this.yachtSprite).to({angle:'+6'}, 1500, Phaser.Easing.Quadratic.InOut, true, 100);
        this.rockForwardTween.onComplete.add(this.startRockBackward, this);
    }

    private startRockBackward(){
        this.rockBackwardTween = this.game.add.tween(this.yachtSprite).to({angle:'-6'}, 1500, Phaser.Easing.Quadratic.InOut, true, 100);
        this.rockBackwardTween.onComplete.add(this.startRockForward, this);
    }

    private drawDragInstructions(){
        var dragInstructions:Phaser.Sprite = new Phaser.Sprite(this.game, 640, 550, "phaseTwoDragInstructions");
        this.group.add(dragInstructions);

    }

    private startRounds():void{
        this.startNextRound(0, 10);
    }

    private ratedRound:number = 0;

    private startNextRound(valuesAdvanced:number, bonusValuesLeft:number):void{
        this.round++;
        var roundString:string = ""+this.round;
        if(parseInt(roundString) > parseInt(this.biggestGroup)){
            var gameProgressEvent:GameProgressEvent = new GameProgressEvent(this, 100);
            this.publish(gameProgressEvent);
            this.game.state.start("PhaseTwoPartB", true, false, this.gameState);
        }else {
            var values:Array<Value> = new Array<Value>();
            for(var valueIndex:number = 0;valueIndex<this.advancedValues.length;valueIndex++)
                if(this.advancedValues[valueIndex].getBigGroup() == roundString) values.push(this.advancedValues[valueIndex]);

            if (values.length == 0 || values.length == 1) {
                if (values.length == 1) {
                    console.log("Only 1 value in group, rating.");
                    this.publish(new ValueRatedEvent(this, values[0], 11, 0.5));
                }
                this.startNextRound(valuesAdvanced, bonusValuesLeft);
            }else {
                var phaseTwoRound:PhaseTwoRound = new PhaseTwoRound(this.game, this.group, values, valuesAdvanced, bonusValuesLeft);
                phaseTwoRound.subscribe(this);
                phaseTwoRound.draw();
                var gameProgressEvent:GameProgressEvent = new GameProgressEvent(this, (this.ratedRound++/this.choiceGroups)*100);
                this.publish(gameProgressEvent);
            }
        }
    }





    accept(gameEvent:GameEvent) {;
        if(gameEvent instanceof PhaseTwoRoundEvent){
            this.startNextRound(gameEvent.getAdvanced(), gameEvent.getBonusValues());
        }else {
            this.publish(gameEvent);
        }
    }

    subscribe(sub:GameEventSubscriber):void {
        this.subs.push(sub);
    }

    publish(gameEvent:GameEvent):void {
        for(var i in this.subs){
            this.subs[i].accept(gameEvent);
        }
    }



}