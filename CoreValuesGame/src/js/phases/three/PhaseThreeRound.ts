/// <reference path= "../../../../lib/phaser.d.ts" />
/// <reference path= "../../event/GameEventPublisher.ts" />
/// <reference path= "../../event/GameEventSubscriber.ts" />
/// <reference path= "../all/Value.ts" />
/// <reference path= "../all/ValueHexagon.ts" />
/// <reference path= "../../permanent/GameParameters.ts" />
/// <reference path= "../../event/ValueRatedEvent.ts" />
/// <reference path= "../../event/PhaseTwoRoundEvent.ts" />
/// <reference path= "../all/ScoreTimer.ts" />


class PhaseThreeRound implements GameEventPublisher, GameEventSubscriber{

    private game:Phaser.Game;
    private group:Phaser.Group;

    private subs:Array<GameEventSubscriber> = new Array<GameEventSubscriber>();
    public values:Array<Value>;

    private gameParameters:GameParameters = GameParameters.getInstance();
    private valuesAdvanced:number;

    public notChosenGroups:Array<Phaser.Group> = new Array<Phaser.Group>();
    public chosenValue:Value = null;
    public hexagonSprites:Array<Phaser.Sprite> = new Array<Phaser.Sprite>();
    private amountOfTime:number = 10;
    private round:number;
    private scoreTimer:ScoreTimer;

    constructor(game:Phaser.Game, group:Phaser.Group, values:Array<Value>, valuesAdvanced:number, round:number){
        this.game = game;
        var newGroup:Phaser.Group = new Phaser.Group(this.game);
        group.add(newGroup);
        this.group = newGroup;
        this.values = values;
        this.valuesAdvanced = valuesAdvanced;
        this.round = round;
        if(round == 1){
            this.amountOfTime = 10;
        }else{
            this.amountOfTime = 6;
        }
    }

    public draw(){
        this.drawAdvancedCounter();
        this.drawValues();
        this.drawScoreTimer();
    }

    private drawAdvancedCounter():void{
        var trHeight:number = 85;
        var trWidth:number = 75;
        var topOffset:number = 20;
        var counter:Phaser.Sprite = new Phaser.Sprite(this.game, 980, 535);
        this.group.add(counter);

        var hexagon:Phaser.Graphics = new Phaser.Graphics(this.game, 0, 0);
        var points: Phaser.Point[] = [];
        points.push(new Phaser.Point(0,trHeight-topOffset));
        points.push(new Phaser.Point(0,topOffset));
        points.push(new Phaser.Point(trWidth/2, 0));
        points.push(new Phaser.Point(trWidth,topOffset));
        points.push(new Phaser.Point(trWidth,trHeight-topOffset));
        points.push(new Phaser.Point(trWidth/2, trHeight));
        hexagon.beginFill(0x00000, 0.8);
        hexagon.drawPolygon(points);
        counter.addChild(hexagon);

        var countTitle:Phaser.Text = new Phaser.Text(this.game, 39, 34, "Values ", this.gameParameters.font_valuesAdvancedYellow);
        countTitle.anchor.set(0.5, 0);
        counter.addChild(countTitle);
        var countTitle2:Phaser.Text = new Phaser.Text(this.game, 39, 49, "Saved ", this.gameParameters.font_valuesAdvancedYellow);
        countTitle2.anchor.set(0.5, 0);
        counter.addChild(countTitle2);
        var count:Phaser.Text = new Phaser.Text(this.game, 39, 10, this.valuesAdvanced+" ", this.gameParameters.font_valuesAdvancedWhite);
        count.anchor.set(0.5, 0);
        counter.addChild(count);

    }

    private drawValues(){
        var xOffset:number = 275;
        var yOffset:number = -100;
        var x:number = 200;
        var y:number = 470;
        for(var i in this.values){
            var group:Phaser.Group = new Phaser.Group(this.game);
            var biggerGroup:Phaser.Group = new Phaser.Group(this.game);
            biggerGroup.add(group);
            this.group.add(biggerGroup);
            biggerGroup.x = x+xOffset*i;
            biggerGroup.y = y+yOffset*i+180;
            var valueHexagon:ValueHexagon = new ValueHexagon(this.game, group);
            valueHexagon.setValue(this.values[i]);
            valueHexagon.setScale(1.7-i/10);
            valueHexagon.disableDashedHexagon();
            valueHexagon.draw(0, 0);
            group.angle = -2;
            var game:Phaser.Game = this.game;
            this.addTweens(group, game);
            //var rockTween:Phaser.Tween = this.game.add.tween(group).to({height:'-5', y:'+2', angle:'+4'}, 2000, Phaser.Easing.Quadratic.InOut , true, 0, -1);
            //rockTween.yoyo(true);

            var graphics = new Phaser.Graphics(this.game, x+xOffset*i, y+yOffset*i);
            graphics.beginFill(0xFF3300);
            graphics.lineStyle(10, 0xffd900, 1);
            graphics.moveTo(0,0);
            graphics.lineTo(0, 110);
            graphics.lineTo(200, 140);
            graphics.lineTo(200, 0);
            graphics.lineTo(0, 0);
            graphics.endFill();
            this.group.add(graphics);
            group.mask = graphics;
            var hexagonSprite:Phaser.Sprite = valueHexagon.getHexagonSprite();
            this.hexagonSprites.push(hexagonSprite);
            this.notChosenGroups.push(biggerGroup);

            this.addClick(hexagonSprite, game, biggerGroup, graphics, biggerGroup.x, biggerGroup.y, this, i);
        }
    }

    public end():void{
        this.scoreTimer.stop();
        for(var i in this.hexagonSprites){
            this.hexagonSprites[i].inputEnabled = false;
        }
        for(var i in this.notChosenGroups){
            this.game.add.tween(this.notChosenGroups[i]).to({y: '+180'}, 1500, Phaser.Easing.Quadratic.Out, true);
        }
        var timer:Phaser.Timer = this.game.time.create(true);
        timer.add(1000, this.remove, this);
        timer.start();
    }

    public remove():void{
        this.scoreTimer.stop();
        this.group.destroy();
        if(this.chosenValue != null){
                this.publish(new ValueRatedEvent(this, this.chosenValue, 5, 0));
                this.valuesAdvanced++;
        }
        for(var i in this.values){
            if(this.chosenValue != this.values[i]){
                this.publish(new ValueRatedEvent(this, this.values[i], 0, -0.5));
            }
        }
        this.publish(new PhaseTwoRoundEvent(this, this.valuesAdvanced, 0));
        console.log("Chosen value:");
        console.log(this.chosenValue);
    }

    private addClick(hexagonSprite, game, group, maskGraphics:Phaser.Graphics, x, y, phaseThreeRound:PhaseThreeRound, j) {
        hexagonSprite.events.onInputDown.add(function () {
            for(var i in phaseThreeRound.hexagonSprites){
                phaseThreeRound.hexagonSprites[i].inputEnabled = false;
            }
            phaseThreeRound.notChosenGroups[j] = null;
            phaseThreeRound.chosenValue = phaseThreeRound.values[j];
            maskGraphics.destroy();
            var lifeBand:Phaser.Sprite = new Phaser.Sprite(game, -90, -230, "lifeBand");
            group.add(lifeBand);
            game.add.tween(group).to({x: ''+(900-x), y:''+(600-y), width:'-250', height:'-200'}, 1000, Phaser.Easing.Quadratic.Out, true);
            phaseThreeRound.end();
        })
    }



    private addTweens(group, game) {
        this.game.add.tween(group).to({y: '-180'}, 1500, Phaser.Easing.Quadratic.Out, true).onComplete.add(function () {
            var rockTween:Phaser.Tween = game.add.tween(group).to({
                height: '-5',
                y: '+2',
                angle: '+4'
            }, 2000, Phaser.Easing.Quadratic.InOut, true, 0, -1);
            rockTween.yoyo(true);
        });
    }

    private drawScoreTimer():void{
        this.scoreTimer = new ScoreTimer(this.game, this.game.add.group());
        this.scoreTimer.draw(1095, 150);
        this.scoreTimer.start(this.amountOfTime);
        this.scoreTimer.subscribe(this);
    }


    accept(gameEvent:GameEvent) {
        if(gameEvent instanceof TimerEndEvent){
            this.end();
        }
    }

    subscribe(sub:GameEventSubscriber):void {
        this.subs.push(sub);
    }

    publish(gameEvent:GameEvent):void {
        for(var i in this.subs){
            this.subs[i].accept(gameEvent);
        }
    }

}