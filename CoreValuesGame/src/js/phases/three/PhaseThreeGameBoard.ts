/// <reference path= "../../../../lib/phaser.d.ts" />
/// <reference path= "../../event/GameEventPublisher.ts" />
/// <reference path= "../../event/GameEventSubscriber.ts" />
/// <reference path= "../../event/PhaseTwoRoundEvent.ts" />
/// <reference path= "../../permanent/GameParameters.ts" />

/// <reference path= "../../event/GameProgressEvent.ts" />
/// <reference path= "../all/Value.ts" />
/// <reference path= "../../permanent/GameState.ts" />
/// <reference path= "PhaseThreeRound.ts" />

class PhaseThreeGameBoard implements GameEventSubscriber, GameEventPublisher{

    private subs:Array<GameEventSubscriber> = new Array<GameEventSubscriber>();
    private game:Phaser.Game;
    private group:Phaser.Group;

    private gameState:GameState;
    private advancedValues:Array<Value>;
    private instructionsText:Phaser.Text;
    private instructionsSprite:Phaser.Sprite;
    private gameParameters:GameParameters = GameParameters.getInstance();
    private currentValue:number = 0;
    private destroyEvent:Phaser.SignalBinding;
    private tapToClick:Phaser.Sprite;

    constructor(game:Phaser.Game, group:Phaser.Group, gameState:GameState){
        this.game = game;
        var newGroup:Phaser.Group = new Phaser.Group(this.game);
        group.add(newGroup);
        this.group = newGroup;
        this.gameState = gameState;
    }

    public draw():void{
        this.drawInstructionsText();
    }

    public setAdvancedValues(advancedValues:Array<Value>){
        this.advancedValues = advancedValues;
    }

    private drawInstructionsText(){
        this.instructionsText = new Phaser.Text(this.game, 400, 350, "Use your mouse to click or tap on\n" +
        "your touchpad to save the value\n" +
        "that matters the most to you.\n\n" +
        "Save your most important values\n" +
        "before they drown.", this.gameParameters.font_phaseThreeInstructions);
        this.instructionsSprite = new Phaser.Sprite(this.game, 0, 0);
        this.group.add(this.instructionsSprite);
        this.instructionsSprite.addChild(this.instructionsText);
        this.instructionsText.align = 'center';
        var rockTween:Phaser.Tween = this.game.add.tween(this.instructionsText).to({height:'-20', y:'+10', angle:'+1'}, 2000, Phaser.Easing.Quadratic.InOut , true, 0, -1);
        rockTween.yoyo(true);

        var timer:Phaser.Timer = this.game.time.create(true);
        timer.add(5000, this.fadeInstructions, this);
        timer.start();
    }

    private fadeInstructions(){
        var fadeTween:Phaser.Tween = this.game.add.tween(this.instructionsSprite).to({alpha:'-1'}, 2000, Phaser.Easing.Linear.None , true);
        fadeTween.onComplete.add(this.startRounds, this);
    }

    private drawLifeRaft(){
        var lifeRaftMoving = this.game.add.sprite(800, 500, "lifeRaftMoving");
        var rockForwardTween:Phaser.Tween = this.game.add.tween(lifeRaftMoving).to({height:'-20', y:'+10'}, 1500, Phaser.Easing.Quadratic.InOut , true, 0, -1);
        rockForwardTween.yoyo(true);
    }


    private startRounds():void{
        this.drawTapToClick();
        this.destroyEvent = this.game.input.onDown.add(this.removeTapToClick, this);
        this.instructionsSprite.destroy();
        this.drawLifeRaft();
        this.startNextRound(0);
    }

    private drawTapToClick():void{
        this.tapToClick = new Phaser.Sprite(this.game, 400, 260, "phaseThreeTapToClick");
        this.group.add(this.tapToClick);
    }

    private removeTapToClick():void{
        this.tapToClick.destroy();
        this.destroyEvent.active = false;
    }


    private startNextRound(advancedValues:number):void{
        if(this.hasNextValues()) {
            var phaseThreeRound:PhaseThreeRound = new PhaseThreeRound(this.game, new Phaser.Group(this.game), this.getNextValues(), advancedValues, 1);
            phaseThreeRound.subscribe(this);
            phaseThreeRound.draw();
        }
        var gameProgressEvent:GameProgressEvent = new GameProgressEvent(this, (advancedValues/this.advancedValues.length)*100);
        this.publish(gameProgressEvent);
    }

    private hasNextValues():boolean{
        return this.currentValue < this.advancedValues.length;
    }

    private getNextValues():Array<Value>{
        var result:Array<Value> = [];
        var amount:number = 3;
        while(amount>0){
            amount--;
            if(this.hasNextValues()){
                result.push(this.advancedValues[this.currentValue++]);
            }
        }
        return result;
    }





    accept(gameEvent:GameEvent) {;
        if(gameEvent instanceof PhaseTwoRoundEvent){
            this.startNextRound(gameEvent.getAdvanced());
        }else {
            this.publish(gameEvent);
        }
    }

    subscribe(sub:GameEventSubscriber):void {
        this.subs.push(sub);
    }

    publish(gameEvent:GameEvent):void {
        for(var i in this.subs){
            this.subs[i].accept(gameEvent);
        }
    }



}