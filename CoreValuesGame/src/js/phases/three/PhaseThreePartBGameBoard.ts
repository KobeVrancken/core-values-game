/// <reference path= "../../../../lib/phaser.d.ts" />
/// <reference path= "../../event/GameEventPublisher.ts" />
/// <reference path= "../../event/GameEventSubscriber.ts" />
/// <reference path= "../../event/PhaseTwoRoundEvent.ts" />
/// <reference path= "../../permanent/GameParameters.ts" />

/// <reference path= "../../event/GameProgressEvent.ts" />
/// <reference path= "../all/Value.ts" />
/// <reference path= "../../permanent/GameState.ts" />
/// <reference path= "PhaseThreeRound.ts" />
/// <reference path= "../../permanent/GameParameters.ts" />


class PhaseThreePartBGameBoard implements GameEventSubscriber, GameEventPublisher{

    private subs:Array<GameEventSubscriber> = new Array<GameEventSubscriber>();
    private game:Phaser.Game;
    private group:Phaser.Group;

    private gameState:GameState;
    private advancedValues:Array<Value>;
    private gameParameters:GameParameters = GameParameters.getInstance();
    private raft:Phaser.Sprite;
    private currentValue:number = 0;
    private keyboardInstructionsGroup:Phaser.Group;
    private gameMask:Phaser.Graphics;
    private loopTimer:Phaser.Timer;
    private valuesAdvanced:number = 0;
    private valuesAdvancedText:Phaser.Text;


    constructor(game:Phaser.Game, group:Phaser.Group, gameState:GameState, raft:Phaser.Sprite){
        this.game = game;
        var newGroup:Phaser.Group = new Phaser.Group(this.game);
        group.add(newGroup);
        this.group = newGroup;
        this.gameState = gameState;
        this.raft = raft;
    }

    public draw():void{
        this.drawInstructionsText();
    }

    public setAdvancedValues(advancedValues:Array<Value>){
        this.advancedValues = advancedValues;
    }

    private drawInstructionsText(){
        this.keyboardInstructionsGroup = new Phaser.Group(this.game);
        var inputSprite:Phaser.Sprite = new Phaser.Sprite(this.game, 375, 250, "keyboardInstructions");
        this.keyboardInstructionsGroup.add(inputSprite);
        var arrowSprite:Phaser.Sprite = new Phaser.Sprite(this.game, 130, 580, "moveArrowsTwo");
        this.keyboardInstructionsGroup.add(arrowSprite);
        this.keyboardInstructionsGroup.alpha = 0.8;
        var fadeTween:Phaser.Tween = this.game.add.tween(this.keyboardInstructionsGroup).to( { alpha: 0.4 }, 700, Phaser.Easing.Linear.None, true, 0, -1);
        fadeTween.yoyo(true);
        arrowSprite.alpha = 0.8;
        var timer:Phaser.Timer = this.game.time.create(true);
        timer.add(5000, this.fadeInstructions, this);
        timer.start();
    }

    private fadeInstructions(){
        var fadeTween:Phaser.Tween = this.game.add.tween(this.keyboardInstructionsGroup).to({alpha:'-1'}, 2000, Phaser.Easing.Linear.None , true);
        fadeTween.onComplete.add(this.startRounds, this);
    }


    private startRounds():void{
        this.drawAdvancedCounter();
        this.keyboardInstructionsGroup.destroy();
        this.drawGameMask();
        this.loopTimer = this.game.time.create(true);
        this.drawNextValue();
        this.loopTimer.loop(2000, this.drawNextValue, this);
        this.loopTimer.start();
    }

    private drawGameMask():void{
        this.gameMask = this.game.add.graphics(90, 246);
        this.gameMask.beginFill(0xFF3300);
        this.gameMask.drawRoundedRect(0, 0, 1000, 444, 25);
        this.gameMask.endFill();
    }

    private drawNextValue():void{
        if(this.hasNextValues()){
            var value:Value = this.getNextValue();
            this.drawValue(value, this.currentValue%2 == 0);
            var gameProgressEvent:GameProgressEvent = new GameProgressEvent(this, (this.currentValue/this.advancedValues.length)*100);
            this.publish(gameProgressEvent);
        }else{
            this.loopTimer.stop();
            // round ended..!
        }
    }


    private hasNextValues():boolean{
        return this.currentValue < this.advancedValues.length;
    }

    private getNextValue():Value{
        return this.advancedValues[this.currentValue++];
    }




    private drawValue(val:Value, right:boolean){
        var xOffset:number = 0;
        var yOffset:number = 0;
        var x:number = 500;
        if(right) x = 600;
        var xMovement:number = 450;
        if(right) xMovement = -450;
        var y:number = 400;
        var group:Phaser.Group = new Phaser.Group(this.game);
        var biggerGroup:Phaser.Group = new Phaser.Group(this.game);
        var biggestGroup:Phaser.Group = new Phaser.Group(this.game);
        biggestGroup.add(biggerGroup);
        biggerGroup.add(group);
        this.group.add(biggestGroup);
        biggerGroup.x = x;
        biggerGroup.y = y;
        var valueHexagon:ValueHexagon = new ValueHexagon(this.game, group);
        valueHexagon.setValue(val);
        valueHexagon.setScale(1.6);
        valueHexagon.disableDashedHexagon();
        valueHexagon.draw(0, 0);
        group.angle = -2;
        var rockTween:Phaser.Tween = this.game.add.tween(group).to({height:'-3', angle:'+5'}, 1000, Phaser.Easing.Quadratic.InOut , true, 0, -1);
        rockTween.yoyo(true);
        var graphics = new Phaser.Graphics(this.game, x+xOffset, y+yOffset);
        graphics.beginFill(0xFF3300);
        graphics.lineStyle(10, 0xffd900, 1);
        graphics.moveTo(0,0);
        graphics.lineTo(0, 110);
        graphics.lineTo(200, 140);
        graphics.lineTo(200, 0);
        graphics.lineTo(0, 0);
        graphics.endFill();
        biggerGroup.add(graphics);
        group.mask = graphics;
        var hexagonSprite:Phaser.Sprite = valueHexagon.getHexagonSprite();
        hexagonSprite.inputEnabled = false;
        biggerGroup.width = biggerGroup.width/4;
        biggerGroup.height = biggerGroup.height/4;
        graphics.x = 0;
        graphics.y = 0;
        biggerGroup.mask = this.gameMask;
        this.game.add.tween(biggerGroup).to({x: x-xMovement, y:y+150, width:biggerGroup.width*6, height: biggerGroup.height*6}, 10000, Phaser.Easing.Cubic.In, true).onComplete.add(function(){
            biggestGroup.destroy();
        });
        this.game.world.bringToTop(this.raft);
        this.createRateTimer(biggestGroup, val, right);
    }

    private createRateTimer(biggestGroup, val, right:boolean) {
        var timer:Phaser.Timer = this.game.time.create(true);
        var game:Phaser.Game = this.game;
        timer.add(9000, function () {
            console.log(this.raft.x);
            this.game.add.tween(biggestGroup).to({alpha: 0}, 1000, Phaser.Easing.Linear.None, true);
            if (this.raft.x < 300 && !right || this.raft.x > 700 && right) {
                this.caughtValue(val);
                var text:Phaser.Text = game.add.text(this.raft.x, 550, val.getValueName()+"\nSaved!", this.gameParameters.font_phaseThreeSaved);
                text.align = "center";
                this.game.add.tween(text).to({y:'-100', alpha: 0}, 2000, Phaser.Easing.Cubic.Out, true).onComplete.add(function(){
                    text.destroy();
                });
            }

        }, this);
        timer.start();
    }

    private caughtValue(val) {
        this.publish(new ValueRatedEvent(this, val, 3, 1));
        this.valuesAdvanced++;
        this.valuesAdvancedText.text = this.valuesAdvanced+" ";
    }

    private drawAdvancedCounter():void{
        var trHeight:number = 85;
        var trWidth:number = 75;
        var topOffset:number = 20;
        var counter:Phaser.Sprite = new Phaser.Sprite(this.game, 980, 250);
        this.group.add(counter);

        var hexagon:Phaser.Graphics = new Phaser.Graphics(this.game, 0, 0);
        var points: Phaser.Point[] = [];
        points.push(new Phaser.Point(0,trHeight-topOffset));
        points.push(new Phaser.Point(0,topOffset));
        points.push(new Phaser.Point(trWidth/2, 0));
        points.push(new Phaser.Point(trWidth,topOffset));
        points.push(new Phaser.Point(trWidth,trHeight-topOffset));
        points.push(new Phaser.Point(trWidth/2, trHeight));
        hexagon.beginFill(0x00000, 0.8);
        hexagon.drawPolygon(points);
        counter.addChild(hexagon);

        var countTitle:Phaser.Text = new Phaser.Text(this.game, 39, 34, "Values ", this.gameParameters.font_valuesAdvancedYellow);
        countTitle.anchor.set(0.5, 0);
        counter.addChild(countTitle);
        var countTitle2:Phaser.Text = new Phaser.Text(this.game, 39, 49, "Saved ", this.gameParameters.font_valuesAdvancedYellow);
        countTitle2.anchor.set(0.5, 0);
        counter.addChild(countTitle2);
        this.valuesAdvancedText = new Phaser.Text(this.game, 39, 10, this.valuesAdvanced+" ", this.gameParameters.font_valuesAdvancedWhite);
        this.valuesAdvancedText.anchor.set(0.5, 0);
        counter.addChild(this.valuesAdvancedText);

    }





    accept(gameEvent:GameEvent) {
        this.publish(gameEvent);
    }

    subscribe(sub:GameEventSubscriber):void {
        this.subs.push(sub);
    }

    publish(gameEvent:GameEvent):void {
        for(var i in this.subs){
            this.subs[i].accept(gameEvent);
        }
    }



}