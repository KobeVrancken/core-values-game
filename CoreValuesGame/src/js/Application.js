/// <reference path= "../../lib/phaser.d.ts" />
/// <reference path= "states/boot.ts" />
/// <reference path= "states/LandingScreen.ts" />
/// <reference path= "states/Preload.ts" />
/// <reference path= "states/PillarSelection.ts" />
/// <reference path= "states/PillarPhase.ts" />
/// <reference path= "states/PhaseTwo.ts" />
/// <reference path= "states/PhaseTwoPartB.ts" />
/// <reference path= "states/PhaseThree.ts" />
/// <reference path= "states/PhaseThreePartB.ts" />
/// <reference path= "states/PhaseFourSelection.ts" />
/// <reference path= "states/PhaseFour.ts" />
/// <reference path= "permanent/GameParameters.ts" />
/// <reference path= "../../lib/jquery/jquery.d.ts" />
var Application = (function () {
    function Application() {
        var gameParameters = GameParameters.getInstance();
        var gameWidth = gameParameters.stageWidth;
        if (gameParameters.debug) {
            gameWidth = gameParameters.stageWidth + gameParameters.debugWidth;
        }
        this.game = new Phaser.Game(gameWidth, gameParameters.stageHeight, Phaser.AUTO, 'content', "Boot", true);
        $('.center').width(gameWidth);
        this.game.state.add("Boot", Boot);
        this.game.state.add("Preload", Preload);
        this.game.state.add("LandingScreen", LandingScreen);
        this.game.state.add("PillarSelection", PillarSelection);
        this.game.state.add("PillarPhase", PillarPhase);
        this.game.state.add("PhaseTwo", PhaseTwo);
        this.game.state.add("PhaseTwoPartB", PhaseTwoPartB);
        this.game.state.add("PhaseThree", PhaseThree);
        this.game.state.add("PhaseThreePartB", PhaseThreePartB);
        this.game.state.add("PhaseFourSelection", PhaseFourSelection);
        this.game.state.add("PhaseFour", PhaseFour);
        this.game.state.start("Boot");
    }
    return Application;
})();
window.onload = function () {
    new Application();
};
//# sourceMappingURL=Application.js.map