/// <reference path= "../../../lib/jquery/jquery.d.ts" />

class VideoReference{

    private id:string;
    private videoElement:any;
    private vidRef;

    //private actualElement:any;

    constructor(id:string){
        this.id = id;
        this.vidRef = $("#" + this.id);
        this.videoElement = this.vidRef[0];
    }

    showVideo(){
        this.vidRef.css("visibility", "visible");
        this.vidRef.show();
    }

    hideVideo(){
        this.vidRef.css("visibility", "hidden");
        this.vidRef.hide();
        if(this.vidRef.hasClass("video-js"))
        {
            this.videoElement.player.pause();
        }else{
            this.videoElement.pause();
        }
    }


    playVideo(){
       this.videoElement.play();
    }
}