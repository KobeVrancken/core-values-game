/// <reference path= "../phases/all/Pillar.ts" />
/// <reference path= "../phases/all/Value.ts" />
/// <reference path= "../../../lib/jquery/jquery.d.ts" />

class XMLParser{

    private xml:XMLDocument;

    constructor(xmlString:string){
        this.xml = $.parseXML(xmlString);
    }

    public parsePillars():Array<Pillar>{
        var resultList:Array<Pillar> = new Array<Pillar>();
        var pillarElements:JQuery = $(this.xml).find("pillar");
        var parseFunction:Function = this.parseMap; //bring function in scope of jquery
        $(pillarElements).each(function(){
            var name:string = $(this).attr("name");
            var color:string = $(this).attr("color");
            var mapString:string = $(this).text();
            var group:string = $(this).attr("group");
            var longName:string = $(this).attr("fullname");
            var shortName:string = $(this).attr("shortname");
            resultList.push(new Pillar(name, color, parseFunction(mapString), group, shortName, longName));
        });
        return resultList;
    }

    private parseMap(mapString:string):Array<Array<string>>{
        var result:Array<Array<string>> = new Array<Array<string>>();
        var rows:Array<string> = mapString.split(";");
        for(var i in rows){
            var row:string = rows[i];
            var rowArray:Array<string> = row.split(",");
            result.push(rowArray);
        }
        return result;
    }


    public parseValues():Array<Value>{
        var resultList:Array<Value> = new Array<Value>();

        var longValues:JQuery = $(this.xml).find("long").find("item");
        $(longValues).each(function(){
            var rawLongString:string = $(this).attr("linea");
            var splitLongString:Array<string> = rawLongString.split("***");
            var value:Value = new Value(splitLongString[0]);
            value.setShortDefinition(splitLongString[1]);
            value.setImprovement(splitLongString[2]);
            resultList.push(value);
        });
        for(var i in resultList){
            this.completeSmallInfo(resultList[i]);
            this.completeTinyInfo(resultList[i]);
        }
        return resultList;
    }

    private completeTinyInfo(val:Value):void{
        var smallGroup:string = $(this.xml).find("tiny").find("item[value='"+val.getValueName()+"']").attr("group");
        val.setSmallGroup(smallGroup);
    }

    private completeSmallInfo(val:Value):void{
        var item:JQuery = $(this.xml).find("short").find("item[line='"+val.getValueName()+"']");
        val.setBigGroup(item.attr("group"));
        var nextItem:JQuery = item.next();
        val.setLongDefinition(nextItem.attr("line"));
        nextItem = nextItem.next();
        val.setExample(nextItem.attr("line"));
    }

}