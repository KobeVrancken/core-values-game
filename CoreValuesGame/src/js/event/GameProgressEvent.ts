/// <reference path= "GameEvent.ts" />
/// <reference path= "GameEventPublisher.ts" />

class GameProgressEvent extends GameEvent{

    private progress:number;


    constructor(publisher:GameEventPublisher, progress:number){
        super(publisher);
        this.progress = progress;
    }

    public getProgress():number{
        return this.progress;
    }

}