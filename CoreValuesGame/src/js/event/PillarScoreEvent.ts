/// <reference path= "../phases/all/Pillar.ts" />
/// <reference path= "GameEvent.ts" />
/// <reference path= "GameEventPublisher.ts" />

class PillarScoreEvent extends GameEvent{

    private pillar:Pillar;
    private score:number;

    constructor(publisher:GameEventPublisher, pillar:Pillar, score:number){
        super(publisher);
        this.pillar = pillar;
        this.score = score;
    }

    public getPillar():Pillar{
        return this.pillar;
    }

    public getScore():number{
        return this.score;
    }
}