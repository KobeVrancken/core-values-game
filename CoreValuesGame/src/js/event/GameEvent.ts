/**
 * Created by kobe on 3/26/2015.
 */

/// <reference path= "GameEventPublisher.ts" />
class GameEvent{

    private publisher:GameEventPublisher;

    constructor(publisher:GameEventPublisher){
        this.publisher = publisher;
    }

    public getPublisher():GameEventPublisher{
        return this.publisher;
    }




}