/**
 * Created by kobe on 4/4/2015.
 */

/// <reference path= "GameEventPublisher.ts" />
class PhaseTwoRoundEvent extends GameEvent {

    private advanced:number;
    private bonusValues:number;

    constructor(publisher:GameEventPublisher, advanced:number, bonusValues:number){
        super(publisher);
        this.advanced = advanced;
        this.bonusValues = bonusValues;
    }

    public getAdvanced():number{
        return this.advanced;
    }

    public getBonusValues():number{
        return this.bonusValues;
    }

}