/**
 * Created by kobe on 3/26/2015.
 */

/// <reference path= "GameEventSubscriber.ts" />
/// <reference path= "GameEvent.ts" />

interface GameEventPublisher {
    subscribe(sub:GameEventSubscriber):void;
    publish(gameEvent:GameEvent):void;
}