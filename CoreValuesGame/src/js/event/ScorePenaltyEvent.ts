/// <reference path= "GameEvent.ts" />
/// <reference path= "../phases/all/Pillar.ts" />

class ScorePenaltyEvent extends GameEvent{

    private pillar:Pillar;
    private penalty:number;

    constructor(publisher:GameEventPublisher, pillar:Pillar, penalty:number){
        super(publisher);
        this.pillar = pillar;
        this.penalty = penalty;
    }

    public getPillar():Pillar{
        return this.pillar;
    }

    public getPenalty():number{
        return this.penalty;
    }


}