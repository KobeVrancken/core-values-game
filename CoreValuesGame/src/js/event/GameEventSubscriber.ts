/**
 * Created by kobe on 3/26/2015.
 */
/// <reference path= "GameEventPublisher.ts" />
/// <reference path= "GameEvent.ts" />

interface GameEventSubscriber{
    accept(gameEvent:GameEvent);
}