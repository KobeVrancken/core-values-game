/**
 * Created by kobe on 3/26/2015.
 */
/// <reference path= "GameEvent.ts" />

class EndInstructionsEvent extends GameEvent{
    constructor(publisher:GameEventPublisher){
        super(publisher);
    }
}