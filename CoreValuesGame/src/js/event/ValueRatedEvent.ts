/// <reference path= "GameEvent.ts" />
/// <reference path= "../phases/all/Value.ts" />

class ValueRatedEvent extends GameEvent{

    private value:Value;
    private rating:number;
    private realScore:number;

    constructor(publisher:GameEventPublisher, value:Value, rating:number, realScore:number){
        super(publisher);
        this.value = value;
        this.rating = rating;
        this.realScore = realScore;
    }

    public getValue():Value{
        return this.value;
    }
    public getRating():number{
        return this.rating;
    }

    public getRealScore():number{
        return this.realScore;
    }


}