/// <reference path= "../phases/all/Value.ts" />
/// <reference path= "GameEvent.ts" />
/// <reference path= "GameEventPublisher.ts" />

class CurrentScoreEvent extends GameEvent{

    private score:number;


    constructor(publisher:GameEventPublisher, score:number){
        super(publisher);
        this.score = score;
    }

    public getScore():number{
        return this.score;
    }

}