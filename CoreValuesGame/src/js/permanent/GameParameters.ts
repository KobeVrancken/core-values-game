/// <reference path= "../../../lib/phaser.d.ts" />

class GameParameters{

    private static instance:GameParameters = null;

    public debug:boolean = true;
    public debugWidth:number = 300;
    public stageWidth:number = 1200;
    public stageHeight:number = 724;
    public stageColor:number = 0x1E1C4B;

    public font_bigInstructions:any = {
        font: '15px Arial',
        fill: '#FFFFFF'
    };

    public font_bigInstructionsTitle:any = {
        font: '900 40px Arial',
        fill: '#FFFF00'
    };

    public font_mediumInstructions:any = {
        font: '18px Arial',
        fill: '#FFFF00'
    };

    public font_mediumInstructionsTitle:any = {
        font: '900 30px Arial',
        fill: '#FFFFFF'
    };

    public font_mediumInstructionsSubtitle:any = {
        font: '900 22px Arial',
        fill: '#FFFFFF'
    };

    public font_menuButtonBig:any = {
        font: '900 18px Arial',
        fill: '#FFFFFF'
    };

    public font_menuButtonBigYellow:any = {
        font: '900 18px Arial',
        fill: '#FFFF00'
    };

    public font_menuAudioButtonWhite:any = {
        font: 'italic 14px Arial',
        fill: '#FFFFFF'
    };

    public font_menuAudioButtonYellow:any = {
        font: 'italic 14px Arial',
        fill: '#FFFF00'
    };

    public font_bigPhaseTitle:any = {
        font: '900 50px Arial',
        fill: '#FFFFFF'
    };

    public font_phaseSubtitle:any = {
        font: '28px Arial',
        fill: '#FFFFFF'
    };

    public font_copyrightFont:any = {
        font: '14px Arial',
        fill: '#FFFFFF'
    };

    public font_phaseIndicator:any = {
        font: '11px Arial',
        fill: '#808080'
    };

    public font_usernameFont:any = {
        font: 'bold 12px Arial',
        fill: '#FFFFFF'
    };

    public font_valueRater:any = {
        font: '14px Arial',
        fill: '#FFFFFF'
    };

    public font_valueRaterTitle:any = {
        font: '900 25px Arial',
        fill: '#FFFF00'
    };

    public font_valueRaterItalic:any = {
        font: 'italic 14px Arial',
        fill: '#FFFFFF'
    };

    public font_Timer:any = {
        font: 'italic 21px Arial',
        fill: '#FFFFFF'
    };


    public font_bonusValueWhite:any = {
        font: 'italic 18px Arial',
        fill: '#FFFFFF'
    };

    public font_bonusValueYellow:any = {
        font: 'italic 24px Arial',
        fill: '#FFFF00'
    };

    public font_valuesAdvancedYellow:any = {
        font: 'italic 13px Arial',
        fill: '#FFFF00'
    };
    public font_valuesAdvancedWhite:any = {
        font: 'italic 24px Arial',
        fill: '#FFFFFF'
    };

    public font_phaseTwoBValueName:any = {
        font: 'italic 18px Arial',
        fill: '#FFFF00'
    };

    public font_phaseTwoBTitleText:any = {
        font: 'italic 18px Arial',
        fill: '#FFFFFF'
    };

    public font_phaseThreeInstructions:any = {
        font: 'italic 32px Arial',
        fill: '#18e0fe'
    };

    public font_phaseThreeSaved:any = {
        font: '900 32px Arial',
        fill: '#ffffff'
    };

    public font_valueBonusInitial:any = {
        font: '900 35px Arial',
        fill: '#000000'
    };

    public font_valueBonusTitle:any = {
        font: 'bold 14px Arial',
        fill: '#ffffff'
    };

    public font_valueBonusFooter:any = {
        font: 'italic bold 14px Arial',
        fill: '#ffffff'
    };
    public font_hexagonToStart:any = {
        font: '900 9px Arial',
        fill: '#ffff00'
    };

    public font_hexagonToStartBlack:any = {
        font: '900 9px Arial',
        fill: '#000000'
    };

    public font_pause:any = {
        font: '900 italic 60px Arial',
        fill: '#ffffff'
    };

    public font_phaseFourValueName:any = {
        font: '12px Arial',
        fill: '#ffff00'
    };








    constructor(){
        if(GameParameters.instance){
            throw new Error("Error: Instantiation failed: Use GameParameters.getInstance() instead of new.");
        }
        GameParameters.instance = this;
    }

    public static getInstance():GameParameters{
        if(GameParameters.instance === null){
            GameParameters.instance = new GameParameters();
        }
        return GameParameters.instance;
    }

    public getPercentageBarFont(color:string):any{
        return {
            font: '900 13px Arial',
            fill: color
        };
    }

    public getPillarPhaseFont(color:string):any{
        return {
            font: '900 23px Arial',
            fill: color
        };
    }

    public getPhaseTwoBChoiceFont(color:string):any{
        return {
            font: 'italic 20px Arial',
            fill: color
        };
    }

    public getValueHexagonFont(size:string):any{
        return {
            font: size+'px Arial',
            fill: '#FFFF00'
        };
    }

    public getValueBonusText(color:string):any{
        return {
            font: 'bold 13px Arial',
            fill: color
        };
    }

    convertColorStringToNumber(color:string):number{
        var hexString = color.replace("#", "0x");
        return parseInt(hexString, 16);
    }

    public pad(n, width:number):string{
        var z:string = '0';
        n = n + '';
        return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
    }

    public fadeObjectIn(game:Phaser.Game, phaserObject:any, speed:number, delay:number):void{
        phaserObject.alpha = 0;
        game.time.events.add(delay, function () {
            game.add.tween(phaserObject).to({alpha: 1}, speed, Phaser.Easing.Linear.None, true);
        }, this);
    }

    public fadeObjectOut(game:Phaser.Game, phaserObject:any, speed:number, delay:number):void{
        phaserObject.alpha = 1;
        game.time.events.add(delay, function () {
            game.add.tween(phaserObject).to({alpha: 0}, speed, Phaser.Easing.Linear.None, true);
        }, this);

    }


}