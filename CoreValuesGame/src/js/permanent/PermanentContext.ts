/**
 * Created by kobe on 3/22/2015.
 */
/// <reference path= "../../../lib/phaser.d.ts" />
/// <reference path= "ScoreBoard.ts"/>
/// <reference path= "DebugPanel.ts"/>
/// <reference path= "../event/GameEventSubscriber.ts" />
/// <reference path= "../event/PillarScoreEvent.ts" />
/// <reference path= "GameState.ts" />
/// <reference path= "../phases/all/Pillar.ts" />

class PermanentContext implements GameEventSubscriber{

    private game:Phaser.Game;

    private gameState:GameState;

    private group:Phaser.Group;

    private scoreBoard:ScoreBoard;

    private debugPanel:DebugPanel;

    private fontStyle:any = {
        font: '14px Verdana',
        fill: '#FFFFFF'
    };

    constructor(game:Phaser.Game, gameState:GameState){
        this.game = game;
        this.gameState = gameState;
        this.group = new Phaser.Group(this.game, null, "Permanent Context", true); //Add group to stage to make it persistent between states
    }

    draw():void{
        this.addLogo();
        this.addScoreBoard();
        this.addDebug();
    }

    addLogo():void{
        var logo = this.group.create(0, 23, 'logo');
        var text:Phaser.Text = new Phaser.Text(this.game, 75, 133, "Find out what matters the most to you!", this.fontStyle);
        this.group.add(text);
    }

    addScoreBoard():void{
        this.scoreBoard = new ScoreBoard(this.game, this.group);

        this.scoreBoard.addTotalBar();
        var pillars:Array<Pillar> = this.gameState.getPillars();
        for(var i in pillars){
            this.scoreBoard.addScoreBar(pillars[i]);
        }

    }

    addDebug():void{
        this.debugPanel = new DebugPanel(this.game, this.group, this.gameState);
        this.debugPanel.draw();
    }

    accept(gameEvent:GameEvent) {
        if(gameEvent instanceof PillarScoreEvent){
            this.scoreBoard.setPillarScore(gameEvent.getPillar(),gameEvent.getScore());
        }
        this.debugPanel.redraw();
    }

}