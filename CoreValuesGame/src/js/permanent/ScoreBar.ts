/**
 * Created by kobe on 3/22/2015.
 */
/// <reference path= "../../../lib/phaser.d.ts" />
/// <reference path= "GameParameters.ts" />

class ScoreBar {
    private name:string;
    private game:Phaser.Game;
    private group:Phaser.Group;
    private scoreBar:Phaser.Graphics;
    private gameParameters:GameParameters = GameParameters.getInstance();
    private total:number;
    private scoreText:Phaser.Text;
    private score:number = 0;

    private fontStyle:any = {
        font: 'italic 12px Arial',
        fill: '#FFFFFF'
    };
    constructor(game: Phaser.Game, group: Phaser.Group, name:string, total:number) {
        this.game = game;
        this.name = name;
        this.group = group;
        this.total = total;
    }

    getName():string{
        return this.name;
    }

    getFont(color:string):any{
        this.fontStyle.fill = color;
        return this.fontStyle;
    }

    draw(x:number, y:number, color:string):void{
        var text:Phaser.Text = new Phaser.Text(this.game, x, y, this.getName(), this.getFont(color));
        this.group.add(text);
        var emptyBar:Phaser.Graphics = new Phaser.Graphics(this.game, 0, 0);
        emptyBar.beginFill(0x000000, 1);
        emptyBar.drawRect(x+text.width+8, y+4, 91, 8);
        this.group.add(emptyBar);
        this.scoreText = new Phaser.Text(this.game, x+text.width+104, y, "0000", this.getFont(color));
        this.group.add(this.scoreText);

        this.scoreBar = new Phaser.Graphics(this.game, x+text.width+8, y+4);
        this.scoreBar.beginFill(this.gameParameters.convertColorStringToNumber(color), 1);
        this.scoreBar.drawRect(0, 0, 91, 8);
        this.scoreBar.width = 0;
        this.group.add(this.scoreBar);

    }

    public setScore(score:number):void{
        this.score = score;
        var fraction:number = score/this.total;
        this.scoreBar.width = 91*fraction;
        this.scoreText.text = this.gameParameters.pad(Math.round(score),4);
    }

    public getScore():number{
        return Math.round(this.score);
    }



}