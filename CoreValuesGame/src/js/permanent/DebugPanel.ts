/**
 * Created by kobe on 3/22/2015.
 */
/// <reference path= "../../../lib/phaser.d.ts" />
    /// <reference path= "GameState.ts" />

class DebugPanel{


    private game:Phaser.Game;
    private group:Phaser.Group;
    private gameState:GameState;

    private actualText:Phaser.Text;

    private fontStyle:any = {
        font: '10px Arial',
        fill: '#FFFFFF'
    };

    constructor(game:Phaser.Game, group:Phaser.Group, gameState:GameState){
        this.game = game;
        this.group = group;
        this.gameState = gameState;
    }

    draw():void{
        var emptyBar:Phaser.Graphics = new Phaser.Graphics(this.game, 0, 0);
        emptyBar.beginFill(0x000000, 1);
        emptyBar.drawRect(1200, 0, 300, 724);
        this.group.add(emptyBar);
        this.actualText = new Phaser.Text(this.game, 1220, 30, "VALUES SCORES [DEBUG]", this.fontStyle);
        this.group.add(this.actualText);
    }

    redraw():void{
        var text:string = "";
        var j = -1;
        for(var i in this.gameState.getValues()){
            var value:Value = this.gameState.getValues()[i];
            var valueScore:ValueScore = this.gameState.getValueScore(value);
            if(valueScore.getTotal() != 0 || valueScore.getRealTotal() != 0) {
                text += value.getValueName() + " ";
                text += valueScore.getTotal() + " ";
                text += valueScore.getRealTotal();
                j++;
                if (j % 3 == 2) {
                    text += "\n"
                }else{
                    text += " - "
                }
            }
        }
        this.actualText.text = text;
    }
}