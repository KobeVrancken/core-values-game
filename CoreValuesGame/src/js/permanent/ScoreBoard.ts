/**
 * Created by kobe on 3/22/2015.
 */
/// <reference path= "ScoreBar.ts"/>
/// <reference path= "../phases/all/Pillar.ts"/>

class ScoreBoard{
    private scoreBars:Array<ScoreBar>;
    private totalBar:ScoreBar;

    private game:Phaser.Game;
    private group:Phaser.Group;

    private xCoordinates:Array<number> = [10, 422, 609, 799, 1002];
    private colors:Array<string> = ['#ffffff', '#fa1b4d', '#815bce', '#659b2f', '#1497d6'];
    private pillars:Array<Pillar> = new Array<Pillar>();

    private arrayPointer:number = 0;


    private yCoordinate:number = 6;


    constructor(game:Phaser.Game, group:Phaser.Group){
        this.scoreBars = new Array<ScoreBar>();
        this.game = game;
        this.group = group;
    }

    addTotalBar():void{
        var scoreBar:ScoreBar = new ScoreBar(this.game, this.group, "TOTAL SCORE", 20000);
        this.totalBar = scoreBar;
        scoreBar.draw(this.xCoordinates[this.arrayPointer],this.yCoordinate, this.colors[this.arrayPointer++]);
    }

    addScoreBar(pillar:Pillar):void{
        var scoreBar:ScoreBar = new ScoreBar(this.game, this.group, pillar.getShortName(), 5000);
        this.scoreBars.push(scoreBar);
        this.pillars.push(pillar);
        scoreBar.draw(this.xCoordinates[this.arrayPointer],this.yCoordinate, this.colors[this.arrayPointer++]);
    }

    setPillarScore(pillar:Pillar, score:number){
        if(score < 0) score = 0;
        var total:number = 0;
        for(var i in this.pillars){
            if(pillar === this.pillars[i]){
                this.scoreBars[i].setScore(score);
            }
            total += this.scoreBars[i].getScore();
        }
        this.totalBar.setScore(total);
    }

}