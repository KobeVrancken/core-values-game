/// <reference path= "../../../lib/phaser.d.ts" />
/// <reference path= "GameParameters.ts" />

class Pause{
    private game:Phaser.Game;
    private gameParameters:GameParameters = GameParameters.getInstance();

    constructor(game:Phaser.Game){
        this.game = game;
        this.pause();
    }

    private pause(){
        var group:Phaser.Group = new Phaser.Group(this.game, null, "Pause", true);
        var blockingSprite:Phaser.Graphics = new Phaser.Graphics(this.game, 0, 0);
        blockingSprite.beginFill(0x1E1C4B, 0.9);
        blockingSprite.drawRect(0, 0, this.gameParameters.stageWidth, this.gameParameters.stageHeight);
        var actualSprite = new Phaser.Sprite(this.game, 0, 0);
        actualSprite.addChild(blockingSprite);
        actualSprite.inputEnabled = true;
        group.add(actualSprite);
        actualSprite.bringToTop();
        var pauseText:Phaser.Text = new Phaser.Text(this.game,  this.gameParameters.stageWidth/2, this.gameParameters.stageHeight/2, "Game is paused! \nClick to Continue ", this.gameParameters.font_pause);
        pauseText.anchor.set(0.5, 0.5);
        group.add(pauseText);
        this.game.paused = true;
        var event:Phaser.SignalBinding = this.game.input.onDown.add(function () {
            this.game.paused = false;
            group.destroy();
            event.active = false;
        },this);
    }
}