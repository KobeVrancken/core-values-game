/// <reference path= "../phases/all/Pillar.ts" />
/// <reference path= "../phases/all/Value.ts" />
/// <reference path= "../phases/all/ValueScore.ts" />
/// <reference path= "../phases/all/PillarScore.ts" />
/// <reference path= "../phases/all/RoundScore.ts" />
/// <reference path= "../event/GameEventPublisher.ts" />
/// <reference path= "../event/GameEventSubscriber.ts" />
/// <reference path= "../event/ValueRatedEvent.ts" />
/// <reference path= "../event/ScorePenaltyEvent.ts" />
/// <reference path= "../event/PillarScoreEvent.ts" />
/// <reference path= "../event/CurrentScoreEvent.ts" />
/// <reference path= "../phases/one/ValueRater.ts" />

class GameState implements  GameEventPublisher, GameEventSubscriber{

    private pillars:Array<Pillar> = new Array<Pillar>();
    private pillarScores:Array<PillarScore> = new Array<PillarScore>();

    private values:Array<Value> = new Array<Value>();
    private valueScores:Array<ValueScore> = new Array<ValueScore>();

    private subs:Array<GameEventSubscriber> = new Array<GameEventSubscriber>();
    private roundScore:RoundScore = new RoundScore();

    constructor(pillars:Array<Pillar>, values:Array<Value>){
        this.pillars = pillars;
        this.values = values;
        for(var i in values){
            this.valueScores.push(new ValueScore(this.values[i]));
        }
        for(var i in pillars){
            var pillarScore:PillarScore = new PillarScore(pillars[i]);
            for(var j in values){
                if(pillars[i].hasValue(values[j])){
                    values[j].setPillar(pillars[i]);
                    var valueScore:ValueScore = this.getValueScore(values[j]);
                    pillarScore.addValueScore(valueScore);
                }
            }
            this.pillarScores.push(pillarScore);
        }
    }

    public getPillarByValue(value:Value):Pillar{
        return value.getPillar();
    }

    public getPillars():Array<Pillar>{
        return this.pillars;
    }

    public getValues():Array<Value>{
        return this.values;
    }

    public getValueScores():Array<ValueScore>{
        return this.valueScores;
    }

    public getValueScoresByPillar(pillar:Pillar):Array<ValueScore>{
        var result:Array<ValueScore> = new Array<ValueScore>();
        for(var i in this.valueScores){
            if(this.valueScores[i].getValue().getPillar() === pillar){
                result.push(this.valueScores[i]);
            }
        }
        return result;
    }

    public hasValueScoresForPillar(pillar:Pillar, round:number){
        if(round == 1){
            for(var i in this.pillars){
                if(this.pillars[i] == pillar){
                    return this.pillarScores[i].getScore() > 0;
                }
            }
        }
    }

    public removeValueScoresForPillar(pillar:Pillar, round:number){
        if(round == 1){
            var valueScoresToRemove:Array<ValueScore> = this.getValueScoresByPillar(pillar);
            for(var i in valueScoresToRemove){
                var valueScore:ValueScore = valueScoresToRemove[i];
                valueScore.removePhaseOneRating();
                var pillarScore:PillarScore = this.getPillarScore(pillar);
                var pillarScoreEvent:PillarScoreEvent = new PillarScoreEvent(this, pillar, pillarScore.getScore());
                this.publish(pillarScoreEvent);
            }
        }
    }

    public getValueScore(val:Value):ValueScore{
        for(var i in this.valueScores){
            if(this.valueScores[i].getValue() === val){
                return this.valueScores[i];
            }
        }
        /*var index:number;
        for(var i in this.values){
            if(this.values[i] === val){
                index = i;
            }
        }
        return this.valueScores[index];*/
    }

    private getPillarScore(pillar:Pillar):PillarScore{
        var index:number;
        for(var i in this.pillars){
            if(this.pillars[i] === pillar){
                index = i;
            }
        }
        return this.pillarScores[index];
    }


    subscribe(sub:GameEventSubscriber):void {
        this.subs.push(sub);
    }

    publish(gameEvent:GameEvent):void {
        for(var i in this.subs){
            this.subs[i].accept(gameEvent);
        }
    }

    accept(gameEvent:GameEvent) {
        if(gameEvent instanceof ValueRatedEvent){
            console.log("Value rating received");
            var val:Value = gameEvent.getValue();
            var rating:number = gameEvent.getRating();
            var valueScore:ValueScore = this.getValueScore(val);
            valueScore.addRating(rating);
            valueScore.addRealScore(gameEvent.getRealScore());
            if(gameEvent.getPublisher() instanceof ValueRater) valueScore.setPhaseOneRating(rating);
            this.roundScore.addScore(rating);

            var pillar:Pillar = this.getPillarByValue(val);
            var pillarScore:PillarScore = this.getPillarScore(pillar);
            var pillarScoreEvent:PillarScoreEvent = new PillarScoreEvent(this, pillar, pillarScore.getScore());
            this.publish(pillarScoreEvent);
            var currentScoreEvent:CurrentScoreEvent = new CurrentScoreEvent(this, this.roundScore.getScore());
            this.publish(currentScoreEvent);
        }else if(gameEvent instanceof ScorePenaltyEvent){
            var rating:number = gameEvent.getPenalty();
            var pillar:Pillar = gameEvent.getPillar();
            this.roundScore.addScore(-rating);
            var pillarScore:PillarScore = this.getPillarScore(pillar);
            pillarScore.addPenalty(rating);
            var pillarScoreEvent:PillarScoreEvent = new PillarScoreEvent(this, pillar, pillarScore.getScore());
            this.publish(pillarScoreEvent);
            var currentScoreEvent:CurrentScoreEvent = new CurrentScoreEvent(this, this.roundScore.getScore());
            this.publish(currentScoreEvent);
        }
    }




}